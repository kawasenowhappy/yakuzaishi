//
//  Site.m
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "Site.h"

@implementation Site

@synthesize title;
@synthesize excerpt;
@synthesize content;
@synthesize linkUrl;
@synthesize iconUrl;
@synthesize subtitle;
@synthesize isReaded;

static NSString* const SITE_IMAGE_URL_TPL = @"http://yaku.nowhappy.info/api/image/%d.png";
static NSString* const SITE_LIST_IMAGE_URL_TPL = @"http://yaku.nowhappy.info/api/image/%d_list.png";

-(id) initWithJson:(NSDictionary*)json{
    self = [super init];
    if(self){
        id_ = [[json objectForKey:@"id"] intValue];
        title = [json objectForKey:@"name"];
        subtitle = [json objectForKey:@"subtitle"];
        excerpt = [json objectForKey:@"excerpt"];
        content = [json objectForKey:@"content"];
        linkUrl = [json objectForKey:@"link_url"];
    }
    
    return self;
}

-(void) readed{
    isReaded = YES;
}

-(NSString*)listImageUrl{
    return [NSString stringWithFormat:SITE_LIST_IMAGE_URL_TPL, id_];
}

-(NSString*)contentImageUrl{
    return [NSString stringWithFormat:SITE_IMAGE_URL_TPL, id_];
}

@end
