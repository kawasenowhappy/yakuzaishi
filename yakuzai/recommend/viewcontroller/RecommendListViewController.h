//
//  RecommendListViewController.h
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "BaseViewController.h"
#import "Recommend.h"

@protocol RecommendListViewControllerDelegate <NSObject>

-(void) onSelectRecommend:(Recommend*)recommend;

@end

@interface RecommendListViewController : BaseViewController

-(id) initWithDelegate:(NSObject<RecommendListViewControllerDelegate>*)delegate_;

@end
