//
//  SelectButton.h
//  yakuzai
//
//  Created by kawase yu on 2014/07/05.
//  Copyright (c) 2014年 nowhappy. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SelectButtonDelegate <NSObject>

-(UIView*) getView;

@end

@interface SelectButton : UIView<
UIPickerViewDelegate
, UIPickerViewDataSource
>{
    __weak NSObject<SelectButtonDelegate>* delegate;
    UILabel* label;
    UILabel* keyLabel;
    NSArray* itemList;
    UIView* blackLayer;
    UIPickerView* pickerView;
    UIView* currentParentView;
}

+(SelectButton*)create:(NSObject<SelectButtonDelegate>*)delegate_ index:(int)index;
-(int) getIndex;
-(NSString*)labelText;

@end