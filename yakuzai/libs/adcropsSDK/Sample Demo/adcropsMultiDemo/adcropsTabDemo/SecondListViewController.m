//
//  SecondListViewController.m
//  adcropsTabDemo
//
//  Created by Tatsuya Uemura on 12/02/23.
//  Copyright (c) 2012年 8crops inc. All rights reserved.
//

#import "SecondListViewController.h"
#import "AppViewCell.h"

#import <dispatch/dispatch.h>
#import <QuartzCore/QuartzCore.h>

@implementation SecondListViewController

@synthesize appTableOptional;
@synthesize naviControllerOptional;
@synthesize chkControllerOptional;
@synthesize chkDataListOptional;
@synthesize indicatorOptional;
@synthesize updateTriggerOptional;

dispatch_queue_t main_queue;
dispatch_queue_t image_queue;

- (void)dealloc {
    NSLog(@"dealloc");
    self.naviControllerOptional = nil;
    self.appTableOptional       = nil;
    self.chkControllerOptional  = nil;
    self.chkDataListOptional    = nil;
    self.indicatorOptional      = nil;
    self.updateTriggerOptional  = nil;
    
    dispatch_release(image_queue);
    
    [super dealloc];
}

-(id) init {
    if (self = [super init]) {
        [self setTitle:@"無料アプリ(417)"];
        self.tabBarItem.image = [UIImage imageNamed:@"tab_app.png"]; 
        self.tabBarItem.tag = 2;
        self.chkControllerOptional = [[[ChkController alloc] initWithDelegateOptional:self] autorelease];
        
        main_queue = dispatch_get_main_queue();
		image_queue = dispatch_queue_create("net.adcrops.8chk.list-image", NULL);
    }
    return self;
}

- (void)getChkData {
    NSLog(@"getChkData");
    if ([self.indicatorOptional isAnimating] == YES) return;
	[self.indicatorOptional sizeToFit];
	[self.indicatorOptional startAnimating];
	self.indicatorOptional.center = self.view.center;
    
    // データを取得する。
    [self.chkControllerOptional requestDataList];
}


- (void)loadView {
    NSLog(@"loadView");
    [super loadView];
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    self.naviControllerOptional = [[[UINavigationController alloc] init] autorelease];
    self.naviControllerOptional.view.frame = CGRectMake(0.0f ,0.0f, self.view.frame.size.width, self.view.frame.size.height);
    
    self.appTableOptional = [[[UITableViewController alloc] init] autorelease];
    
    self.appTableOptional.view.backgroundColor = [UIColor clearColor];
    self.appTableOptional.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.appTableOptional.tableView.delegate = self;
    self.appTableOptional.tableView.dataSource = self;
    self.appTableOptional.navigationItem.title = @"無料アプリ(417)";
    
    // ヘッダー
    UIView *headerView = [[UIView alloc]initWithFrame: CGRectMake(
                                                                  0,
                                                                  0,
                                                                  self.view.frame.size.width,
                                                                  0)];
    self.updateTriggerOptional = [[[UpdateTrigger alloc] init] autorelease];
    [self.updateTriggerOptional adapteTrigger:self.appTableOptional.tableView view:headerView];
    self.updateTriggerOptional.delegte = self;
    
    self.appTableOptional.tableView.tableHeaderView = headerView;
    [headerView autorelease];
    
    // フッター
    UIButton *footerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [footerButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [footerButton setTitle:@"続きを読み込む" forState:UIControlStateNormal];
    [footerButton setFrame:CGRectMake(0,0,self.appTableOptional.tableView.frame.size.width,45)];
    [footerButton addTarget:self action:@selector(getChkData) forControlEvents:UIControlEventTouchUpInside];
    footerButton.backgroundColor = [UIColor clearColor];
    
	UIView *footerView = [[UIView alloc]initWithFrame: CGRectMake(
                                                                  0,
                                                                  0,
                                                                  self.appTableOptional.tableView.frame.size.width,
                                                                  45)];
    footerView.backgroundColor = [UIColor clearColor];
    [footerView addSubview:footerButton];
    [footerView setHidden:YES];
    self.appTableOptional.tableView.tableFooterView = footerView;
    [footerView autorelease];
    
    [self.naviControllerOptional pushViewController:self.appTableOptional animated:YES];
    
    //ナビゲーションバーを表示する
    [self.naviControllerOptional setNavigationBarHidden:NO animated:YES];
    //ツールバーを非表示にする
    [self.naviControllerOptional setToolbarHidden:YES animated:NO];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.naviControllerOptional.view];
    
    // indicator
    self.indicatorOptional = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
    self.indicatorOptional.autoresizingMask =
    UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin
    | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [self.view addSubview:self.indicatorOptional];
    
    // データをリセットする。
    [self.chkControllerOptional resetDataList];
    
    // テーブルをクリアする。
    [self.appTableOptional.tableView reloadData];
    
    [self getChkData];
}

#pragma mark -
#pragma mark UpdateTriggerDelegate delegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
	[self.updateTriggerOptional scrollViewWillBeginDragging:scrollView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
	[self.updateTriggerOptional scrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
	[self.updateTriggerOptional scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
}

-(void)update:(BOOL)foword {
    NSLog(@"update Trigger update:%d",foword);
    if ([self.indicatorOptional isAnimating] == YES) return;
    
    // データをリセットする。
    [self.chkControllerOptional resetDataList];
    self.chkDataListOptional = [NSMutableArray array];
    
    // フッターを非表示
    [self.appTableOptional.tableView.tableFooterView setHidden:YES];
    
    // テーブルをクリアする。
    [self.appTableOptional.tableView reloadData];
    
    [self getChkData];
}

#pragma mark -
#pragma mark ChkControllerDelegate delegate
- (void)chkControllerDataListWithSuccess:(NSDictionary *)data{
    NSLog(@"chkControllerDataListWithSuccess:%@",data);
    
    // 次のページがなければフッターを消す。
    if( [self.chkControllerOptional hasNextData] ) {
        [self.appTableOptional.tableView.tableFooterView setHidden:NO];
    }else{
        [self.appTableOptional.tableView.tableFooterView setHidden:YES];
    }
    
    self.chkDataListOptional = [NSMutableArray arrayWithArray:[self.chkControllerOptional dataList]];

    // テーブルをリロード
    [self.appTableOptional.tableView reloadData];
    
    [self.indicatorOptional stopAnimating];
}

- (void)chkControllerDataListWithError:(NSError*)error{
    NSLog(@"chkControllerDataListWithError");
    // エラー処理を書きます。
    [self.indicatorOptional stopAnimating];
}

- (void) chkControllerDataListWithNotFound:(NSDictionary *)data {
    NSLog(@"chkControllerDataListWithNotFound");
    // 在庫無し時の処理を書きます。
    [self.indicatorOptional stopAnimating];
}

#pragma mark -
#pragma mark Table view data source

// 交互に背景色を変える。
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row % 2 == 0) {
        // For even
        cell.backgroundColor = [UIColor colorWithHue:0.67
                                          saturation:0.00
                                          brightness:0.97
                                               alpha:1.0];
    } else {
        // For odd
        cell.backgroundColor = [UIColor colorWithHue:0.61
                                          saturation:0.09
                                          brightness:0.99
                                               alpha:1.0];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.chkDataListOptional count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"AppTableCell";
    AppViewCell *cell = (AppViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[AppViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier]autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell setNeedsLayout];
    }
    
    ChkRecordData *data = [self.chkDataListOptional objectAtIndex:[indexPath row]];
    
    // タイトル
    cell.titleLabel.text = data.title;
    // 詳細文
    cell.detailLabel.text = data.detail;
    // アプリアイコン
    if (![data cacheImage]) {
        // インプレッションを送信する。
        [self.chkControllerOptional sendImpression:data];
        NSString *url = [data imageIcon];
		dispatch_async(image_queue, ^{
			UIImage *icon = [self.chkControllerOptional getImage:url];
            // TODO:画像イメージをキャッシュする。
            [data setCacheImage:icon]; 
            dispatch_async(main_queue, ^{
                AppViewCell *cell = (AppViewCell *)[tableView cellForRowAtIndexPath:indexPath];
                cell.appImageView.layer.masksToBounds = YES;
                cell.appImageView.layer.cornerRadius = 10.0f;
                cell.appImageView.image = icon;
                [cell.indicator stopAnimating];
            });
		});
	} else {
		cell.appImageView.image = [data cacheImage];
        [cell.indicator stopAnimating];
	}
    
    return cell;
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return CELL_APP_IMAGE_SIZE + (CELL_APP_IMAGE_MARGIN*2);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ChkRecordData *data = [self.chkDataListOptional objectAtIndex:[indexPath row]];
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *url = [NSURL URLWithString:data.linkUrl];
    
    if( [application canOpenURL:url]){
        [application openURL:url];
    }else{
        NSLog(@"openUrl error.");
    }
}

@end
