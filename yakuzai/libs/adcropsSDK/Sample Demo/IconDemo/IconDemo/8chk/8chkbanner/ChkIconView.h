//
//  ChkIconView.h
//  iconDemo
//
//  Created by sazawayuki on 2013/04/26.
//  Copyright (c) 2013年 8crops inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChkController.h"
#import "ChkControllerDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import <StoreKit/StoreKit.h>

extern const float kChkAppIconSize;
extern const float kChkMarginTop;

@class ChkIconView;

@protocol ChkIconViewDelegate <NSObject>
// 初回ロードが成功した際に通知(在庫なしの場合は呼ばれません)
- (void)chkBannerViewDidFinishLoad:(ChkIconView *)iconView;
// 2回目以降の受信が成功した際に通知(在庫なしの場合は呼ばれません)
- (void)chkBannerViewDidReceiveAd:(ChkIconView *)iconView;
// 受信が失敗した際に通知(在庫なしの場合はエラーが返されます)
- (void)chkBannerViewDidFailToReceiveAd:(ChkIconView *)iconView;


@end

@interface ChkIconView : UIView

@property (assign, nonatomic) id <ChkIconViewDelegate> delegate;
// イニシャライズ(CPCサイズ)
- (id)initWithFrame:(CGRect)frame rootViewController:(UIViewController*)rootViewController iconCount:(NSInteger)count iconSize:(float)iconSize;
// バックグランドへ遷移した時
- (void)applicationDidEnterBackground;
// バッグランドから復帰した時
- (void)applicationWillEnterForeground;
// 広告のロード
- (void)load;
// アプリ名を表示するか？
- (void)setShowAppName:(BOOL)show;

@end
