//
//  ViewController.m
//  IconSlideDemo
//
//  Created by sazawayuki on 2014/02/28.
//  Copyright (c) 2014年 8crops inc. All rights reserved.
//

#import "ViewController.h"
#import "ChkSlideIconView.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // スクロール広告
    ChkSlideIconView *chkSlideIconView = [[ChkSlideIconView alloc]initWithIconSize:self.view.frame.size.height - kChkSlideIconAdViewICONSize.height rootViewController:self];
    [chkSlideIconView setShowAppName:[UIColor blackColor] backgroundColor:[UIColor whiteColor] backgroundAlpha:0.8];
    [chkSlideIconView setIndicatorColor:[UIColor blackColor]];
    [self.view addSubview:chkSlideIconView];
    [chkSlideIconView load];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
