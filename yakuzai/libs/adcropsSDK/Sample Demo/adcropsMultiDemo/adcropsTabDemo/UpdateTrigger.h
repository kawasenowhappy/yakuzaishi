//
//  UpdateTrigger.h
//  adcropsTabDemo
//
//  Created by Tatsuya Uemura on 12/02/23.
//  Copyright (c) 2012年 8crops inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpdateTriggerDelegate;

@interface UpdateTrigger : NSObject {
	UITableView*	tableView;
	BOOL			dragging;
	UILabel*		triggerHeader;
	BOOL			headerOn;
	id<UpdateTriggerDelegate>	delegte;
    UIView          *headerView;
    
}
@property (nonatomic, assign) id<UpdateTriggerDelegate>	delegte;

- (void)adapteTrigger:(UITableView*)inTableView view:(UIView*)view;
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView;
- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
- (void)adjust:(CGFloat)contentsHeight;
@end


@protocol UpdateTriggerDelegate<NSObject>
-(void)update:(BOOL)foword;
@end