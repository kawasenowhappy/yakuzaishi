//
//  News.h
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "Site.h"
#import "VknModel.h"

@interface News : VknModel

@property NSString* title;
@property NSString* linkUrl;
@property NSString* excerpt;
@property NSDate* date;
@property (readonly) BOOL isReaded;

-(NSString*)dateStr;
-(id) initWithJson:(NSDictionary*)jsonDic;
-(void) readed;

@end


