//
//  AppListViewController.m
//  adcropsTabDemo
//
//  Created by Tatsuya Uemura on 12/02/23.
//  Copyright (c) 2012年 8crops inc. All rights reserved.
//

#import "AppListViewController.h"
#import "AppViewCell.h"

#import <dispatch/dispatch.h>
#import <QuartzCore/QuartzCore.h>

@implementation AppListViewController

@synthesize appTable;
@synthesize naviController;
@synthesize chkController;
@synthesize chkDataList;
@synthesize indicator;
@synthesize updateTrigger;

@synthesize chkControllerOptional;
@synthesize chkDataListOptional;

dispatch_queue_t main_queue;
dispatch_queue_t image_queue;

- (void)dealloc {
    NSLog(@"dealloc");
    self.naviController = nil;
    self.appTable       = nil;
    self.chkController  = nil;
    self.chkDataList    = nil;
    self.indicator      = nil;
    self.updateTrigger  = nil;
    
    self.chkControllerOptional  = nil;
    self.chkDataListOptional    = nil;
    
    dispatch_release(image_queue);
    
    [super dealloc];
}

-(id) init {
    if (self = [super init]) {
        [self setTitle:@"無料アプリ混在"];
        self.tabBarItem.image = [UIImage imageNamed:@"tab_app.png"]; 
        self.tabBarItem.tag = 3;
        self.chkController = [[[ChkController alloc] initWithDelegate:self] autorelease];
        self.chkControllerOptional = [[[ChkController alloc] initWithDelegateOptional:self] autorelease];
        
        main_queue = dispatch_get_main_queue();
		image_queue = dispatch_queue_create("net.adcrops.8chk.list-image", NULL);
    }
    return self;
}

- (void)getChkData {
    NSLog(@"getChkData");
    if ([self.indicator isAnimating] == YES) return;
	[self.indicator sizeToFit];
	[self.indicator startAnimating];
	self.indicator.center = self.view.center;
    
    // データを取得する。
    [self.chkController requestDataList];
    
    // データを取得する。
    [self.chkControllerOptional requestDataList];
}


- (void)loadView {
    NSLog(@"loadView");
    [super loadView];
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    self.naviController = [[[UINavigationController alloc] init] autorelease];
    self.naviController.view.frame = CGRectMake(0.0f ,0.0f, self.view.frame.size.width, self.view.frame.size.height);
    
    self.appTable = [[[UITableViewController alloc] init] autorelease];
    
    self.appTable.view.backgroundColor = [UIColor clearColor];
    self.appTable.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.appTable.tableView.delegate = self;
    self.appTable.tableView.dataSource = self;
    self.appTable.navigationItem.title = @"無料アプリ混在";
    
    // ヘッダー
    UIView *headerView = [[UIView alloc]initWithFrame: CGRectMake(
                                                                  0,
                                                                  0,
                                                                  self.view.frame.size.width,
                                                                  0)];
    self.updateTrigger = [[[UpdateTrigger alloc] init] autorelease];
    [self.updateTrigger adapteTrigger:self.appTable.tableView view:headerView];
    self.updateTrigger.delegte = self;
    
    self.appTable.tableView.tableHeaderView = headerView;
    [headerView autorelease];
    
    [self.naviController pushViewController:self.appTable animated:YES];
    
    //ナビゲーションバーを表示する
    [self.naviController setNavigationBarHidden:NO animated:YES];
    //ツールバーを非表示にする
    [self.naviController setToolbarHidden:YES animated:NO];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.naviController.view];
    
    // indicator
    self.indicator = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
    self.indicator.autoresizingMask =
    UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin
    | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [self.view addSubview:self.indicator];
    
    // データをリセットする。
    [self.chkController resetDataList];
    [self.chkControllerOptional resetDataList];
    
    // テーブルをクリアする。
    [self.appTable.tableView reloadData];
    
    [self getChkData];
}

#pragma mark -
#pragma mark UpdateTriggerDelegate delegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
	[self.updateTrigger scrollViewWillBeginDragging:scrollView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
	[self.updateTrigger scrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
	[self.updateTrigger scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
}

-(void)update:(BOOL)foword {
    NSLog(@"update Trigger update:%d",foword);
    if ([self.indicator isAnimating] == YES) return;
    
    // データをリセットする。
    [self.chkController resetDataList];
    self.chkDataList = [NSMutableArray array];
    
    [self.chkControllerOptional resetDataList];
    self.chkDataListOptional = [NSMutableArray array];
    
    // フッターを非表示
    [self.appTable.tableView.tableFooterView setHidden:YES];
    
    // テーブルをクリアする。
    [self.appTable.tableView reloadData];
    
    [self getChkData];
}

#pragma mark -
#pragma mark ChkControllerDelegate delegate
- (void)chkControllerDataListWithSuccess:(NSDictionary *)data{
    NSLog(@"chkControllerDataListWithSuccess:%@",data);
    
    // 最後まで全部取りに行く。
    if([[data objectForKey:@"mediaSiteId"] isEqualToString:@"416"]) {
        if( [self.chkController hasNextData] ) {
            NSLog(@"****** self.chkController retry request *****");
            [self.chkController requestDataList];
            return;
        }
        for(ChkRecordData* data in [self.chkController dataList]) {
            NSLog(@"nomal data:%@",[data articleId]);
        }
        self.chkDataList = [NSMutableArray arrayWithArray:[self.chkController dataList]];
    }else {
        // optional
        if( [self.chkControllerOptional hasNextData] ) {
            NSLog(@"****** self.chkControllerOptional retry request *****");
            [self.chkControllerOptional requestDataList];
            return;
        }
        for(ChkRecordData* data in [self.chkControllerOptional dataList]) {
            NSLog(@"optional data:%@",[data articleId]);
        }
        self.chkDataListOptional = [NSMutableArray arrayWithArray:[self.chkControllerOptional dataList]];
    }
    
    // テーブルをリロード
    [self.appTable.tableView reloadData];
    
    [self.indicator stopAnimating];
}

- (void)chkControllerDataListWithError:(NSError*)error{
    NSLog(@"chkControllerDataListWithError:%@",error);
    // エラー処理を書きます。
    [self.indicator stopAnimating];
}

- (void) chkControllerDataListWithNotFound:(NSDictionary *)data {
    NSLog(@"chkControllerDataListWithNotFound");
    // 在庫無し時の処理を書きます。
    [self.indicator stopAnimating];
}

#pragma mark -
#pragma mark Table view data source

// 交互に背景色を変える。
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row % 2 == 0) {
        // For even
        cell.backgroundColor = [UIColor colorWithHue:0.67
                                          saturation:0.00
                                          brightness:0.97
                                               alpha:1.0];
    } else {
        // For odd
        cell.backgroundColor = [UIColor colorWithHue:0.61
                                          saturation:0.09
                                          brightness:0.99
                                               alpha:1.0];
    }
}

/*
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40.0;
}
*/

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger) section {
	switch(section) {
		case 0:
			return @"サイトID416のリスト";
		case 1:
			return @"サイトID417のリスト";
		default:
			return nil;
	}
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) {
        return [self.chkDataList count];
    }
    return [self.chkDataListOptional count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0) {
        static NSString *CellIdentifier = @"AppTableCell";
        AppViewCell *cell = (AppViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[AppViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier]autorelease];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell setNeedsLayout];
        }
        
        ChkRecordData *data = [self.chkDataList objectAtIndex:[indexPath row]];
        
        // タイトル
        cell.titleLabel.text = data.title;
        // 詳細文
        cell.detailLabel.text = data.detail;
        // アプリアイコン
        if (![data cacheImage]) {
            // インプレッションを送信する。
            [self.chkController sendImpression:data];
            NSString *url = [data imageIcon];
            dispatch_async(image_queue, ^{
                UIImage *icon = [self.chkController getImage:url];
                // TODO:画像イメージをキャッシュする。
                [data setCacheImage:icon]; 
                dispatch_async(main_queue, ^{
                    AppViewCell *cell = (AppViewCell *)[tableView cellForRowAtIndexPath:indexPath];
                    cell.appImageView.layer.masksToBounds = YES;
                    cell.appImageView.layer.cornerRadius = 10.0f;
                    cell.appImageView.image = icon;
                    [cell.indicator stopAnimating];
                });
            });
        } else {
            cell.appImageView.image = [data cacheImage];
            [cell.indicator stopAnimating];
        }
        
        return cell;
    }
    // Optional
    static NSString *CellIdentifier = @"AppOptionalTableCell";
    AppViewCell *cell = (AppViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[AppViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier]autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell setNeedsLayout];
    }
    
    ChkRecordData *data = [self.chkDataListOptional objectAtIndex:[indexPath row]];
    
    // タイトル
    cell.titleLabel.text = data.title;
    // 詳細文
    cell.detailLabel.text = data.detail;
    // アプリアイコン
    if (![data cacheImage]) {
        // インプレッションを送信する。
        [self.chkControllerOptional sendImpression:data];
        NSString *url = [data imageIcon];
		dispatch_async(image_queue, ^{
			UIImage *icon = [self.chkControllerOptional getImage:url];
            // TODO:画像イメージをキャッシュする。
            [data setCacheImage:icon]; 
            dispatch_async(main_queue, ^{
                AppViewCell *cell = (AppViewCell *)[tableView cellForRowAtIndexPath:indexPath];
                cell.appImageView.layer.masksToBounds = YES;
                cell.appImageView.layer.cornerRadius = 10.0f;
                cell.appImageView.image = icon;
                [cell.indicator stopAnimating];
            });
		});
	} else {
		cell.appImageView.image = [data cacheImage];
        [cell.indicator stopAnimating];
	}
    
    return cell;
    
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return CELL_APP_IMAGE_SIZE + (CELL_APP_IMAGE_MARGIN*2);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ChkRecordData *data = nil;
    if(indexPath.section==0) {
        data = [self.chkDataList objectAtIndex:[indexPath row]];
    }else{
        data = [self.chkDataListOptional objectAtIndex:[indexPath row]];
    }
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *url = [NSURL URLWithString:data.linkUrl];
    NSLog(@"data.linkUrl:%@",data.linkUrl);
    
    if( [application canOpenURL:url]){
        [application openURL:url];
    }else{
        NSLog(@"openUrl error.");
    }
}

@end
