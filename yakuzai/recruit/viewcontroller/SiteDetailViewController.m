//
//  SiteDetailViewController.m
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "SiteDetailViewController.h"

@interface SiteDetailViewController ()<
UIAlertViewDelegate
>{
    NSObject<SiteDetailViewControllerDelegate>* delegate;
    Site* site;
    UIImageView* icon;
    UILabel* excerptLabel;
    UILabel* contentLabel;
    UIButton* button;
    UIScrollView* sv;
}

@end

@implementation SiteDetailViewController

-(id) initWithDelegate:(NSObject<SiteDetailViewControllerDelegate>*)delegate_ andSite:(Site*)site_{
    self = [super init];
    if(self){
        delegate = delegate_;
        site = site_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    UIView* view = self.view;

    UIView* bgView = [[UIView alloc] initWithFrame:CGRectMake(2, 37, 298, 435.0f)];
    bgView.backgroundColor = UIColorFromHex(0x5fcadd);
    [view addSubview:bgView];
    [self setSv];
    
    // events
    UISwipeGestureRecognizer* reco = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(back)];
    reco.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:reco];
}

-(void) back{
    [self.navigationController popViewControllerAnimated:YES];
    NSNotification *n = [NSNotification notificationWithName:NOTIFICATION_KEY_HIDE_BACK object:self];
    [[NSNotificationCenter defaultCenter] postNotification:n];
}

-(void) setSv{
    UIView* view = self.view;
    float height = 443.0f;
    if([VknDeviceUtil is35Inch]){
        height -= 35;
    }
    CGRect svFrame = CGRectMake(10, 37, 285, height);
    
    sv = [[UIScrollView alloc] initWithFrame:svFrame];
    sv.showsVerticalScrollIndicator = NO;
    
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, 280, 20)];
    titleLabel.text = site.title;
    titleLabel.font = [UIFont boldSystemFontOfSize:20.0f];
    titleLabel.textColor = UIColorFromHex(0x0d5663);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [sv addSubview:titleLabel];
    
    UIView* line = [[UIView alloc] initWithFrame:CGRectMake(0, 37.5, 280, 2)];
    line.backgroundColor = UIColorFromHex(0x0d5663);
    [sv addSubview:line];
    
    icon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 50, 0, 0)];
    icon.contentMode = /*UIViewContentModeScaleAspectFill*/UIViewContentModeScaleAspectFit;
    icon.clipsToBounds = YES;
    icon.layer.borderColor = UIColorFromHex(0x278fa2).CGColor;
    icon.layer.borderWidth = 2;
    icon.userInteractionEnabled = YES;
    [icon addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapIcon)]];
    [sv addSubview:icon];
    
    excerptLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 340, 280, 22)];
    excerptLabel.numberOfLines = 1;
    excerptLabel.font = [UIFont boldSystemFontOfSize:13.0f];
    excerptLabel.text = [NSString stringWithFormat:@"  %@", site.subtitle];
    excerptLabel.backgroundColor = UIColorFromHex(0x0d5663);
    excerptLabel.textColor = [UIColor whiteColor];
    [sv addSubview:excerptLabel];
    
    float y = excerptLabel.frame.size.height + 340 + 20;
    
    contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, y, 280, 1000)];
    contentLabel.font = [UIFont systemFontOfSize:12.0f];
    contentLabel.numberOfLines = 0;
    contentLabel.text = site.content;
//    contentLabel.textColor = UIColorFromHex(0x76452c);
    contentLabel.textColor = UIColorFromHex(0x0d5663);
    [contentLabel sizeToFit];
    [sv addSubview:contentLabel];
    
    y += contentLabel.frame.size.height;
    y += 19;
    
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(50, y, 184, 44.5f);
    button.adjustsImageWhenHighlighted = NO;
    [button setImage:[UIImage imageNamed:@"upButtonBlue"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(tapRegister) forControlEvents:UIControlEventTouchUpInside];
    
    y += 44.5f;
    y += 20;
    
    [sv addSubview:button];
    sv.contentSize = CGSizeMake(280, y);
    
    [view addSubview:sv];
    
    [VknImageCache loadImage:[site contentImageUrl] defaultImage:nil callback:^(UIImage *image, NSString *key, BOOL useCache) {
        image = [VknImageUtil resizeImage:image width:280];
        [VknThreadUtil mainBlock:^{
            icon.image = image;
            [self adjustPosition:image.size.height];
        }];
    }];
}

-(void) adjustPosition:(float)imageHeight{
    float y = 55;
    
    icon.frame = CGRectMake(0, 50, 280, imageHeight);
    y += imageHeight + 10;
    
    CGRect excerptFrame = excerptLabel.frame;
    excerptFrame.origin.y = y;
    excerptLabel.frame = excerptFrame;
    y += excerptFrame.size.height + 20;
    
    CGRect contentFrame = contentLabel.frame;
    contentFrame.origin.y = y;
    contentLabel.frame = contentFrame;
    y += contentFrame.size.height + 20;
    
    CGRect buttonFrame = button.frame;
    buttonFrame.origin.y = y;
    button.frame = buttonFrame;
    y += buttonFrame.size.height + 20;
    sv.contentSize = CGSizeMake(280, y);
}

-(void) reload{
    
}

-(void) tapIcon{
    [YakuUtil pain:icon];
    [self tapRegister];
}

-(void) tapRegister{
    [YakuUtil pain:button];
//    [VknUtils openBrowser:site.linkUrl];
//    [self showToast:@"webView起動"];
    UIAlertView* alertView = [[UIAlertView alloc] init];
    alertView.delegate = self;
    [alertView addButtonWithTitle:@"キャンセル"];
    [alertView addButtonWithTitle:@"OK"];
    alertView.message = @"ブラウザを開きます";
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex != 1) return;
    [VknUtils openBrowser:site.linkUrl];
}

-(void) tapBack{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
