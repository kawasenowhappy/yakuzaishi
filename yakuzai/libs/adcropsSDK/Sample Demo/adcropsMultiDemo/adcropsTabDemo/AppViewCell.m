//
//  AppViewCell.m
//  adcropsTabDemo
//
//  Created by Tatsuya Uemura on 12/02/23.
//  Copyright (c) 2012年 8crops inc. All rights reserved.
//

#import "AppViewCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation AppViewCell

@synthesize titleLabel;
@synthesize detailLabel;
@synthesize appImageView;
@synthesize indicator;

- (void)dealloc {
    NSLog(@"dealloc");
    
    self.titleLabel     = nil;
    self.detailLabel    = nil;
    self.appImageView   = nil;
    self.indicator      = nil;
    
    [super dealloc];
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
		// Imageの設定
		self.appImageView = [[[UIImageView alloc] init]autorelease];
        self.appImageView.backgroundColor = [UIColor blackColor];
        self.appImageView.layer.masksToBounds = YES;
        self.appImageView.layer.cornerRadius = 10.0f;
		self.appImageView.frame = CGRectMake(
										 CELL_APP_IMAGE_MARGIN,
										 CELL_APP_IMAGE_MARGIN,
										 CELL_APP_IMAGE_SIZE,
										 CELL_APP_IMAGE_SIZE);
		[self.contentView addSubview:self.appImageView];
		
        // インジケータ
        self.indicator = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite] autorelease];
        self.indicator.autoresizingMask =
        UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin
        | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        self.indicator.backgroundColor = [UIColor clearColor];
        [self.appImageView addSubview:self.indicator];
        [self.indicator sizeToFit];
        [self.indicator startAnimating];
        self.indicator.frame = CGRectMake(self.appImageView.frame.size.width/3,
                                     self.appImageView.frame.size.height/3,
                                     self.indicator.frame.size.width,
                                     self.indicator.frame.size.height);
        
		// タイトルラベルの設定
		self.titleLabel = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
		self.titleLabel.font = [UIFont boldSystemFontOfSize:13];
		self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.numberOfLines = 1;
        self.titleLabel.adjustsFontSizeToFitWidth = NO;
        [self.titleLabel sizeToFit];
		self.titleLabel.frame = CGRectMake(
									   CELL_APP_IMAGE_SIZE + CELL_APP_IMAGE_MARGIN*2,
									   CELL_APP_IMAGE_MARGIN,
									   self.contentView.frame.size.width - self.appImageView.frame.size.width 
                                       - CELL_APP_IMAGE_MARGIN*3,
                                       15.0);
		[self.contentView addSubview:self.titleLabel];
        
		// 詳細文ラベルの設定
		self.detailLabel = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
		self.detailLabel.font = [UIFont systemFontOfSize:9];
		self.detailLabel.textColor = [UIColor blackColor];
        self.detailLabel.backgroundColor = [UIColor clearColor];
        self.detailLabel.numberOfLines = 3;
        self.detailLabel.adjustsFontSizeToFitWidth = NO;
        [self.detailLabel sizeToFit];
		
		self.detailLabel.frame = CGRectMake(
                                           CELL_APP_IMAGE_SIZE + CELL_APP_IMAGE_MARGIN*2,
                                           CELL_APP_IMAGE_MARGIN + self.titleLabel.frame.size.height,
                                            self.contentView.frame.size.width - self.appImageView.frame.size.width 
                                            - CELL_APP_IMAGE_MARGIN*3, 
                                           self.contentView.frame.size.height);
		[self.contentView addSubview:self.detailLabel];
	}
	return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
