//
//  RecommendContents.h
//  yakuzai
//
//  Created by kawase yu on 2014/07/12.
//  Copyright (c) 2014年 nowhappy. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RecommendContentsDelegate <NSObject>

-(void) onContentsShow:(int)index;

@end

@interface RecommendContents : NSObject

-(id) initWithDelegate:(NSObject<RecommendContentsDelegate>*)delegate_;
-(UIView*) getView;
-(void) hide;
-(void) hide2;

-(void) hasNew;

@end
