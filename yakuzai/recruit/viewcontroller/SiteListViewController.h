//
//  SiteListViewController.h
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "BaseViewController.h"
#import "SiteList.h"

@protocol SiteListViewControllerDelegate <NSObject>

-(void) onSelectSite:(Site*)site;

@end

@interface SiteListViewController : BaseViewController

-(id) initWithDelegate:(NSObject<SiteListViewControllerDelegate>*)delegate_;

@end
