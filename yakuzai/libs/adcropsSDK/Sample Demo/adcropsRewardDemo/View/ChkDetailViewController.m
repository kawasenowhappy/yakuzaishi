//
//  ChkDetailViewController.m
//  8chk
//
//  Created by 8crops Inc. on 11/06/20.
//  Copyright 2011 8crops Inc. All rights reserved.
//

#import "ChkDetailViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ChkViewSetting.h"

@implementation ChkDetailViewController

@synthesize chkView;
@synthesize titleLabel;
@synthesize backgroudView;
@synthesize appData;
@synthesize appImageView;
@synthesize condition;
@synthesize conditionLabel;
@synthesize detail;
@synthesize detailLabel;
@synthesize appNameLabel;
@synthesize downloadButton;
@synthesize ic_price;
@synthesize price;
@synthesize ic_point;
@synthesize point;
@synthesize chkConfig;

#pragma mark -
#pragma mark private method
-(void) closebtnDidPush {
	[self.view removeFromSuperview];
}

-(void) downloadBtnDidPush {
	UIApplication *application = [UIApplication sharedApplication];
	NSURL *url = nil;

    if([self.appData isInstalled]) {
        // インストール済みアラート
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"既にインストール済みです。" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];
        return;
    }else{
        url = [NSURL URLWithString:[self.appData linkUrl]];
    }
    NSLog(@"downloadBtnDidPush URL:%@",url);
	if( [application canOpenURL:url]){
		[application openURL:url];
	}else{
		NSLog(@"not open");
	}
}

#pragma mark -
#pragma mark Initialization
- (id)initWithConfig:(ChkConfig*)config{
	if ((self = [super init])) {
        NSLog(@"initWithConfig:%@",config);
        self.chkConfig = config;
	}
	return self;
}

#pragma mark -
#pragma mark view delegate
- (void)loadView {
	// viewを作成する
	self.chkView = [[[ChkView alloc] initWithConfig:self.chkConfig] autorelease];
	self.view = self.chkView;	

	// add title
	CGFloat titleLabelFontSize = 14;
	self.titleLabel = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
	self.titleLabel.text = [self.appData title];
	self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.textColor = [ChkView convertHtmlColorToUIColor:CHK_VIEW_APP_TITLE_TEXT_COLOR];
	self.titleLabel.font = [UIFont boldSystemFontOfSize:titleLabelFontSize];
	self.titleLabel.textAlignment = UITextAlignmentCenter;
	self.titleLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
	[self.view addSubview:self.titleLabel];
	
	// 背景ビューを追加する
	self.backgroudView = [[[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame] autorelease];
    self.backgroudView.backgroundColor = [ChkView convertHtmlColorToUIColor:CHK_VIEW_APP_BACKGROUND_COLOR];
	[self.view addSubview:self.backgroudView];
	
	// アプリイメージ
	self.appImageView = [[[UIImageView alloc] initWithImage:[self.appData cacheImage]] autorelease];
    // 角を丸くする
    self.appImageView.layer.masksToBounds = YES;
    self.appImageView.layer.cornerRadius = 13.5f;

	[self.view addSubview:self.appImageView];
	
	// アプリ名
	self.appNameLabel = [[[UILabel alloc] init] autorelease];
	self.appNameLabel.text = [self.appData title];
	self.appNameLabel.font = [UIFont boldSystemFontOfSize:12];
	self.appNameLabel.backgroundColor = [UIColor clearColor];
	self.appNameLabel.textColor = [UIColor blackColor];
    self.appNameLabel.adjustsFontSizeToFitWidth = YES;
    self.appNameLabel.minimumFontSize = 10;
    self.appNameLabel.numberOfLines = 2;
	[self.view addSubview:self.appNameLabel];
	
	// priceの設定
	self.ic_price = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"8c_money.png"]] autorelease];
	[self.view addSubview:self.ic_price];
	self.price = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
	self.price.font = [UIFont boldSystemFontOfSize:14];
	self.price.textColor = [UIColor blueColor];
	self.price.textAlignment = UITextAlignmentLeft;
	self.price.backgroundColor = [UIColor clearColor];
    NSMutableString *priceStr = [NSMutableString stringWithFormat:@"%@円",[[self.appData price] stringValue]];
	self.price.text = priceStr;
	[self.view addSubview:self.price];
	
	// pointの設定
	self.ic_point = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"8c_point.png"]] autorelease];
	[self.view addSubview:self.ic_point];
	self.point = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
	self.point.font = [UIFont boldSystemFontOfSize:14];
	self.point.textColor = [UIColor blueColor];
	self.point.textAlignment = UITextAlignmentLeft;
    NSMutableString *pointStr = [NSMutableString stringWithFormat:@"%@Pt",[[self.appData point]stringValue]];
	self.point.text = pointStr;
	self.point.backgroundColor = [UIColor clearColor];
	[self.view addSubview:self.point];
	
	// ダウンロードORインストール済ボタン作成
	self.downloadButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    if([self.appData isInstalled]) {
        [self.downloadButton setTitle:@"インストール済" forState:UIControlStateNormal];
    }else{
        [self.downloadButton setTitle:@"ダウンロード" forState:UIControlStateNormal];
    }
	self.downloadButton.titleLabel.font = [UIFont boldSystemFontOfSize:12];
	self.downloadButton.titleLabel.textColor = [UIColor darkGrayColor];
	[self.downloadButton sizeToFit];
	[self.downloadButton addTarget:self action:@selector(downloadBtnDidPush) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:self.downloadButton];
	
	// 条件
	self.conditionLabel = [[[UILabel alloc] init] autorelease];
    self.conditionLabel.text = @"条件";
	self.conditionLabel.font = [UIFont boldSystemFontOfSize:12];
	self.conditionLabel.backgroundColor = [UIColor clearColor];
	self.conditionLabel.textColor = [UIColor darkGrayColor];
	[self.view addSubview:self.conditionLabel];
    
	self.condition = [[[UITextView alloc] init] autorelease];
	self.condition.text = [self.appData cvCondition];
	self.condition.editable = NO;
	self.condition.textAlignment = UITextAlignmentLeft;
	self.condition.layer.borderWidth = 2;
	self.condition.layer.borderColor = [[UIColor lightGrayColor] CGColor];
	self.condition.layer.cornerRadius = 8;
	[self.view addSubview:self.condition];

	// 詳細
	self.detailLabel = [[[UILabel alloc] init] autorelease];
    self.detailLabel.text = @"詳細";
	self.detailLabel.font = [UIFont boldSystemFontOfSize:12];
	self.detailLabel.backgroundColor = [UIColor clearColor];
	self.detailLabel.textColor = [UIColor darkGrayColor];
	[self.view addSubview:self.detailLabel];
    
	self.detail = [[[UITextView alloc] init] autorelease];
	self.detail.text = [self.appData detail];
	self.detail.editable = NO;
	self.detail.textAlignment = UITextAlignmentLeft;
	self.detail.layer.borderWidth = 2;
	self.detail.layer.borderColor = [[UIColor lightGrayColor] CGColor];
	self.detail.layer.cornerRadius = 8;
	[self.view addSubview:self.detail];
	
	// ボタン作成
	UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	[closeButton setTitle:@"close" forState:UIControlStateNormal];
	[closeButton sizeToFit];
	CGPoint newPoint = self.view.center;
	newPoint.y += 150;
    newPoint.x += -10;
	closeButton.center = newPoint;
	[closeButton addTarget:self action:@selector(closebtnDidPush) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:closeButton];
	
}

- (BOOL) isiPhone5 {
    // 画面取得
    UIScreen *screen = [UIScreen mainScreen];
    // ステータスバー無しのサイズ
    CGRect rect = screen.applicationFrame;
    if(rect.size.height >= 548 ) {
        return YES;
    }
    return NO;
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[super viewDidLoad];
	[self.titleLabel sizeToFit];
	self.titleLabel.frame = CGRectMake(
								   RVIEW_BORDER_WIDTH,
								   RVIEW_BORDER_WIDTH,
								   self.view.frame.size.width - RVIEW_BORDER_WIDTH*2,
								   self.titleLabel.frame.size.height + RVIEW_TITLE_MARGIN);
    
	self.backgroudView.frame = CGRectMake(
									  RVIEW_BORDER_WIDTH +1,
									  RVIEW_BORDER_WIDTH + self.titleLabel.frame.size.height,
									  self.view.frame.size.width - RVIEW_BORDER_WIDTH*2 -2,
									  self.view.frame.size.height - RVIEW_BORDER_WIDTH*2 - self.titleLabel.frame.size.height);

	self.appImageView.frame = CGRectMake(
									 RVIEW_BORDER_WIDTH + 10, RVIEW_BORDER_WIDTH + self.titleLabel.frame.size.height + 10,
									 80,80);

	self.appNameLabel.frame = CGRectMake(
									 self.appImageView.frame.origin.x + self.appImageView.frame.size.width + 10,
									 RVIEW_BORDER_WIDTH + self.titleLabel.frame.size.height + 10,
									 170,
                                         30);
//	[self.appNameLabel sizeToFit];
	
	self.ic_price.frame = CGRectMake(
								 self.appNameLabel.frame.origin.x,
								 self.appNameLabel.frame.origin.y + self.appNameLabel.frame.size.height + 10,
								 14,
								 14);
	
	self.price.frame = CGRectMake(
							  self.ic_price.frame.origin.x + self.ic_price.frame.size.width + 5,
							  self.ic_price.frame.origin.y,
							  0,
							  0);
	[self.price sizeToFit];
	
	self.ic_point.frame = CGRectMake(
								 self.price.frame.origin.x + self.price.frame.size.width + 10,
								 self.ic_price.frame.origin.y,
								 14,
								 14);
	
	self.point.frame = CGRectMake(
							   self.ic_point.frame.origin.x + self.ic_point.frame.size.width + 5,
							   self.ic_point.frame.origin.y,
							   0,
							   0);
	[self.point sizeToFit];
	
	[self.downloadButton sizeToFit];
	self.downloadButton.frame = CGRectMake(
									   self.appNameLabel.frame.origin.x,
									 self.price.frame.origin.y + self.price.frame.size.height + 10,
									 0,0);
	[self.downloadButton sizeToFit];

	self.conditionLabel.frame = CGRectMake(
								  RVIEW_BORDER_WIDTH + 10,
								  self.appImageView.frame.origin.y + self.appImageView.frame.size.height + 10,
								  self.backgroudView.frame.size.width - 20,
								  0);
	[self.conditionLabel sizeToFit];

	self.condition.frame = CGRectMake(
								  RVIEW_BORDER_WIDTH + 10,
								  self.conditionLabel.frame.origin.y + self.conditionLabel.frame.size.height + 10,
								  self.backgroudView.frame.size.width - 20,
								  80);

	self.detailLabel.frame = CGRectMake(
									   RVIEW_BORDER_WIDTH + 10,
									   self.condition.frame.origin.y + self.condition.frame.size.height + 10,
									   self.backgroudView.frame.size.width - 20,
									   0);
	[self.detailLabel sizeToFit];
	
    if([self isiPhone5]) {
        self.detail.frame = CGRectMake(
                                       RVIEW_BORDER_WIDTH + 10,
                                       self.detailLabel.frame.origin.y + self.detailLabel.frame.size.height + 10,
                                       self.backgroudView.frame.size.width - 20,
                                       130);
    }else{
        self.detail.frame = CGRectMake(
								  RVIEW_BORDER_WIDTH + 10,
								  self.detailLabel.frame.origin.y + self.detailLabel.frame.size.height + 10,
								  self.backgroudView.frame.size.width - 20,
								  80);
	}
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

#pragma mark -
#pragma mark Memory management
- (void)dealloc {
    NSLog(@"***** dealloc ChkDetailViewController");
    self.chkView = nil;
    self.titleLabel = nil;
    self.backgroudView = nil;
    self.appData = nil;
    self.appImageView = nil;
    self.condition = nil;
    self.conditionLabel = nil;
    self.detail = nil;
    self.detailLabel = nil;
    self.appNameLabel = nil;
    self.downloadButton = nil;
    self.ic_price = nil;
    self.price = nil;
    self.ic_point = nil;
    self.point = nil;
    self.chkConfig = nil;
    
    [super dealloc];
}


@end
