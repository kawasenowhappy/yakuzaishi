//
//  UpdateTrigger.m
//  adcropsTabDemo
//
//  Created by Tatsuya Uemura on 12/02/23.
//  Copyright (c) 2012年 8crops inc. All rights reserved.
//

#import "UpdateTrigger.h"


@implementation UpdateTrigger
@synthesize	delegte;

-(void)adjust:(CGFloat)contentsHeight {
    NSLog(@"UpdateTrigger adjust");
	CGRect frame = tableView.tableFooterView.frame;
	frame.size.height = tableView.frame.size.height - contentsHeight;
	if (frame.size.height < 40)
		frame.size.height = 40;
	tableView.tableFooterView.frame = frame;
	UIView* v = tableView.tableFooterView;
	[v retain];
	tableView.tableFooterView = nil;
	tableView.tableFooterView = v;
	[v release];
}
	
-(void)adapteTrigger:(UITableView*)inTableView view:(UIView*)view{
	tableView = inTableView;
	CGRect r = view.bounds;
	r.origin.y -= 40;
	r.size.height = 40;
	triggerHeader = [[UILabel alloc] initWithFrame:r]; 
    
	triggerHeader.text = @"更新する";
	triggerHeader.textAlignment = UITextAlignmentCenter;
    triggerHeader.backgroundColor = [UIColor clearColor];
    [view addSubview:triggerHeader];

	//	トリガーのイメージを埋め込むtriggerHeader
	UIImageView* imageview = [[UIImageView alloc] initWithFrame:CGRectMake(8, (40 - 16) / 2, 16, 16)];
	imageview.image = [UIImage imageNamed:@"arrow.png"];
	imageview.tag = 1;
	[triggerHeader addSubview:imageview];
	[imageview release];

	headerOn = NO;
	
	[triggerHeader release];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
	dragging = YES;
	headerOn = NO;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
	if (dragging == NO)
		return;
	//	ヘッダー側
	CGRect r = tableView.bounds;
    
	if ((r.origin.y < -50) && (headerOn == NO)) {
		headerOn = YES;
		UIImageView* imageview = (UIImageView*)[triggerHeader viewWithTag:1];
		[UIView beginAnimations:nil context:nil];
		imageview.transform = CGAffineTransformRotate(CGAffineTransformIdentity, 3.14);
		[UIView commitAnimations];
		triggerHeader.text = @"手を離すと更新します";
	}
	if ((r.origin.y > -50) && (headerOn == YES)) {
		headerOn = NO;
		UIImageView* imageview = (UIImageView*)[triggerHeader viewWithTag:1];
		[UIView beginAnimations:nil context:nil];
		imageview.transform = CGAffineTransformIdentity;
		[UIView commitAnimations];
		triggerHeader.text = @"更新する";
	}
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
	printf("%s\n", headerOn?"headerOn":"headerOff");
	dragging = NO;
	triggerHeader.text = @"更新する";
    UIImageView* imageview = (UIImageView*)[triggerHeader viewWithTag:1];
    [UIView beginAnimations:nil context:nil];
    imageview.transform = CGAffineTransformIdentity;
    [UIView commitAnimations];
    
	if (delegte && headerOn) {
		if ([delegte respondsToSelector:@selector(update:)]) {
			[delegte update:(headerOn == YES)];
		}
	}
}

@end
