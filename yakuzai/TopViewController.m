//
//  TopViewController.m
//  yakuzai
//
//  Created by kawase yu on 2014/07/05.
//  Copyright (c) 2014年 nowhappy. All rights reserved.
//

#import "TopViewController.h"
#import "SelectButton.h"

@interface TopViewController ()<
SelectButtonDelegate
, UIAlertViewDelegate
>{
    __weak NSObject<TopViewDelegate>* delegate;
    UIView* contentsView;
    UIView* formView;
    UIView* resultView;
    UIImageView* bgView;
    UIImageView* ttl;
    NSArray* closeButtonList;
    UIButton* backButton;
    NSArray* selectButtonList;
    UILabel* nextLabel;
    UILabel* currentLabel;
    Site* currentSite;
}

@end

@implementation TopViewController

-(id) initWithDelegate:(NSObject<TopViewDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    [self setBg];
    [self setXButton];
    [self setBackButton];
    [self setForm];
    [self setResult];
    
    // events
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(showBack) name:NOTIFICATION_KEY_SHOW_BACK object:nil];
    [nc addObserver:self selector:@selector(hideBack) name:NOTIFICATION_KEY_HIDE_BACK object:nil];
}

-(void) setBg{
    UIView* view = self.view;
    view.frame = CGRectMake(0, 0, 320, [self showHeight]+26.5f);
    view.clipsToBounds = YES;
    
    UIImage* kamiImage = [UIImage imageNamed:@"kamiWhite"];
    kamiImage = [kamiImage stretchableImageWithLeftCapWidth:7 topCapHeight:7];
    
    bgView = [[UIImageView alloc] initWithImage:kamiImage];
    bgView.frame = CGRectMake(10, 26.5f, 304, [self showHeight]);
    [view addSubview:bgView];
    
    UIImageView* pin = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pin"]];
    pin.frame = CGRectMake(92, 21, 136, 39.5f);
    [view addSubview:pin];
    
    ttl = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ttlLabel"]];
    ttl.frame = CGRectMake(30, 60, 260, 38);
    [view addSubview:ttl];
}

-(void) setXButton{
    NSMutableArray* tmp = [[NSMutableArray alloc] init];
    CGRect buttonFrame = CGRectMake(275, 30, 34.5f, 34.5f);
    for(int i=1;i<=3;i++){
        NSString* imageName = [NSString stringWithFormat:@"xButton%d", /*i*/1];
        UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(tapXButton:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = buttonFrame;
        button.hidden = YES;
        button.tag = i;
        [self.view addSubview:button];
        [tmp addObject:button];
    }
    
    closeButtonList = [NSArray arrayWithArray:tmp];
}

-(void) tapXButton:(UIButton*)button{
    [self hideBack];
    [delegate hideContents:button.tag];
    button.hidden = YES;
    
    [UIView animateWithDuration:0.2f animations:^{
        ttl.alpha = 1.0f;
        self.view.frame = CGRectMake(0, 0, 320, [self showHeight]+26.5f);
        bgView.frame = CGRectMake(10, 26.5f, 304, [self showHeight]);
    } completion:^(BOOL finished) {
        
    }];
}

-(void) setBackButton{
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    160 × 69
    backButton.frame = CGRectMake(10, 30, 80, 34.5f);
    backButton.alpha = 0.0f;
    [backButton setImage:[UIImage imageNamed:@"backButton"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(tapBack:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
}

-(void) tapBack:(UIButton*)button{
    [YakuUtil pain:button];
    // 通知を作成する
    NSNotification *n = [NSNotification notificationWithName:NOTIFICATION_KEY_BACK object:self];
    [[NSNotificationCenter defaultCenter] postNotification:n];
    
    [self hideBack];
}

-(float) showHeight{
    float height = 336.0f;
    if([VknDeviceUtil is35Inch]){
        height -= 35;
    }
    return height;
}

-(float) hideHeight{
    return 65.0f - 23.5f;
}

-(void) setSv{
//    sv = [[UIScrollView alloc] initWithFrame:CGRectMake(10, 23.5f+2, 304, [VknDeviceUtil windowHeight]-77-10)];
//    [self.view addSubview:sv];
}

-(void) setForm{
    formView = [[UIView alloc] initWithFrame:CGRectMake(30.0f, 60, 240, 275)];
    [self.view addSubview:formView];
    
    //    392 × 253
    UIImageView* labels = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ttlInfo"]];
    labels.frame = CGRectMake(0, 38, 260, 85.5f-38.0f);
    [formView addSubview:labels];
    
    CGRect buttonFrame = CGRectMake(10, 65, 240, 25);
    NSMutableArray* tmp = [[NSMutableArray alloc] init];
    for(int i=0;i<4;i++){
        SelectButton* button = [SelectButton create:self index:i];
        button.frame = buttonFrame;
        [formView addSubview:button];
        buttonFrame.origin.y += 40.0f;
        [tmp addObject:button];
    }
    
    selectButtonList = [NSArray arrayWithArray:tmp];
    
    UIButton* checkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [checkButton setImage:[UIImage imageNamed:@"checkButton"] forState:UIControlStateNormal];
    [checkButton addTarget:self action:@selector(tapCheck:) forControlEvents:UIControlEventTouchUpInside];
    checkButton.adjustsImageWhenHighlighted = NO;
    
    float y = 230;
    if([VknDeviceUtil is35Inch]){
        y -= 13;
    }
    checkButton.frame = CGRectMake(10, y, 242, 42);
    [formView addSubview:checkButton];
}

-(void) setResult{
    resultView = [[UIView alloc] initWithFrame:CGRectMake(30.0f, 60, 290, 290)];
    resultView.alpha = 0;
    [self.view addSubview:resultView];
    UIImageView* bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"resultBg"]];
    bg.frame = CGRectMake(10, 48, 240, 159.5f);
    [resultView addSubview:bg];
    
    nextLabel = [self createResultLabtl:@"530万6230円です。" y:48+51];
    [resultView addSubview:nextLabel];
    
    currentLabel = [self createResultLabtl:@"+120万1600円です。" y:48+120];
    [resultView addSubview:currentLabel];
    
    UIButton* upButton = [UIButton buttonWithType:UIButtonTypeCustom];
    float y = 230;
    if([VknDeviceUtil is35Inch]){
        y -= 15;
    }
    upButton.frame = CGRectMake(40, y, 182, 42);
    [upButton setImage:[UIImage imageNamed:@"upButton"] forState:UIControlStateNormal];
    [upButton addTarget:self action:@selector(tapUp:) forControlEvents:UIControlEventTouchUpInside];
    [resultView addSubview:upButton];
    
    UIButton* xButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    y = 254;
    if([VknDeviceUtil is35Inch]){
        y -= 35;
    }
    xButton.frame = CGRectMake(236, y, 44, 44);
    [xButton setImage:[UIImage imageNamed:@"resultXButton"] forState:UIControlStateNormal];
    [xButton addTarget:self action:@selector(tapX:) forControlEvents:UIControlEventTouchUpInside];
    [resultView addSubview:xButton];
}

-(UILabel*) createResultLabtl:(NSString*)text y:(float)y{
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 240, 40)];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont boldSystemFontOfSize:22.0f];
    label.textColor = [UIColor whiteColor];
    label.text = text;
    
    return label;
}

-(void) tapUp:(UIButton*)button{
    [YakuUtil pain:button];
    
    SiteList* siteList = [YakuUtil siteList];
    
    int r = [VknUtils randIntRange:NSMakeRange(0, [siteList count]-1)];
    currentSite = [siteList get:r];
    
    UIAlertView* alertView = [[UIAlertView alloc] init];
    alertView.delegate = self;
    [alertView addButtonWithTitle:@"キャンセル"];
    [alertView addButtonWithTitle:@"OK"];
    alertView.message = @"ブラウザを開きます";
    [alertView show];
}

-(void) tapX:(UIButton*)button{
    [YakuUtil pain:button];
    
    formView.transform = CGAffineTransformMakeTranslation(-20, 0);
    [UIView animateWithDuration:0.2f animations:^{
        resultView.alpha = 0;
        resultView.transform = CGAffineTransformMakeTranslation(20, 0);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2f animations:^{
            formView.transform = CGAffineTransformMakeTranslation(0, 0);
            formView.alpha = 1.0f;
        } completion:^(BOOL finished) {
            
        }];
    }];
}

-(void) tapCheck:(UIButton*)button{
    [YakuUtil pain:button];
    
    // selected check
    int ageIndex = 0;
    int prefIndex = 0;
    int workIndex = 0;
    int currentIndex = 0;
    int i=0;
    for(SelectButton* button in selectButtonList){
        if([button getIndex] == -1){
            UIAlertView* alertView = [[UIAlertView alloc] init];
            alertView.tag = 1;
            alertView.title = [NSString stringWithFormat:@"「%@」を選択してください", [button labelText]];
            [alertView addButtonWithTitle:@"OK"];
            [alertView show];
            return;
        }
        
        if(i == 0) ageIndex = [button getIndex];
        if(i == 1) prefIndex = [button getIndex];
        if(i == 2) workIndex = [button getIndex];
        if(i == 3) currentIndex = [button getIndex];
        i++;
    }
    
    int tekisei = [YakuUtil tekisei:ageIndex prefIndex:prefIndex workIndex:workIndex];
    int current = [YakuUtil current:currentIndex];
    int sa = abs(tekisei-current);
    
    NSString* tekiseiStr = [YakuUtil priceToString:tekisei];
    NSString* saStr = [YakuUtil priceToString:sa];
    
    if(current > tekisei){
        saStr = [NSString stringWithFormat:@"- %@", saStr];
    }else{
        saStr = [NSString stringWithFormat:@"+ %@", saStr];
    }
    
    nextLabel.text = tekiseiStr;
    currentLabel.text = saStr;
    
    resultView.transform = CGAffineTransformMakeTranslation(20, 0);
    [UIView animateWithDuration:0.2f animations:^{
        formView.alpha = 0;
        formView.transform = CGAffineTransformMakeTranslation(-20, 0);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2f animations:^{
            resultView.transform = CGAffineTransformMakeTranslation(0, 0);
            resultView.alpha = 1.0f;
        } completion:^(BOOL finished) {
            
        }];
    }];
}

-(UIView*)getView{
    return self.view;
}

-(void) hide:(int)contentsIndex{
    UIButton* xButton =  closeButtonList[contentsIndex-1];
    xButton.hidden = NO;
    
    ttl.alpha = 0;
    CGRect frame = CGRectMake(0, 0, 320, [self hideHeight]+26.5f);
    CGRect bgFrame =  CGRectMake(10, 26.5f, 304, [self hideHeight]);
    
    [UIView animateWithDuration:0.2f animations:^{
        self.view.frame = frame;
        bgView.frame = bgFrame;
    } completion:^(BOOL finished) {
        
    }];
}

-(void) showBack{
    [UIView animateWithDuration:0.2f animations:^{
        backButton.alpha = 1.0f;
    }];
}

-(void) hideBack{
    [UIView animateWithDuration:0.2f animations:^{
        backButton.alpha = 0.0f;
    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex != 1) return;
    [VknUtils openBrowser:currentSite.linkUrl];
}

@end
