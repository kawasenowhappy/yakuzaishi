//
//  NewsCell.m
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "NewsCell.h"

@interface NewsCell (){
    News* news;
    UILabel* titleLabel;
    UIView* titleBg;
    UILabel* dateLabel;
    UILabel* excerptLabel;
    UIImageView* arrow;
    
    UIView* overView;
}

@end

@implementation NewsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initialize];
    }
    return self;
}

-(void) initialize{
    self.backgroundColor = [UIColor clearColor];
    UIView* view = self.contentView;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    overView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 80.5f)];
    overView.backgroundColor = UIColorFromHex(0xffb8d6);
    overView.alpha = 0;
    [view addSubview:overView];
    
    UIView* wrapView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 82)];
    [view addSubview:wrapView];
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, 275, 18)];
    titleLabel.font = [UIFont boldSystemFontOfSize:13];
    titleBg = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 280, 18)];
    titleBg.alpha = 0.5f;
    [wrapView addSubview:titleBg];
    [wrapView addSubview:titleLabel];
    
    excerptLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 34, 255, 40)];
    excerptLabel.font = [UIFont systemFontOfSize:11.0f];
    excerptLabel.numberOfLines = 3;
    excerptLabel.textColor = UIColorFromHex(0xc33250);
    [wrapView addSubview:excerptLabel];
    
    UIImageView* line  = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lineRed"]];
    line.frame = CGRectMake(0, 80.5f, 280, 2);
    [wrapView addSubview:line];
    
//    dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(175, 80, 80, 20)];
//    dateLabel.textAlignment = NSTextAlignmentRight;
//    dateLabel.font = [UIFont systemFontOfSize:11];
//    [wrapView addSubview:dateLabel];
    
    arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrowRed"]];
    arrow.frame = CGRectMake(280-13, 44, 13, 21.5f);
    [wrapView addSubview:arrow];
}

-(void) reload:(News*)news_{
    news = news_;
    titleLabel.text = [NSString stringWithFormat:@"%@", news.title];
//    dateLabel.text = [news dateStr];
    excerptLabel.text = news.excerpt;
    
    // 奇特
    if([news isReaded]){
        arrow.image = [UIImage imageNamed:@"readedArrowRed"];
        titleBg.backgroundColor = UIColorFromHex(0xee759f);
//        dateLabel.textColor = UIColorFromHex(0xb28a8a);
        titleLabel.textColor = UIColorFromHex(0xffb5d4);
        excerptLabel.textColor = UIColorFromHex(0xffb6d5);
        return;
    }
    
    // 未読
    titleLabel.textColor = UIColorFromHex(0xffffff);
    titleBg.backgroundColor = UIColorFromHex(0xda5c82);
//    dateLabel.textColor = UIColorFromHex(0xfd9f9f);
    excerptLabel.textColor = UIColorFromHex(0xc33250);
    arrow.image = [UIImage imageNamed:@"arrowRed"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) light{
    overView.alpha = 0.5f;
    SEL sel = @selector(updateLight:);
    [Tween addTween:self tweenId:99 startValue:0.5 endValue:0.0 time:0.3 delay:0 easing:@"easeInCirc" startSEL:sel updateSEL:sel endSEL:sel];
}

-(void) updateLight:(TweenObject*)tween{
    double val = tween.currentValue;
    overView.alpha = val;
}

@end
