//
//  SecondListViewController.h
//  adcropsTabDemo
//
//  Created by Tatsuya Uemura on 12/02/23.
//  Copyright (c) 2012年 8crops inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChkControllerDelegate.h"
#import "ChkController.h"

#import "UpdateTrigger.h"

@interface SecondListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource,ChkControllerDelegate,UpdateTriggerDelegate>
{
    UITableViewController               *appTableOptional;
    UINavigationController              *naviControllerOptional;
    NSMutableArray                      *chkDataListOptional;
    UIActivityIndicatorView             *indicatorOptional;
    ChkController                       *chkControllerOptional;
    UpdateTrigger                       *updateTriggerOptional;
}

@property(retain, nonatomic) UITableViewController      *appTableOptional;
@property(retain, nonatomic) UINavigationController     *naviControllerOptional;
@property(retain, nonatomic) NSMutableArray             *chkDataListOptional;
@property(retain, nonatomic) UIActivityIndicatorView    *indicatorOptional;
@property(retain, nonatomic) ChkController              *chkControllerOptional;
@property(retain, nonatomic) UpdateTrigger              *updateTriggerOptional;

@end
