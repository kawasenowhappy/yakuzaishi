//
//  AppDelegate.m
//  adcropsInterstitialDemo
//
//  Created by sazawayuki on 2013/11/14.
//  Copyright (c) 2013年 8crops inc. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.viewController = [[ViewController alloc] init];
    self.window.rootViewController = self.viewController;
    
    // 必須：初期化を行います。
    ChkInterstitial *chkInterstital = [ChkInterstitial sharedChkInterstitial];
    // 使用する設定ファイル’を変更したい場合は以下のように初期化を行います。
    //chkInterstital  *chkInterstital = [ChkInterstitial sharedChkInterstitial:ChkPropertiyFileOptional1];
    
    // suid,sadを付与する場合は以下を設定します。
    ChkConfig   *config = [[ChkConfig alloc] init];
    config.suid = @"suid";
    config.sad = @"sad";
    [chkInterstital setConfig:config];
    
    // delegate実装が必要な場合は、以下を設定します。
    chkInterstital.delegate = self;
    
    // フォアグランドに移った際にインタースティシャルを表示しないようにするには以下を設定します。(デフォルトはNO)
    //chkInterstital.showInterstitialInForegroundSession = NO;
    
    // 必須：セッションを開始します。
    [chkInterstital startSession];
    
    [self.window makeKeyAndVisible];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [ChkApplicationOptional applicationDidEnterBackground:application];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    [ChkApplicationOptional applicationWillEnterForeground];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)chkViewDidDisappearInterstitial:(NSString *)tag {
    NSLog(@"chkViewDidDisappearInterstitial:%@",tag);
}

-(void)chkView:(UIView *)chkView willAppearInterstitial:(NSString *)tag {
    NSLog(@"willAppearInterstitial:%@:%@",chkView,tag);
    
    chkView.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.5f,0.5f);
    
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         chkView.transform = CGAffineTransformScale(CGAffineTransformIdentity,1.1f,1.1f);
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:0.1
                                               delay:0.0
                                             options:UIViewAnimationOptionCurveEaseOut
                                          animations:^{
                                              chkView.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.9f,0.9f);
                                          }
                                          completion:^(BOOL finished) {
                                              [UIView animateWithDuration:0.1
                                                                    delay:0.0
                                                                  options:UIViewAnimationOptionCurveEaseOut
                                                               animations:^{
                                                                   chkView.transform = CGAffineTransformScale(CGAffineTransformIdentity,1.0f,1.0f);
                                                               }
                                                               completion:nil
                                               ];
                                              
                                          }
                          ];
                         
                     }
     ];

}



-(void)chkView:(UIView *)chkView didCloseInterstitial:(NSString *)tag {
    NSLog(@"didCloseInterstitial:%@:%@",chkView,tag);
    
    // 非表示アニメーションをつける。
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.2f];    // 時間の指定
    [chkView setAlpha:0.0f];    // アルファチャンネルを0.0fに
    chkView.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.0f,0.0f);
    
}

- (void)chkViewDidClickInterstitial:(NSString *)tag {
    NSLog(@"chkViewDidClickInterstitial:%@",tag);
}

// インタースティシャル表示でエラーが発生した時に呼び出される物
- (void)chkViewDidFailToLoadInterstitial:(NSString *)tag {
    NSLog(@"chkViewDidFailToLoadInterstitial:%@",tag);
}

@end
