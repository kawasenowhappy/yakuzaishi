//
//  RootViewController.h
//  yakuzai
//
//  Created by kawase yu on 2014/07/05.
//  Copyright (c) 2014年 nowhappy. All rights reserved.
//

#import "BaseViewController.h"

@interface RootViewController : BaseViewController

-(void) startAnimation;
-(UIView*) contentView;

@end
