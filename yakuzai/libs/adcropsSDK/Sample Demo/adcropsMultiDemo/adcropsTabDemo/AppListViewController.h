//
//  AppListViewController.h
//  adcropsTabDemo
//
//  Created by Tatsuya Uemura on 12/02/23.
//  Copyright (c) 2012年 8crops inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChkControllerDelegate.h"
#import "ChkController.h"

#import "UpdateTrigger.h"

@interface AppListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource,ChkControllerDelegate,UpdateTriggerDelegate>
{
    UITableViewController               *appTable;
    UINavigationController              *naviController;
    NSMutableArray                      *chkDataList;
    UIActivityIndicatorView             *indicator;
    ChkController                       *chkController;
    NSMutableArray                      *chkDataListOptional;
    ChkController                       *chkControllerOptional;
    UpdateTrigger                       *updateTrigger;
}

@property(retain, nonatomic) UITableViewController      *appTable;
@property(retain, nonatomic) UINavigationController     *naviController;
@property(retain, nonatomic) NSMutableArray             *chkDataList;
@property(retain, nonatomic) UIActivityIndicatorView    *indicator;
@property(retain, nonatomic) ChkController              *chkController;
@property(retain, nonatomic) NSMutableArray             *chkDataListOptional;
@property(retain, nonatomic) ChkController              *chkControllerOptional;
@property(retain, nonatomic) UpdateTrigger              *updateTrigger;

@end
