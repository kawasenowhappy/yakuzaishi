//
//  RecommendContents.m
//  yakuzai
//
//  Created by kawase yu on 2014/07/12.
//  Copyright (c) 2014年 nowhappy. All rights reserved.
//

#import "RecommendContents.h"
#import "RecommendListViewController.h"

@interface RecommendContents ()<
RecommendListViewControllerDelegate
, UIAlertViewDelegate
>{
    UIView* view;
    NSObject<RecommendContentsDelegate>* delegate;
    UIButton* showButton;
    UIImageView* bgView;
    UIImageView* newIcon;
    UINavigationController* navigationController;
    Recommend* currentRecommend;
}

@end

@implementation RecommendContents

-(id) initWithDelegate:(NSObject<RecommendContentsDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
//    605, 891
    CGRect frame = [self baseFrame];
    frame.size.height = [self hideHeight];
    view = [[UIView alloc] initWithFrame:frame];
    UIImage* bgImage = [UIImage imageNamed:@"kamiYellow"];
    bgImage = [bgImage stretchableImageWithLeftCapWidth:7 topCapHeight:7];
    
    frame.origin.x = frame.origin.y = 0;
    bgView = [[UIImageView alloc] initWithFrame:frame];
    bgView.image = bgImage;
    [view addSubview:bgView];
    
    showButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [showButton setImage:[UIImage imageNamed:@"showRecommendButton"] forState:UIControlStateNormal];
    showButton.frame = CGRectMake(0, [self hideHeight]-44-2, 300, 44);
    [showButton addTarget:self action:@selector(tapShow:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:showButton];
    
    newIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"newIconYellow"]];
    newIcon.frame = CGRectMake(184, showButton.frame.origin.y + 9, 30.5f, 22.5f);
     newIcon.hidden = YES;
    [view addSubview:newIcon];
}

-(void) tapShow:(UIButton*)button{
    [YakuUtil pain:button];
    
    [delegate onContentsShow:3];
    [self show];
}

-(CGRect) baseFrame{
    return CGRectMake(11, 28, 302.0f, [VknDeviceUtil windowHeight]);
}

-(float) hideHeight{
    float height = 478;
    if([VknDeviceUtil is35Inch]){
        height -= 35;
    }
    return height;
//    return [VknDeviceUtil windowHeight] - 223.0f + 90 + 37 + 6;
}

-(float)hideHeight2{
    return 40.0f;
}

-(float)showHeight{
//    NSLog(@"showHeight:%f", [VknDeviceUtil windowHeight] - 122.5f + 4.5f);
    float height =  450.0f;
    if([VknDeviceUtil is35Inch]){
        height -= 35;
    }
    return height;
}

-(void) show{
    newIcon.hidden = YES;
    CGRect frame = [self baseFrame];
    frame.size.height = [self showHeight];
    
    CGRect bgFrame = frame;
    bgFrame.origin.x = 0;
    
    showButton.alpha = 1.0f;
    showButton.userInteractionEnabled = NO;
    newIcon.alpha = 0.0;
    
    RecommendListViewController* listViewController = [[RecommendListViewController alloc] initWithDelegate:self];
    navigationController = [[UINavigationController alloc] initWithRootViewController:listViewController];
    [navigationController setNavigationBarHidden:YES];
    
    [UIView animateWithDuration:0.3f animations:^{
        view.frame = frame;
        bgView.frame = bgFrame;
        float y = -388;
        if([VknDeviceUtil is35Inch]){
            y += 35;
        }
        showButton.transform = CGAffineTransformMakeTranslation(0, y);
    } completion:^(BOOL finished) {
        UIView* navigationView = navigationController.view;
        navigationView.alpha = 0;
        [view addSubview:navigationView];
        [UIView animateWithDuration:0.2f animations:^{
            navigationView.alpha = 1.0f;
        } completion:^(BOOL finished) {
            [YakuUtil showRecommendList];
        }];
    }];
}

-(void) hide{
    CGRect frame = [self baseFrame];
    frame.size.height = [self hideHeight];
    
    CGRect bgFrame = frame;
    bgFrame.origin.x = bgFrame.origin.y = 0;
    showButton.userInteractionEnabled = YES;
    [UIView animateWithDuration:0.4f animations:^{
        view.frame = frame;
        bgView.frame = bgFrame;
        showButton.alpha = 1.0f;
        showButton.transform = CGAffineTransformMakeTranslation(0, 0);
        navigationController.view.alpha = 0;
    } completion:^(BOOL finished) {
        // TODO
        newIcon.alpha = 1.0f;
        [navigationController.view removeFromSuperview];
        navigationController = nil;
    }];
}

-(void) hide2{
    CGRect frame = [self baseFrame];
    frame.origin.y = 30;
    frame.size.height = [self hideHeight2];
    
    CGRect bgFrame = frame;
    bgFrame.origin.x = bgFrame.origin.y = 0;
    showButton.alpha = 0.0f;
    newIcon.alpha = 0.0f;
    [UIView animateWithDuration:0.4f animations:^{
        view.frame = frame;
        bgView.frame = bgFrame;
    } completion:^(BOOL finished) {
        
    }];
}

-(UIView*) getView{
    return view;
}

-(void) onSelectRecommend:(Recommend *)recommend{
    [YakuUtil readRecommend:recommend.id_];
    currentRecommend = recommend;
    
    UIAlertView* alertView = [[UIAlertView alloc] init];
    alertView.delegate = self;
    [alertView addButtonWithTitle:@"キャンセル"];
    [alertView addButtonWithTitle:@"OK"];
    alertView.message = @"AppStoreを開きます";
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex != 1) return;
    
    [VknUtils openBrowser:currentRecommend.linkUrl];
}

-(void) hasNew{
    newIcon.hidden = NO;
}

@end
