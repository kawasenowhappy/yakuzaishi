//
//  SiteListViewController.m
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "SiteListViewController.h"
#import "SiteCell.h"

@interface SiteListViewController ()<
UITableViewDataSource
, UITableViewDelegate
, SiteCellDelegate
>{
    NSObject<SiteListViewControllerDelegate>* delegate;
    UITableView* tableView_;
    SiteList* siteList;
    AdstirMraidView* adView;
}

@end

@implementation SiteListViewController

-(id) initWithDelegate:(NSObject<SiteListViewControllerDelegate> *)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        siteList = [YakuUtil siteList];
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    UIView* view = self.view;
    
    UIView* line = [[UIView alloc] initWithFrame:CGRectMake(10, 82.5f, 280, 2.5f)];
    line.backgroundColor = UIColorFromHex(0x278fa2);
    [view addSubview:line];
    
    float height = 391;
    if([VknDeviceUtil is35Inch]){
        height -= 35;
    }
    CGRect frame = CGRectMake(10, 85, 285, height);
    tableView_ = [[UITableView alloc] initWithFrame:frame];
    tableView_.delegate = self;
    tableView_.dataSource = self;
    tableView_.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView_.backgroundColor = [UIColor clearColor];
    
    // ad
    UIView* adViewWrapper = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 280, 100)];
    float scale = 0.877f;
    adView = [[AdstirMraidView alloc] initWithAdSize:kAdstirAdSize320x100 media:ADSTIR_MEDIA_ID spot:ADSTIR_RECRUIT_BOTTOM_SPOT_ID];
    adView.frame = CGRectMake(-20, 0, kAdstirAdSize320x100.size.width, kAdstirAdSize320x100.size.height);
//    adView.backgroundColor = UIColorFromHex(0xcccccc);
    adView.transform = CGAffineTransformMakeScale(scale, scale);
    [adViewWrapper addSubview:adView];
    tableView_.tableFooterView = adViewWrapper;
    [adView start];
    
    [view addSubview:tableView_];
}

-(void) toTop{
    [tableView_ setContentOffset:CGPointMake(0, -80) animated:YES];
}

-(void) tapBack{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- UITableViewDelegate --

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [siteList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellIdentifier = @"Cell";
    SiteCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[SiteCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.delegate = self;
    }
    
    Site* site = [siteList get:indexPath.row];
    [cell reload:site];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 82.5f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Site* site = [siteList get:indexPath.row];
    [delegate onSelectSite:site];
        
    SiteCell* siteCell = (SiteCell*)[tableView cellForRowAtIndexPath:indexPath];
    [siteCell light];
    
    [site readed];
    [YakuUtil readSite:site.id_];
    [VknUtils wait:0.3f callback:^{
        [tableView reloadData];
    }];
}

@end
