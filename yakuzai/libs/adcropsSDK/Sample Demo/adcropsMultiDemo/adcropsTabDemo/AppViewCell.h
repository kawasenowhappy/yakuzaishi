//
//  AppViewCell.h
//  adcropsTabDemo
//
//  Created by Tatsuya Uemura on 12/02/23.
//  Copyright (c) 2012年 8crops inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CELL_APP_IMAGE_MARGIN 5.0f
#define CELL_APP_IMAGE_SIZE 57.0f

@interface AppViewCell : UITableViewCell {
    UILabel                     *titleLabel;
    UILabel                     *detailLabel;
	UIImageView                 *appImageView;
    UIActivityIndicatorView     *indicator;
}
@property (nonatomic, retain) UILabel                   *titleLabel;
@property (nonatomic, retain) UILabel                   *detailLabel;
@property (nonatomic, retain) UIImageView               *appImageView;
@property (nonatomic, retain) UIActivityIndicatorView   *indicator;

@end
