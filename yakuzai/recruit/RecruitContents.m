//
//  RecruitContents.m
//  yakuzai
//
//  Created by kawase yu on 2014/07/12.
//  Copyright (c) 2014年 nowhappy. All rights reserved.
//

#import "RecruitContents.h"
#import "SiteListViewController.h"
#import "SiteDetailViewController.h"

@interface RecruitContents ()<
SiteListViewControllerDelegate
, SiteDetailViewControllerDelegate>{
    UIView* view;
    NSObject<RecruitContentsDelegate>* delegate;
    UIButton* showButton;
    UIImageView* bgView;
    UINavigationController* navigationController;
}

@end

@implementation RecruitContents

-(id) initWithDelegate:(NSObject<RecruitContentsDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
//    605, 891
    CGRect frame = [self baseFrame];
    frame.size.height = [self hideHeight];
    view = [[UIView alloc] initWithFrame:frame];
    view.clipsToBounds = YES;
    UIImage* bgImage = [UIImage imageNamed:@"kamiBlue"];
    bgImage = [bgImage stretchableImageWithLeftCapWidth:7 topCapHeight:7];
    
    frame.origin.x = frame.origin.y = 0;
    bgView = [[UIImageView alloc] initWithFrame:frame];
    bgView.image = bgImage;
    [view addSubview:bgView];
    
    showButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [showButton setImage:[UIImage imageNamed:@"showRecruitButton"] forState:UIControlStateNormal];
    showButton.frame = CGRectMake(0, [self hideHeight]-44-5, 300, 44);
    [showButton addTarget:self action:@selector(tapShow:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:showButton];
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(notifyBack) name:NOTIFICATION_KEY_BACK object:nil];
}

-(void) notifyBack{
    [navigationController popViewControllerAnimated:YES];
}

-(void) tapShow:(UIButton*)button{
    [YakuUtil pain:button];
    
    [delegate onContentsShow:1];
    [self show];
}

-(CGRect) baseFrame{
    return CGRectMake(11, 28, 302.0f, [VknDeviceUtil windowHeight]);
}

-(float) hideHeight{
    NSLog(@"hideHeight:%f", [VknDeviceUtil windowHeight] - 186);
    float height = 382.000000;
    
    if([VknDeviceUtil is35Inch]){
        height -= 35;
    }
    
    return height;
}

-(float)hideHeight2{
    return 40.0f;
}

-(float)showHeight{
//    NSLog(@"showHeight:%f", [VknDeviceUtil windowHeight] - 89.5f);
    
    float height = 478.5f;
    if([VknDeviceUtil is35Inch]){
        height -= 35;
    }
    
    return height;
}

-(void) show{
    CGRect frame = [self baseFrame];
    frame.size.height = [self showHeight];
    
    CGRect bgFrame = frame;
    bgFrame.origin.x = 0;
    bgFrame.origin.y = -0;
    bgFrame.size.height += 0;
    
    showButton.alpha = 1.0f;
    showButton.userInteractionEnabled = NO;
    
    SiteListViewController* listViewController = [[SiteListViewController alloc] initWithDelegate:self];
    navigationController = [[UINavigationController alloc] initWithRootViewController:listViewController];
    [navigationController setNavigationBarHidden:YES];
    [UIView animateWithDuration:0.4f animations:^{
        view.frame = frame;
        bgView.frame = bgFrame;
        float toY = -295;
        if([VknDeviceUtil is35Inch]){
            toY += 35;
        }
        showButton.transform = CGAffineTransformMakeTranslation(0, toY);
    } completion:^(BOOL finished) {
        
        UIView* navigationView = navigationController.view;
        navigationView.alpha = 0;
        [view addSubview:navigationView];
        [UIView animateWithDuration:0.2f animations:^{
            navigationView.alpha = 1.0f;
        }];
    }];
}

-(void) hide{
    CGRect frame = [self baseFrame];
    frame.size.height = [self hideHeight];
    
    CGRect bgFrame = frame;
    bgFrame.origin.x = bgFrame.origin.y = 0;
    showButton.userInteractionEnabled = YES;
    UIView* navigationView = navigationController.view;
    [UIView animateWithDuration:0.2f animations:^{
        view.frame = frame;
        bgView.frame = bgFrame;
        showButton.transform = CGAffineTransformMakeTranslation(0, 0);
        showButton.alpha = 1.0f;
        navigationView.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [navigationView removeFromSuperview];
        navigationController = nil;
    }];
}

-(void) hide2{
    CGRect frame = [self baseFrame];
    frame.origin.y = 30;
    frame.size.height = [self hideHeight2];
    
    CGRect bgFrame = frame;
    bgFrame.origin.x = bgFrame.origin.y = 0;
    showButton.alpha = 0.0f;
    [UIView animateWithDuration:0.2f animations:^{
        view.frame = frame;
        bgView.frame = bgFrame;
    } completion:^(BOOL finished) {
        
    }];
}

-(UIView*) getView{
    return view;
}

-(void) onSelectSite:(Site *)site{
    SiteDetailViewController* detailViewController = [[SiteDetailViewController alloc] initWithDelegate:self andSite:site];
    [navigationController pushViewController:detailViewController animated:YES];
    
    [delegate showBackButton];
    
    NSNotification *n = [NSNotification notificationWithName:NOTIFICATION_KEY_SHOW_BACK object:self];
    [[NSNotificationCenter defaultCenter] postNotification:n];
}

@end
