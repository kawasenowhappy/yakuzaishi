//
//  ChkViewSetting.h
//  8chkDemo
//
//  Created by Tatsuya Uemura on 2012/12/12.
//
//

#ifndef _ChkViewSetting_h
#define _ChkViewSetting_h

#define CHK_VIEW_ANIMATION_TYPE                 1
#define CHK_VIEW_APP_FRAME_COLOR                @"#808080"
#define CHK_VIEW_APP_TITLE_BACKGROUND_COLOR     @"#1e90ff"
#define CHK_VIEW_APP_TITLE_TEXT_COLOR           @"#ffffff"
#define CHK_VIEW_APP_BACKGROUND_COLOR           @"#ffffff"

#endif
