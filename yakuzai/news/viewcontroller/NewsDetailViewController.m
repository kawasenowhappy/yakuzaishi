//
//  NewsDetailViewController.m
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "NewsDetailViewController.h"

@interface NewsDetailViewController ()<
UIWebViewDelegate
>{
    NSObject<NewsDetailViewControllerDelegate>* delegate;
    News* news;
    UIScrollView* sv;
    BOOL firstLoad;
    UIWebView* webView_;
    UIButton* backButton;
}

@end

@implementation NewsDetailViewController

-(id) initWithDelegate:(NSObject<NewsDetailViewControllerDelegate>*)delegate_ andNews:(News*)news_{
    self = [super init];
    if(self){
        delegate = delegate_;
        news = news_;
        firstLoad = YES;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    self.view.backgroundColor = [UIColor whiteColor];
    [self setWebView];
    [self setHeader];
}

-(void) setWebView{
    CGRect frame = CGRectMake(0, 64, 320, [VknDeviceUtil windowHeight]-64);
    webView_ = [[UIWebView alloc] initWithFrame:frame];
    webView_.delegate = self;
    webView_.scalesPageToFit = YES;
    [webView_ loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:news.linkUrl]]];
    [self.view addSubview:webView_];
    
    [self showLoading];
}

-(void) setHeader{
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    UIImageView* bgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"webHeader"]];
    [headerView addSubview:bgView];
    
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"backButtonWeb"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(tapBack) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 20, 80, 44);
    backButton.hidden = YES;
    [headerView addSubview:backButton];
    
    UIButton* xButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [xButton setImage:[UIImage imageNamed:@"xButtonWeb"] forState:UIControlStateNormal];
    [xButton addTarget:self action:@selector(tapClose) forControlEvents:UIControlEventTouchUpInside];
    xButton.frame = CGRectMake(320-44, 20, 44, 44);
    [headerView addSubview:xButton];
    
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 20, 180, 44)];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.numberOfLines = 2;
    titleLabel.adjustsFontSizeToFitWidth = YES;
    titleLabel.text = news.title;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.userInteractionEnabled = NO;
    [headerView addSubview:titleLabel];
    
    [self.view addSubview:headerView];
}

-(void) tapBack{
    if([webView_ canGoBack]){
        [webView_ goBack];
        if(![webView_ canGoBack]){
            backButton.hidden = YES;
        }
        return;
    }else{
        backButton.hidden = YES;
    }
}

-(void) tapClose{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(void) dealloc{
    webView_.delegate = nil;
    [webView_ removeFromSuperview];
}

#pragma mark -- UIWebViewDelegate --

- (void)webViewDidFinishLoad:(UIWebView *)webView{
     backButton.hidden = ![webView canGoBack];
    if(!firstLoad) return;
    firstLoad = NO;
    [self hideLoading];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked ) {
        [self showLoading];
        firstLoad = YES;
    }
    
    return YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    if(!firstLoad) return;
    firstLoad = NO;
    [self hideLoading];
}

@end
