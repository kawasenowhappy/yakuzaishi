//
//  SelectButton.m
//  yakuzai
//
//  Created by kawase yu on 2014/07/05.
//  Copyright (c) 2014年 nowhappy. All rights reserved.
//

#import "SelectButton.h"

@interface SelectButton (){
    int currentIndex;
    NSString* labelText;
}

@end

@implementation SelectButton

-(id) initWithDelegate:(NSObject<SelectButtonDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    currentIndex = -1;
    UIImageView* bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"selectButton"]];
    bg.frame = CGRectMake(90, 0, 150, 25);
    [self addSubview:bg];
    
    //    330 × 50
    label = [[UILabel alloc] initWithFrame:CGRectMake(90, 0, 150, 25)];
    label.textColor = UIColorFromHex(0x717171);
    label.font = [UIFont boldSystemFontOfSize:14];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"選択する";
    [self addSubview:label];
    
    keyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 75, 25)];
    keyLabel.textColor = UIColorFromHex(0x4ec3ae);
    keyLabel.font = [UIFont boldSystemFontOfSize:14.0f];
    [self addSubview:keyLabel];
    
    [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)]];
}

-(void) tap{
    if(blackLayer.alpha != 0){
        [self hide];
        return;
    }
    //    [YakuUtil pain:self];
    
    UIView* parentView = [delegate getView];
    blackLayer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [VknDeviceUtil windowHeight])];
    blackLayer.backgroundColor = UIColorFromHex(0x000000);
    blackLayer.alpha = 0.0f;
    [blackLayer addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hide)]];
    
    UIView* rootView = [YakuUtil rootViewController].view;
    [rootView addSubview:blackLayer];
    
    pickerView = [[UIPickerView alloc] init];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    CGRect pickerFrame = pickerView.frame;
    pickerFrame.origin.y = [VknDeviceUtil windowHeight] - pickerFrame.size.height;
    pickerView.frame = pickerFrame;
    pickerView.backgroundColor = UIColorFromHex(0xcccccc);
    pickerView.transform = CGAffineTransformMakeTranslation(0, pickerFrame.size.height+50);
    
    if(currentIndex != -1){
        [pickerView selectRow:currentIndex inComponent:0 animated:NO];
    }
    [rootView addSubview:pickerView];
    
    currentParentView = self.superview;
    
    [blackLayer addSubview:self];
    self.transform = CGAffineTransformMakeTranslation(30, 60);
    
    label.textColor = UIColorFromHex(0x252525);
    [UIView animateWithDuration:0.3f animations:^{
        blackLayer.alpha = 0.8f;
        pickerView.transform = CGAffineTransformMakeTranslation(0, 0);
    }];
}

-(void) hide{
    CGRect pickerFrame = pickerView.frame;
    label.textColor = UIColorFromHex(0x717171);
    self.transform = CGAffineTransformMakeTranslation(0, 0);
    [currentParentView addSubview:self];
    [UIView animateWithDuration:0.3f animations:^{
        blackLayer.alpha = 0.0f;
        pickerView.transform = CGAffineTransformMakeTranslation(0, pickerFrame.size.height+50);
    } completion:^(BOOL finished) {
        [blackLayer removeFromSuperview];
        [pickerView removeFromSuperview];
    }];
}

-(void) setItemList:(NSArray*)itemList_{
    itemList = itemList_;
}

-(void) setKeyTtitle:(NSString*)keyText{
    keyLabel.text = keyText;
    labelText = keyText;
}

+(SelectButton*)create:(NSObject<SelectButtonDelegate>*)delegate_ index:(int)index{
    switch(index){
        case 0: return [self createAge:delegate_];
        case 1: return [self createPref:delegate_];
        case 2: return [self createWorkstyle:delegate_];
        case 3: return [self createIncome:delegate_];
    }
    
    return nil;
}

+(SelectButton*) createIncome:(NSObject<SelectButtonDelegate>*)delegate_{
    SelectButton* button = [[SelectButton alloc] initWithDelegate:delegate_];
    [button setKeyTtitle:@"現在の年収"];
    
    NSMutableArray* tmp = [[NSMutableArray alloc] init];
    for(int i=240;i<=1000;i=i+10){
        [tmp addObject:[NSString stringWithFormat:@"%d万円", i]];
    }
    [button setItemList:[NSArray arrayWithArray:tmp]];
    
    return button;
}

+(SelectButton*) createAge:(NSObject<SelectButtonDelegate>*)delegate_{
    SelectButton* button = [[SelectButton alloc] initWithDelegate:delegate_];
    [button setKeyTtitle:@"年齢"];
    [button setItemList:@[
                          @"20〜24",
                          @"24〜28",
                          @"28〜32",
                          @"32〜36",
                          @"36〜40",
                          @"40〜44",
                          @"44〜48",
                          @"48〜52",
                          @"52〜56",
                          @"56〜"
                          ]];
    return button;
}

+(SelectButton*) createPref:(NSObject<SelectButtonDelegate>*)delegate_{
    SelectButton* button = [[SelectButton alloc] initWithDelegate:delegate_];
    [button setKeyTtitle:@"地域"];
    [button setItemList:@[
                          @"北海道",@"青森県",@"岩手県",@"宮城県",@"秋田県",@"山形県",@"福島県",@"茨城県",@"栃木県",@"群馬県",
                          @"埼玉県",@"千葉県",@"東京都",@"神奈川県",@"新潟県",@"富山県",@"石川県",@"福井県",@"山梨県",@"長野県",
                          @"岐阜県",@"静岡県",@"愛知県",@"三重県",@"滋賀県",@"京都府",@"大阪府",@"兵庫県",@"奈良県",@"和歌山県",
                          @"鳥取県",@"島根県",@"岡山県",@"広島県",@"山口県",@"徳島県",@"香川県",@"愛媛県",@"高知県",@"福岡県",
                          @"佐賀県",@"長崎県",@"熊本県",@"大分県",@"宮崎県",@"鹿児島県",@"沖縄県"
                          ]];
    return button;
}

+(SelectButton*) createWorkstyle:(NSObject<SelectButtonDelegate>*)delegate_{
    SelectButton* button = [[SelectButton alloc] initWithDelegate:delegate_];
    [button setKeyTtitle:@"勤務体系"];
    [button setItemList:@[
                          @"派遣"
                          , @"契約社員"
                          , @"正社員"
                          ]];
    return button;
}

#pragma mark -------- public -----------

-(int) getIndex{
    return currentIndex;
}

-(NSString*)labelText{
    return labelText;
}

#pragma mark -------- UIPickerViewDelegate -----------

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [itemList count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return itemList[row];
}

-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    currentIndex = row;
    label.text = [self pickerView:pickerView titleForRow:row forComponent:component];
}

@end

