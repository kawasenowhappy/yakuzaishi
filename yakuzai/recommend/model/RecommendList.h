//
//  RecommendList.h
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "VknModelList.h"
#import "Recommend.h"

@interface RecommendList : VknModelList

+(RecommendList*) createModelList;
-(Recommend*)get:(int)index;
-(void) reloadWithJson:(NSArray*)jsonArray;
-(void) readedSort;

@end
