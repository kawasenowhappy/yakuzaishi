//
//  ChkDetailViewController.h
//  8chk
//
//  Created by 8crops Inc. on 11/06/20.
//  Copyright 2011 8crops Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChkView.h"
#import "ChkRecordData.h"

@interface ChkDetailViewController : UIViewController {
	ChkView                         *_chkView;
	UILabel                         *_titleLabel;
	UIView                          *_backgroudView;
	ChkRecordData                   *_appData;
	UIImageView                     *_appImageView;
	UITextView                      *_condition;
	UILabel                         *_conditionLabel;
	UITextView                      *_detail;
	UILabel                         *_detailLabel;
	UILabel                         *_appNameLabel;
	UIButton                        *_downloadButton;
	UIImageView                     *_ic_price;
	UILabel                         *_price;
	UIImageView                     *_ic_point;
	UILabel                         *_point;
    ChkConfig                       *_chkConfig;
}
@property (nonatomic, retain) ChkView       *chkView;
@property (nonatomic, retain) UILabel       *titleLabel;
@property (nonatomic, retain) UIView        *backgroudView;
@property (nonatomic, retain) ChkRecordData *appData;
@property (nonatomic, retain) UIImageView   *appImageView;
@property (nonatomic, retain) UITextView    *condition;
@property (nonatomic, retain) UILabel       *conditionLabel;
@property (nonatomic, retain) UITextView    *detail;
@property (nonatomic, retain) UILabel       *detailLabel;
@property (nonatomic, retain) UILabel       *appNameLabel;
@property (nonatomic, retain) UIButton      *downloadButton;
@property (nonatomic, retain) UIImageView   *ic_price;
@property (nonatomic, retain) UILabel       *price;
@property (nonatomic, retain) UIImageView   *ic_point;
@property (nonatomic, retain) UILabel       *point;
@property (nonatomic, retain) ChkConfig     *chkConfig;

- (id)initWithConfig:(ChkConfig*)config;

@end
