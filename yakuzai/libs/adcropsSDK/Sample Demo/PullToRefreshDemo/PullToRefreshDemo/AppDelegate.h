//
//  AppDelegate.h
//  PullToRefreshDemo
//
//  Created by Tetsuyak on 2014/05/08.
//  Copyright (c) 2014年 8crops. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
