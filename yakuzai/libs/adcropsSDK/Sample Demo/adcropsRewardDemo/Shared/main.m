//
//  main.m
//  adcropsDemo
//
//  Created by 8crops Inc. on 11/06/14.
//  Copyright 2011 8crops Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal;
	
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
		retVal = UIApplicationMain(argc, argv, nil, @"AppDelegate_iPhone");
	}else {
		retVal = UIApplicationMain(argc, argv, nil, @"AppDelegate_iPad");
	}
	
    [pool release];
    return retVal;
}
