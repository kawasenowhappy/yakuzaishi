//
//  RecruitContents.h
//  yakuzai
//
//  Created by kawase yu on 2014/07/12.
//  Copyright (c) 2014年 nowhappy. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RecruitContentsDelegate <NSObject>

-(void) onContentsShow:(int)index;
-(void) showBackButton;
-(void) hideBackButton;

@end

@interface RecruitContents : NSObject

-(id) initWithDelegate:(NSObject<RecruitContentsDelegate>*)delegate_;
-(UIView*) getView;
-(void) hide;
-(void) hide2;

@end
