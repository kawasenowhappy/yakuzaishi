//
//  ViewController.m
//  AdcropsNtInsertDemo
//
//  Created by sazawayuki on 2014/03/24.
//  Copyright (c) 2014年 8crops inc. All rights reserved.
//

#import "ViewController.h"
#import "ChkNtInsertView.h"

@interface ViewController ()<UITableViewDataSource, UITableViewDelegate>{
    ChkNtInsertView *chkNtInsertView;
}

@end

@implementation ViewController

static NSString * const cellIdentifier = @"UITableCell";
static NSString * const chkNtInsertCellIdentifier = @"AppTableCell";

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor colorWithRed:0.91 green:0.92 blue:0.92 alpha:1.0];
    
    UITableView *tableView = [[UITableView alloc] init];
    tableView.backgroundColor = [UIColor clearColor];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.frame = self.view.bounds;
    [self.view addSubview:tableView];
    
    chkNtInsertView = [[ChkNtInsertView alloc] initWithFrame:5 rootViewController:self];
    [chkNtInsertView load];
}

#pragma mark - TablewView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (indexPath.row == 3) {
        cell = [tableView dequeueReusableCellWithIdentifier:chkNtInsertCellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:chkNtInsertCellIdentifier];
            [cell.contentView addSubview:chkNtInsertView];
        }
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            cell.backgroundColor = [UIColor clearColor];
        }
        cell.textLabel.text = [NSString stringWithFormat:@"ニュース記事 %d", indexPath.row];
    }

    return cell;
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 3) {
        return kChkNtInsertViewSize.height+10;
    } else {
        return 80;
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
