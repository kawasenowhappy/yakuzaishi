//
//  ViewController.m
//  adcropsInterstitialDemo
//
//  Created by sazawayuki on 2013/11/14.
//  Copyright (c) 2013年 8crops inc. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    // ChkController生成
    self.chkController = [[ChkController alloc] initWithDelegate:self];
    
    self.view.backgroundColor = [UIColor greenColor];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btn.frame = CGRectMake(0, 0, 100, 50);
    btn.center = self.view.center;
    [btn setTitle:@"表示" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(showInterstitial:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];

}

-(void)showInterstitial:(UIButton *)btn{
    
    //ウォールインプレッション送信
    [self.chkController sendWallImpression];
    
    // 任意の場所で以下を呼び出しインタースティシャルを表示します。
    [[ChkInterstitial sharedChkInterstitial] showInterstitial];
    // delegateメソッド内で、呼び出された箇所により切り分けなどを行いたい場合は以下のメソッそを利用し任意のタグを付与して切り分ける事が可能です。
    //[[ChkInterstitial sharedChkInterstitial] showInterstitial:@"show1"];
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
