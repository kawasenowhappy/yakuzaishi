//
//  ViewController.h
//  adcropsInterstitialDemo
//
//  Created by sazawayuki on 2013/11/14.
//  Copyright (c) 2013年 8crops inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChkInterstitial.h"

@interface ViewController : UIViewController

@property(strong, nonatomic)ChkController   *chkController;


@end
