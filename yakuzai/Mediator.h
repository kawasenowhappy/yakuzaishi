//
//  Mediator.h
//  yakuzai
//
//  Created by kawase yu on 2014/07/05.
//  Copyright (c) 2014年 nowhappy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Mediator : NSObject

-(id) initWithWindow:(UIWindow*)window_;
-(void) willEnterForeground;
-(void) memoryWarning;
-(void) didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;

@end
