//
//  SiteDetailViewController.h
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "BaseViewController.h"
#import "Site.h"

@protocol SiteDetailViewControllerDelegate <NSObject>


@end

@interface SiteDetailViewController : BaseViewController

-(id) initWithDelegate:(NSObject<SiteDetailViewControllerDelegate>*)delegate_ andSite:(Site*)site_;

@end
