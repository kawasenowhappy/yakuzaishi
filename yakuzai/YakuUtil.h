//
//  YakuUtil.h
//  yakuzai
//
//  Created by kawase yu on 2014/07/05.
//  Copyright (c) 2014年 nowhappy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SiteList.h"
#import "NewsList.h"
#import "RecommendList.h"

@interface YakuUtil : NSObject

// global
+(void) setup:(UIViewController*)rootViewController;
+(UIViewController*) rootViewController;

+(void) pain:(UIView*)view;


+(void) readNews:(int)id_;
+(void) readSite:(int)id_;
+(void) readRecommend:(int) id_;

+(BOOL) isReadedNews:(int)id_;
+(BOOL) isReadedSite:(int)id_;
+(BOOL) isReadedRecommend:(int)id_;

+(BOOL) hasNewNews;
+(BOOL) hasNewRecommend;

+(void) showNewsList;
+(void) showRecommendList;

+(SiteList*)siteList;
+(NewsList*)newsList;
+(RecommendList*)recommendList;
+(void) setAdcropsList:(NSMutableArray*)adcropsList_;

+(NSMutableArray*) showedRecommendList;

+(void) setModelJson:(NSString*)jsonString;
+(void)reloadModel;
+(BOOL) isShowRecommend;

+(int) tekisei:(int)ageIndex prefIndex:(int)prefIndex workIndex:(int)workIndex;
+(int) current:(int)currentIndex;

+(NSString*)priceToString:(int)price;
+(void) tryShowRecommend;

@end
