//
//  TopViewController.h
//  yakuzai
//
//  Created by kawase yu on 2014/07/05.
//  Copyright (c) 2014年 nowhappy. All rights reserved.
//

#import "BaseViewController.h"

@protocol TopViewDelegate <NSObject>

-(void) hideContents:(int)contentsIndex;
-(void) onBack;

@end

@interface TopViewController : BaseViewController

-(id) initWithDelegate:(NSObject<TopViewDelegate>*)delegate_;
-(void) hide:(int)contentsIndex;
-(void) showBack;
-(void) hideBack;

@end
