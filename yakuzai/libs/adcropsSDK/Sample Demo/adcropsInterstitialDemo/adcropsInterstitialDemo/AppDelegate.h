//
//  AppDelegate.h
//  adcropsInterstitialDemo
//
//  Created by sazawayuki on 2013/11/14.
//  Copyright (c) 2013年 8crops inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ChkApplicationOptional.h"
#import "ChkInterstitial.h"

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate,ChkInterstitialDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
