//
//  Recommend.h
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "VknModel.h"
#import "ChkRecordData.h"

@interface Recommend : VknModel

@property NSString* linkUrl;
@property NSString* title;
@property NSString* excerpt;
@property NSString* iconUrl;
@property (readonly) BOOL isReaded;
@property (readonly) BOOL isNew;

-(id) initWithJson:(NSDictionary*)jsonDic;
-(id) initWithAdcropsdata:(ChkRecordData*)data;

-(void) readed;
-(void) setNew;
-(void) noNew;

@end
