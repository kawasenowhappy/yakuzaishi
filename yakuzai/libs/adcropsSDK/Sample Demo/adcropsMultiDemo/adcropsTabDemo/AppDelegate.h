//
//  AppDelegate.h
//  adcropsTabDemo
//
//  Created by Tatsuya Uemura on 12/02/23.
//  Copyright (c) 2012年 8crops inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FirstListViewController.h"
#import "SecondListViewController.h"
#import "AppListViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,UITabBarControllerDelegate>
{
    UIBackgroundTaskIdentifier      backgroundTask;
    UITabBarController              *tabBar;
    FirstListViewController         *firstListView;
    SecondListViewController        *secondListView;
    AppListViewController           *appListView;
}
@property (strong, nonatomic) UIWindow                      *window;
@property (nonatomic, retain) UITabBarController            *tabBar;
@property (nonatomic, retain) FirstListViewController       *firstListView;
@property (nonatomic, retain) SecondListViewController      *secondListView;
@property (nonatomic, retain) AppListViewController         *appListView;

@end
