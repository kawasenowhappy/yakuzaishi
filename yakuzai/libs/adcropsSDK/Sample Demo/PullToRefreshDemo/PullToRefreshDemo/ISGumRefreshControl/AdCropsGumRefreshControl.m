#import "AdCropsGumRefreshControl.h"
#import "ISScalingActivityIndicatorView.h"
#import "ISGumView.h"
#import "AdCropsHScrollView.h"
#import "AdCropsConstants.h"
#import "ChkController.h"
#import <StoreKit/StoreKit.h>

@interface AdCropsGumRefreshControl () <SKStoreProductViewControllerDelegate, AdCropsHScrollViewDelegate>

@property(nonatomic, strong) ISScalingActivityIndicatorView *indicatorView;
@property(nonatomic, strong) ISGumView *gumView;
@property(nonatomic, strong) UIButton *closeButton;
@property(nonatomic, strong) AdCropsHScrollView *scrollAdView;
@property(nonatomic, strong) UIImageView *matchIcon;

@end


@implementation AdCropsGumRefreshControl {
	CGFloat topPadding_;
	BOOL isRotationFlg_;
}


- (id)initWithFrame:(CGRect)frame {

	// UINavigationController配下でないUITablevViewControllerなどに設置する場合は下記をコメントアウトしてください。
	//	if ([[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."][0] intValue] >= 7) {//iOS7 later
	//		topPadding_ = 0;
	//	} else {
	//		topPadding_ = 0;
	//	}

	self = [super initWithFrame:frame];
	if (self) {
		[self initialize];
	}
	return self;
}

- (id)initWithCoder:(NSCoder *)coder {
	self = [super init];
	if (self) {
		[self initialize];
	}
	return self;
}


- (void)initialize {

	self.clipsToBounds = NO;
	self.backgroundColor = [UIColor clearColor];

	self.indicatorView = [[ISScalingActivityIndicatorView alloc] init];
	[self addSubview:self.indicatorView];

	self.gumView = [[ISGumView alloc] init];
	self.gumView.frame = CGRectMake(0.f, 0.f, self.frame.size.width, self.frame.size.height);    // ここはyは0で良い。
	[self addSubview:self.gumView];

}

- (void)layoutSubviews {
	[super layoutSubviews];
	// NSLog(@"%s, %@", __PRETTY_FUNCTION__, NSStringFromCGRect(self.gumView.frame));
	CGFloat pY = self.frame.size.height - kGumTopPadding - 20;
	if (!_scrollAdView)pY += 50;

	self.gumView.frame = CGRectMake(0.f, pY, self.frame.size.width, 200.f);
}


#pragma mark - ISAlternativeRefreshControl events

- (void)didChangeProgress {
	if (self.refreshingState == ISRefreshingStateNormal) {
		self.gumView.maxDistance = ABS(self.threshold) - 35;

		if (self.progress <= .35f) {
			self.gumView.distance = 1.f;
		} else {
			self.gumView.distance = (self.progress - .35f) / (1.f - .35f) * self.gumView.maxDistance;
		}

		[self.gumView setNeedsDisplay];
	} else if (self.refreshingState == ISRefreshingStateRefreshing) {
		if (_closeButton) {
			UIScrollView *scrollView = (id) self.superview;
			// -100 完全に見える
			// 完全に見えなくなる
			// NSLog(@"%s  %f", __PRETTY_FUNCTION__, scrollView.contentOffset.y);
			CGFloat py = scrollView.contentOffset.y;
			if (py > 0) { // RefreshControlエリアが画面外に出た
				isRotationFlg_ = YES;
			} else if (py < -(kAdcropsViewHeight + kCloseButtonAreaHeight - 1)) {// RefreshControlエリアが画面内に完全表示
				if (isRotationFlg_) {
					[_scrollAdView rotateAdItem];
					isRotationFlg_ = NO;
				}
			}


		}
	}

}

- (void)willChangeRefreshingState:(ISRefreshingState)refreshingState {
	switch (refreshingState) {
		case ISRefreshingStateRefreshing: {

			[self.gumView shrink];
			[self.indicatorView startAnimating];
			[self animationgIndicatorView];
			[self createAdcropsArea];    // adcrops バナーエリア作成

		}
			break;

		case ISRefreshingStateRefreshed:
			[self.indicatorView stopAnimating];
			break;

		default:
			break;
	}
}

- (void)didChangeRefreshingState {
	// RefreshCrontrolを閉じた後は閉じるボタン、広告Viewなどを除去しておく
	if (self.refreshingState == ISRefreshingStateRefreshed) {

		[_closeButton removeFromSuperview];
		_closeButton = nil;

		_matchIcon.frame = CGRectMake(0, -24, 35, 24);

		// 自分自身のframeをリセット。
		_scrollAdView.frame = CGRectMake(0, -(kAdcropsViewHeight + topPadding_), self.frame.size.width, kAdcropsViewHeight + topPadding_);
		CGRect gFrame = CGRectOffset(self.frame, 0, 0);

		if (!_chkController || [_chkController.dataList count] == 0) {
			gFrame.size.height = kRefreshControlHeightNormal + topPadding_;
			gFrame.origin.y = -(kRefreshControlHeightNormal + topPadding_);
		} else {
			gFrame.size.height = kRefreshControlHeightFull + topPadding_;
			gFrame.origin.y = -(kRefreshControlHeightFull + topPadding_);
		}


		self.frame = gFrame;

	}

}

- (void)beginRefreshing {
	[super beginRefreshing];
}

- (void)endRefreshing {
	[self closeIndicatorArea];    // インジゲータエリアを狭める
	[self.indicatorView stopAnimating];    // インジゲータアニメーションを停止

	if (!_chkController || [_chkController.dataList count] == 0) {
		[super endRefreshing];
	} else {
		[self showCloseButton];    // 閉じるボタン表示
	}
}


#pragma mark - 8crops.

/*
	閉じるボタンの表示
 */
- (void)showCloseButton {

	_closeButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	CGFloat buttonWidth = self.frame.size.width;
	CGFloat buttonHeight = 15.f;
	[_closeButton setBackgroundImage:[UIImage imageNamed:@"close_button"] forState:UIControlStateNormal];

	CGFloat pY = kAdcropsViewHeight + topPadding_;
	if (!_chkController || [_chkController.dataList count] == 0) {
		pY = (kRefreshControlHeightNormal + topPadding_) - (kIndicatorAreaHeight + buttonHeight);
	}
	_closeButton.frame = CGRectMake(0, pY, buttonWidth, buttonHeight);
	[_closeButton addTarget:self action:@selector(closeButtonPushed:) forControlEvents:UIControlEventTouchUpInside];
	[self addSubview:_closeButton];

}


- (void)closeIndicatorArea {

	// 親ScrollView
	__block __weak UIScrollView *blockScrollView = (id) self.superview;
	UIEdgeInsets inset = blockScrollView.contentInset;
	inset.top -= kIndicatorAreaHeight; // インジケータぶんとじる

	// 本体
	CGRect selfTargetRect = CGRectOffset(self.frame, 0, kIndicatorAreaHeight);
	selfTargetRect.size.height -= kIndicatorAreaHeight;

	[UIView animateWithDuration:.3f
					 animations:^{
		blockScrollView.contentInset = inset;
		self.frame = selfTargetRect;
	}];
}


/*
	閉じるボタンが押された
 */
- (void)closeButtonPushed:(id)sender {
	[super endRefreshing];
}

/*
	インジケータをパネル下部へ移動
 */
- (void)animationgIndicatorView {

	CGFloat indPosiotionY = self.gumView.frame.origin.y;
	self.indicatorView.frame = CGRectMake(self.frame.size.width / 2.f - (kIndicatorViewSize / 2), indPosiotionY, kIndicatorViewSize, kIndicatorViewSize);

	// 下方向へ移動。移動量の計算
	CGFloat targetPositionY = self.frame.size.height - kIndicatorViewSize - kIndicatorBottomPadding; // 移動させたい位置。
	CGFloat offsetY = targetPositionY - self.indicatorView.frame.origin.y; // 移動量
	CGRect targetFrame = CGRectOffset(self.indicatorView.frame, 0, offsetY);

	// NSLog(@"%s %@", __PRETTY_FUNCTION__, NSStringFromCGRect(targetFrame));
	__block __weak AdCropsGumRefreshControl *blockSelf = self;
	[UIView animateWithDuration:0.32f
					 animations:^{
		blockSelf.indicatorView.frame = targetFrame;
	} completion:^(BOOL finished) {

	}];

}


#pragma mark - adcrops

/*
	adcropsバナーエリア表示
	画面上部から下がってくるアニメーション
 */
- (void)createAdcropsArea {

	if (!_matchIcon) {
		_matchIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, -24, 35, 24)];
		_matchIcon.image = [UIImage imageNamed:@"match"];
		[self addSubview:_matchIcon];
	}

	CGRect targetFrame = CGRectOffset(_scrollAdView.frame, 0, kAdcropsViewHeight + topPadding_);
	CGRect matchImageTargetFrame = CGRectOffset(_matchIcon.frame, 0, topPadding_ + 24);

	_matchIcon.hidden = !_chkController || [_chkController.dataList count] == 0;

	[UIView animateWithDuration:0.27f
					 animations:^{
		_scrollAdView.frame = targetFrame;
		_matchIcon.frame = matchImageTargetFrame;
	}
					 completion:^(BOOL finished) {
		[_scrollAdView rotateAdItem];
	}];
}

- (void)setChkController:(ChkController *)chkController {

	_chkController = chkController;

	if (!_scrollAdView && self.refreshingState == ISRefreshingStateNormal && _chkController.dataList) {

		CGRect orgRct = self.frame;
		self.frame = CGRectMake(orgRct.origin.x, orgRct.origin.y - ((kRefreshControlHeightFull + topPadding_) - kRefreshControlHeightNormal),
				orgRct.size.width, kRefreshControlHeightFull + topPadding_);

		// 広告エリア作成
		_scrollAdView = [AdCropsHScrollView create:_chkController.dataList delegate:self];
		_scrollAdView.frame = CGRectMake(0, -(kAdcropsViewHeight + topPadding_), self.frame.size.width, kAdcropsViewHeight + topPadding_);
		_scrollAdView.backgroundColor = [UIColor whiteColor]; // forDebug
		[self addSubview:_scrollAdView];

	}


}

- (void)viewDidAdcropsItem:(ChkRecordData *)recordData {
	if (!_chkController || !recordData) {
		NSLog(@"%s インプレッション送信に失敗。_chkController又はrecordDataがnilである", __PRETTY_FUNCTION__);
	} else {
		NSLog(@"%s インプレッション送信 %@", __PRETTY_FUNCTION__, [recordData debugDescription]);
		[_chkController sendImpression:recordData];
	}
}


- (void)dealloc {
	NSLog(@"%s", __PRETTY_FUNCTION__);
}


@end
