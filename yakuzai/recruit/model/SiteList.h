//
//  SiteList.h
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "VknModelList.h"
#import "Site.h"

@interface SiteList : VknModelList

+(SiteList*)createModelList;
-(Site*)get:(int)index;
-(void) reloadWithJson:(NSArray*)jsonArray;
-(void) readedSort;

@end
