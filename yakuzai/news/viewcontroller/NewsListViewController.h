//
//  NewsListViewController.h
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "BaseViewController.h"
#import "News.h"

@protocol NewsListViewControllerDelegate <NSObject>

-(void) onSelect:(News*)news;

@end

@interface NewsListViewController : BaseViewController

-(id) initWithDelegate:(NSObject<NewsListViewControllerDelegate>*)delegate_;

@end
