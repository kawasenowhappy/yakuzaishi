//
//  ChkViewController.m
//  8chk
//
//  Created by 8crops Inc. on 11/06/14.
//  Copyright 2011 8crops Inc. All rights reserved.
//

#import "ChkViewController.h"
#import "ChkView.h"
#import "ChkViewSetting.h"
#import "ChkDetailViewController.h"
#import "ChkController.h"
#import "ChkRecordData.h"
#import <UIKit/UIKit.h>

#import <dispatch/dispatch.h>
#import <QuartzCore/QuartzCore.h>

#define CELL_APP_IMAGE_MARGIN 5.0f
#define CELL_APP_IMAGE_SIZE 32.0f

@interface ChkViewController ()
@property (nonatomic, retain) ChkView                   *chkView;
@property (nonatomic, retain) UITableView               *tableView;
@property (nonatomic, retain) UILabel                   *titleLabel;
@property (nonatomic, retain) UIButton                  *closeButton;
@property (nonatomic, retain) UIActivityIndicatorView   *indicator;
@property (nonatomic, retain) UINavigationController    *naviController;
@property (nonatomic, retain) ChkController             *chkController;
@property (nonatomic, retain) UIImageView               *networkErrImageView;
@property (nonatomic, retain) ChkDetailViewController   *detailViewController;
@property (nonatomic, retain) UIRefreshControl          *refreshControl;
@property (nonatomic, retain) NSMutableArray            *dataList;

@end

@implementation ChkViewController

dispatch_queue_t                main_queue;
dispatch_queue_t                list_queue;
dispatch_queue_t                image_queue;

@synthesize chkView;
@synthesize tableView = _tableView;
@synthesize titleLabel;
@synthesize closeButton;
@synthesize indicator;
@synthesize naviController;
@synthesize chkController;
@synthesize networkErrImageView;
@synthesize detailViewController;
@synthesize refreshControl;
@synthesize dataList;

#pragma mark -
#pragma mark Private method
- (void)closeButtonDidPush {
    NSLog(@"closeButtonDidPush");
	dispatch_release(list_queue);
    dispatch_release(image_queue);
    [self.chkView hide];
    [self.view removeFromSuperview];
    [self release];
}


- (CGAffineTransform)transformForOrientation {
	UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
	if (orientation == UIInterfaceOrientationLandscapeLeft) {
		return CGAffineTransformMakeRotation(M_PI*1.5);
	} else if (orientation == UIInterfaceOrientationLandscapeRight) {
		return CGAffineTransformMakeRotation(M_PI/2);
	} else if (orientation == UIInterfaceOrientationPortraitUpsideDown) {
		return CGAffineTransformMakeRotation(-M_PI);
	} else {
		return CGAffineTransformIdentity;
	}
}

- (void)getChkData {
	[self.indicator sizeToFit];
    self.indicator.frame = CGRectMake(
                                           self.view.frame.size.width/2 - self.indicator.frame.size.width/2,
                                           self.view.frame.size.height/2,
                                           self.indicator.frame.size.width,
                                           self.indicator.frame.size.height
                                           );

    [self.indicator startAnimating];
    [self.chkController requestDataList];
}

-(void)reflash {
    if ([self.indicator isAnimating] == NO) {
        [self.tableView.tableFooterView setHidden:YES];
        [self performSelectorOnMainThread:@selector(initTable) withObject:nil waitUntilDone:YES ];
        
        // リワードデータ取得
        [self getChkData];
        
        // フッターのボタンを有効化する。
        NSArray *subview = [self.tableView.tableFooterView subviews];
        for (int i = 0; i < [subview count]; i++) {
            if( [[subview objectAtIndex:i] isKindOfClass:[UIButton class]] ) {
                [[subview objectAtIndex:i] setEnabled:YES];
                [[subview objectAtIndex:i] setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            }
        }
    }
}

- (void) showErrorView {
    // 初回起動、リロード時の接続エラー画面を表示する。
    self.chkView = nil;
    self.chkView = [[[ChkView alloc] init] autorelease];
    self.view = self.chkView;
    
    // add close button
    UIImage* closeImage = [UIImage imageNamed:@"8c_close.png"];
    self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [self.closeButton setImage:closeImage forState:UIControlStateNormal];
    UIColor* color = [UIColor colorWithRed:167.0/255 green:184.0/255 blue:216.0/255 alpha:1];
    [self.closeButton setTitleColor:color forState:UIControlStateNormal];
    [self.closeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.closeButton addTarget:self action:@selector(closeButtonDidPush) forControlEvents:UIControlEventTouchUpInside];
    self.closeButton.titleLabel.font = [UIFont boldSystemFontOfSize:12];
    self.closeButton.showsTouchWhenHighlighted = YES;
    self.closeButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
    [self.view addSubview:self.closeButton];
    
    // add title
    CGFloat titleLabelFontSize = 14;
    self.titleLabel = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
    self.titleLabel.text = @"アプリ一覧";
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.textColor = [ChkView convertHtmlColorToUIColor:CHK_VIEW_APP_TITLE_TEXT_COLOR];
    self.titleLabel.font = [UIFont boldSystemFontOfSize:titleLabelFontSize];
    self.titleLabel.textAlignment = UITextAlignmentCenter;
    self.titleLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    [self.view addSubview:self.titleLabel];
    
	[self.titleLabel sizeToFit];
	self.titleLabel.frame = CGRectMake(
								   RVIEW_BORDER_WIDTH,
								   RVIEW_BORDER_WIDTH,
								   self.view.frame.size.width - RVIEW_BORDER_WIDTH*2,
								   self.titleLabel.frame.size.height + RVIEW_TITLE_MARGIN);
	
	[self.closeButton sizeToFit];
	self.closeButton.frame = CGRectMake(
									self.view.frame.size.width - (self.titleLabel.frame.size.height + RVIEW_BORDER_WIDTH),
									RVIEW_BORDER_WIDTH,
									self.titleLabel.frame.size.height,
									self.titleLabel.frame.size.height);
    
    UIImage* img = [UIImage imageNamed:@"8c_network_error.png"];
    self.networkErrImageView = [[[UIImageView alloc] initWithImage:img] autorelease];
	self.networkErrImageView.frame = CGRectMake(
                                            RVIEW_BORDER_WIDTH +1,
                                            RVIEW_BORDER_WIDTH + 26,
                                            self.view.frame.size.width - RVIEW_BORDER_WIDTH*2 -2,
                                            self.view.frame.size.height - RVIEW_BORDER_WIDTH*2 - 26);
    [self.view addSubview:self.networkErrImageView];
}

-(BOOL) isiOS6 {
    NSArray *versionArray = [ [ [ UIDevice currentDevice]systemVersion]componentsSeparatedByString:@"."];
    
    int majorVersion = [ [ versionArray objectAtIndex:0]intValue];
    
    if( majorVersion >= 6 ) {
        return YES;
    }
    return NO;
}

#pragma mark -
#pragma mark Pubulic method
- (void)show {
	UIWindow* window = [UIApplication sharedApplication].keyWindow;
	if (!window) {
		window = [[UIApplication sharedApplication].windows objectAtIndex:0];
	}
	[window addSubview:self.view];
	

	[self.titleLabel sizeToFit];
	self.titleLabel.frame = CGRectMake(
								   RVIEW_BORDER_WIDTH,
								   RVIEW_BORDER_WIDTH,
								   self.view.frame.size.width - RVIEW_BORDER_WIDTH*2,
								   self.titleLabel.frame.size.height + RVIEW_TITLE_MARGIN);
	
	[self.closeButton sizeToFit];
	self.closeButton.frame = CGRectMake(
									self.view.frame.size.width - (self.titleLabel.frame.size.height + RVIEW_BORDER_WIDTH),
									RVIEW_BORDER_WIDTH,
									self.titleLabel.frame.size.height,
									self.titleLabel.frame.size.height);
	
	self.tableView.frame = CGRectMake(
									 RVIEW_BORDER_WIDTH +1,
									 RVIEW_BORDER_WIDTH + self.titleLabel.frame.size.height,
									 self.view.frame.size.width - RVIEW_BORDER_WIDTH*2 -2,
									 self.view.frame.size.height - RVIEW_BORDER_WIDTH*2 - self.titleLabel.frame.size.height);

    // HEADER VIEW
    if(![self isiOS6]) {
        UIButton *headerButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [headerButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [headerButton setTitle:@"リストを更新する" forState:UIControlStateNormal];
        [headerButton setFrame:CGRectMake(-1,0,self.view.frame.size.width - RVIEW_BORDER_WIDTH*2 -2,45)];
        [headerButton addTarget:self action:@selector(reflash) forControlEvents:UIControlEventTouchUpInside];
        headerButton.backgroundColor = [UIColor clearColor];
        
        UIView *headerView = [[[UIView alloc]initWithFrame: CGRectMake(
                                                                       RVIEW_BORDER_WIDTH +1 -10,
                                                                       RVIEW_BORDER_WIDTH + self.titleLabel.frame.size.height,
                                                                       self.view.frame.size.width - RVIEW_BORDER_WIDTH*2 -2,
                                                                       45)] autorelease];
        headerView.backgroundColor = [ChkView convertHtmlColorToUIColor:CHK_VIEW_APP_BACKGROUND_COLOR];
        [headerView addSubview:headerButton];
        self.tableView.tableHeaderView = headerView;
    }else{
        self.refreshControl = [[[UIRefreshControl alloc] init] autorelease];
        [self.refreshControl addTarget:self action:@selector(reflash) forControlEvents:UIControlEventValueChanged];
        
        [self.tableView addSubview:self.refreshControl];
    }
    
    // FOOTER VIEW
    UIButton *footerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [footerButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [footerButton setTitle:@"続きを読み込む" forState:UIControlStateNormal];
    [footerButton setFrame:CGRectMake(-1,0,self.view.frame.size.width - RVIEW_BORDER_WIDTH*2 -2,45)];
    [footerButton addTarget:self action:@selector(getChkData) forControlEvents:UIControlEventTouchUpInside];
    footerButton.backgroundColor = [UIColor clearColor];

	UIView *footerView = [[[UIView alloc]initWithFrame: CGRectMake(
               RVIEW_BORDER_WIDTH +1 -10,
               RVIEW_BORDER_WIDTH + self.titleLabel.frame.size.height,
               self.view.frame.size.width - RVIEW_BORDER_WIDTH*2 -2,
               45)] autorelease];
    footerView.backgroundColor = [ChkView convertHtmlColorToUIColor:CHK_VIEW_APP_BACKGROUND_COLOR];
    [footerView addSubview:footerButton];
    self.tableView.tableFooterView = footerView;
    
	[self.indicator sizeToFit];
    self.indicator.frame = CGRectMake(
                                      self.view.frame.size.width/2 - self.indicator.frame.size.width/2,
                                      self.view.frame.size.height/2,
                                      self.indicator.frame.size.width,
                                      self.indicator.frame.size.height
                                      );

	[self.chkView show];
}

- (id)_init:(ChkConfig *)config {
	if ((self = [super init])) {
        
        // コントローラ生成
        self.chkController = [[[ChkController alloc] initWithConfigDelegate:config callback:self] autorelease];
        if(self.chkController == nil ) {
            return nil;
        }
        
        self.dataList = [[[NSMutableArray alloc] init]autorelease];

		// viewを作成する
		self.chkView = [[[ChkView alloc] initWithConfig:[self.chkController chkConfig]] autorelease];
		self.view = self.chkView;
		
		// add close button
		UIImage* closeImage = [UIImage imageNamed:@"8c_close.png"];
		self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
		[self.closeButton setImage:closeImage forState:UIControlStateNormal];
		UIColor* color = [UIColor colorWithRed:167.0/255 green:184.0/255 blue:216.0/255 alpha:1];
		[self.closeButton setTitleColor:color forState:UIControlStateNormal];
		[self.closeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
		[self.closeButton addTarget:self action:@selector(closeButtonDidPush) forControlEvents:UIControlEventTouchUpInside];
		self.closeButton.titleLabel.font = [UIFont boldSystemFontOfSize:12];
		self.closeButton.showsTouchWhenHighlighted = YES;
		self.closeButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
		[self.view addSubview:self.closeButton];
		
		// add title
		CGFloat titleLabelFontSize = 14;
		self.titleLabel = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
        self.titleLabel.text = @"アプリ一覧";
		self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.textColor = [ChkView convertHtmlColorToUIColor:CHK_VIEW_APP_TITLE_TEXT_COLOR];
		self.titleLabel.font = [UIFont boldSystemFontOfSize:titleLabelFontSize];
		self.titleLabel.textAlignment = UITextAlignmentCenter;
		self.titleLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
		[self.view addSubview:self.titleLabel];
		
		//make tableview
		self.tableView = [[[UITableView alloc] initWithFrame:[self.view bounds] style:UITableViewStylePlain] autorelease];
		self.tableView.delegate = self;
		self.tableView.dataSource = self;
        self.tableView.backgroundColor = [ChkView convertHtmlColorToUIColor:CHK_VIEW_APP_BACKGROUND_COLOR];
        
		[self.view addSubview:self.tableView];
		
		// indicator
		self.indicator = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
		self.indicator.autoresizingMask =
		UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin
		| UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [self.indicator sizeToFit];
        self.indicator.frame = CGRectMake(
                                          self.view.frame.size.width/2 - self.indicator.frame.size.width/2,
                                          self.view.frame.size.height/2,
                                          self.indicator.frame.size.width,
                                          self.indicator.frame.size.height
                                          );

		[self.view addSubview:self.indicator];
		[self.indicator startAnimating];
		
		main_queue = dispatch_get_main_queue();
		list_queue = dispatch_queue_create("net.adcrops.8chk.listview", NULL);
		image_queue = dispatch_queue_create("net.adcrops.8chk.listview-image", NULL);
        
		// リワードデータ取得
		[self getChkData];

        [self retain];
	}
	return self;
}

#pragma mark -
#pragma mark Initialization
- (id)initWith8chk:(ChkConfig *)config {
    NSLog(@"initWith8chk init.");
    return [self _init:config];
}

- (id)init {
    return [self _init:nil];
}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.dataList count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    ChkTableViewCell *cell = (ChkTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        // iOS4.x
        cell = [[[ChkTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier]autorelease];
    }
    
    ChkRecordData *rewardRecord = (ChkRecordData*)[self.dataList objectAtIndex:indexPath.row];
    
	cell.titleLabel.text = [rewardRecord title];
	cell.category.text = [rewardRecord category];
    NSMutableString *price = [NSMutableString stringWithFormat:@"%@円",[[rewardRecord price] stringValue]];
	cell.price.text = price;
    
    NSMutableString *point = [NSMutableString stringWithFormat:@"%@Pt",[[rewardRecord point]stringValue]];
    cell.point.text = point;
	
    
	if (![rewardRecord cacheImage]) {
        // インプレッション送信
        [self.chkController sendImpression:rewardRecord];
        
        NSString *url = [rewardRecord imageIcon];
		dispatch_async(image_queue, ^{
			UIImage *icon = [self.chkController getImage:url];
            [rewardRecord setCacheImage:icon];
            dispatch_async(main_queue, ^{
                ChkTableViewCell *cell = (ChkTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
                // 角を丸くする。
                cell.appImageView.layer.masksToBounds = YES;
                cell.appImageView.layer.cornerRadius = 10.0f;
                cell.appImageView.image = icon;
                [_tableView reloadData];
            });
		});
	} else {
		cell.appImageView.image = [rewardRecord cacheImage];
	}
    
    return cell;
}

#pragma mark -
#pragma mark Table view delegate
-(void) initTable{
    [self.chkController resetDataList];
    self.dataList = [[[NSMutableArray alloc] init]autorelease];
	[self.tableView reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.detailViewController = [[[ChkDetailViewController alloc] initWithConfig:[self.chkController chkConfig]] autorelease];
    self.detailViewController.appData = (ChkRecordData*)[self.dataList objectAtIndex:indexPath.row];
		
    UIWindow* window = [UIApplication sharedApplication].keyWindow;
    if (!window) {
        window = [[UIApplication sharedApplication].windows objectAtIndex:0];
    }
    [window addSubview:self.detailViewController.view];
}

#pragma mark -
#pragma mark ChkControllerDelegate delegate
- (void)chkControllerDataListWithSuccess:(NSDictionary *)data{
    NSLog(@"chkControllerDataListWithSuccess:%@",data);
    self.dataList = [self.chkController dataList];
    
	// tableをリロードする
	[self.tableView reloadData];
	
    // 次に読み込むものがない時は次のX件をを無効にする。
    if( ![self.chkController hasNextData]) {
        NSArray *subview = [self.tableView.tableFooterView subviews];
        for (int i = 0; i < [subview count]; i++) {
            if( [[subview objectAtIndex:i] isKindOfClass:[UIButton class]] ) {
                [[subview objectAtIndex:i] setEnabled:NO];
                [[subview objectAtIndex:i] setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            }
        }
    }
        
	[self.indicator stopAnimating];
    [self.tableView.tableFooterView setHidden:NO];
    if([self isiOS6]) {
        [self.refreshControl endRefreshing];
    }
}

- (void)chkControllerDataListWithError:(NSError*)error{
    NSLog(@"chkControllerDataListWithError:%@",error);
	[self.indicator stopAnimating];
    [self.tableView.tableFooterView setHidden:NO];
    if([self isiOS6]) {
        [self.refreshControl endRefreshing];
    }
    UIAlertView *alertView = [
                              [UIAlertView alloc]
                              initWithTitle:@"接続エラー"
                              message:@"Wi-Fiネットワークまたは、モバイルデータ通信が利用できませんでした。"
                              delegate:nil
                              cancelButtonTitle:@"OK" 
                              otherButtonTitles:nil
                              ];
    [alertView show];
    [alertView release];
}

- (void) chkControllerDataListWithNotFound:(NSDictionary *)data{
    NSLog(@"chkControllerDataListWithNotFound:%@",data);
	[self.indicator stopAnimating];
    [self.tableView.tableFooterView setHidden:NO];
    if([self isiOS6]) {
        [self.refreshControl endRefreshing];
    }
}

#pragma mark -
#pragma mark ChkControllerNetworkNotifyDelegate delegate
- (void) chkControllerInitNotReachableStatusError:(NSError *)error{
    NSLog(@"###########chkControllerInitNotReachableStatusError:%@",error);
    [self showErrorView];
}

- (void) chkControllerRequestNotReachableStatusError:(NSError *)error{
    NSLog(@"###########chkControllerRequestNotReachableStatusError:%@",error);
    // 2回目以降のエラーはダイアログを表示。
    [self.indicator stopAnimating];
    UIAlertView *alertView = [
                              [UIAlertView alloc]
                              initWithTitle:@"データ取得エラー"
                              message:@"データ取得に失敗しました。しばらくしてから再実行して下さい。"
                              delegate:nil
                              cancelButtonTitle:@"OK" 
                              otherButtonTitles:nil
                              ];
    [alertView show];
    [alertView release];
}

- (void) chkControllerNetworkReachable:(NSUInteger)networkType {
    NSLog(@"########### ChkViewController chkControllerNetworkReachable:%d",networkType);
}

- (void) chkControllerNetworkNotReachable:(NSError*) error {
    NSLog(@"########### ChkViewController chkControllerNetworkNotReachable:%@",error);
}

#pragma mark -
#pragma mark ChkSecurityNotifyDelegate delegate
- (void) chkSecurityError {
    NSLog(@"########### ChkViewController chkSecurityError");
    UIAlertView *alertView = [
                              [UIAlertView alloc]
                              initWithTitle:@"起動エラー"
                              message:@"お使いの端末ではご利用できません。"
                              delegate:nil
                              cancelButtonTitle:@"OK" 
                              otherButtonTitles:nil
                              ];
    [alertView show];
    [alertView release];
}

#pragma mark -
#pragma mark Memory management
- (void)dealloc {
    NSLog(@"***** ChkViewController dealloc");
    self.dataList = nil;
    self.chkView = nil;
    self.tableView = nil;
    self.titleLabel = nil;
    self.closeButton = nil;
    self.indicator = nil;
    self.naviController = nil;
    [self.chkController clearDelegate];
    self.chkController = nil;
    self.networkErrImageView = nil;
    self.detailViewController = nil;

    self.refreshControl = nil;
    
    [super dealloc];
}

@end

@implementation ChkTableViewCell
@synthesize titleLabel = _titleLabel;
@synthesize category = _category;
@synthesize price = _price;
@synthesize point = _point;
@synthesize appImageView = _appImageView;

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
		// Imageの設定
		self.appImageView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"8c_close.png"]] autorelease];
		self.appImageView.frame = CGRectMake(
										 CELL_APP_IMAGE_MARGIN,
										 CELL_APP_IMAGE_MARGIN,
										 CELL_APP_IMAGE_SIZE,
										 CELL_APP_IMAGE_SIZE);
		[self.contentView addSubview:self.appImageView];
		
		// タイトルラベルの設定
		self.titleLabel = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
		self.titleLabel.font = [UIFont boldSystemFontOfSize:14];
		self.titleLabel.frame = CGRectMake(
									   CELL_APP_IMAGE_SIZE + CELL_APP_IMAGE_MARGIN*2,
									   CELL_APP_IMAGE_MARGIN,
									   self.contentView.frame.size.width - CELL_APP_IMAGE_SIZE - CELL_APP_IMAGE_MARGIN*2 - 100, 16.0);
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.adjustsFontSizeToFitWidth = YES;
        self.titleLabel.minimumFontSize = 10;

		[self.contentView addSubview:self.titleLabel];
		
		// categoryの設定
		self.category = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
		self.category.font = [UIFont systemFontOfSize:10];
		self.category.textColor = [UIColor blueColor];
        self.category.backgroundColor = [UIColor clearColor];
		self.category.frame = CGRectMake(
									 CELL_APP_IMAGE_SIZE + CELL_APP_IMAGE_MARGIN*2,
									 (CELL_APP_IMAGE_SIZE + CELL_APP_IMAGE_MARGIN*2)/2,
									 320.0, 20.0);
		[self.contentView addSubview:self.category];
		
		// priceの設定
		UIImageView *ic_price = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"8c_money.png"]] autorelease];
		ic_price.frame = CGRectMake(
									self.contentView.frame.size.width - (CELL_APP_IMAGE_SIZE + CELL_APP_IMAGE_MARGIN*2) -55,
									2.0,
									14,
									14);
		[self.contentView addSubview:ic_price];
		
		self.price = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
		self.price.font = [UIFont boldSystemFontOfSize:14];
		self.price.textColor = [UIColor blueColor];
        self.price.backgroundColor = [UIColor clearColor];
		self.price.frame = CGRectMake(
								  self.contentView.frame.size.width - (CELL_APP_IMAGE_SIZE + CELL_APP_IMAGE_MARGIN*2) -40,
								  2.0,
								  40,
								  20);
		self.price.textAlignment = UITextAlignmentLeft;
		
		[self.contentView addSubview:self.price];
		
		// pointの設定
		UIImageView *ic_point = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"8c_point.png"]] autorelease];
		ic_point.frame = CGRectMake(
									 self.contentView.frame.size.width - (CELL_APP_IMAGE_SIZE + CELL_APP_IMAGE_MARGIN*2) -55,
									 22.0,
									 14,
									 14);
		[self.contentView addSubview:ic_point];
		
		self.point = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
		self.point.font = [UIFont boldSystemFontOfSize:14];
		self.point.textColor = [UIColor blueColor];
        self.point.backgroundColor = [UIColor clearColor];
		self.point.frame = CGRectMake(
								   self.contentView.frame.size.width - (CELL_APP_IMAGE_SIZE + CELL_APP_IMAGE_MARGIN*2) -40,
								   22.0,
								   50,
								   20);
		self.point.textAlignment = UITextAlignmentLeft;
		
		[self.contentView addSubview:self.point];
		
	}
	return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    // Configure the view for the selected state.
    [super setSelected:selected animated:animated];
}

#pragma mark -
#pragma mark Memory management
- (void)dealloc {
    self.appImageView = nil;
    self.titleLabel = nil;
    self.category = nil;
    self.price = nil;
    self.point = nil;
    [super dealloc];
}

@end


