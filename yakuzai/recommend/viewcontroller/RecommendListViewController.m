//
//  RecommendListViewController.m
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "RecommendListViewController.h"
#import "RecommendCell.h"
#import "RecommendList.h"

@interface RecommendListViewController ()<
UITableViewDataSource
, UITableViewDelegate
>{
    NSObject<RecommendListViewControllerDelegate>* delegate;
    RecommendList* recommendList;
    UITableView* tableView_;
    AdstirMraidView* adView;
}

@end

@implementation RecommendListViewController

-(id) initWithDelegate:(NSObject<RecommendListViewControllerDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        recommendList = [YakuUtil recommendList];
        
        NSMutableArray* showedRecommendList = [YakuUtil showedRecommendList];
        for(int i=0,max=[recommendList count];i<max;i++){
            Recommend* recommend = [recommendList get:i];
            if([YakuUtil isReadedRecommend:recommend.id_]){
                [recommend readed];
            }
            if([showedRecommendList indexOfObject:[NSNumber numberWithInt:recommend.id_]] == NSNotFound){
                [recommend setNew];
                NSLog(@"setNew");
            }else{
                [recommend noNew];
            }
        }
        
        [recommendList readedSort];
        
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    UIView* view = self.view;
    UIView* line = [[UIView alloc] initWithFrame:CGRectMake(10, 85.5f, 280, 2.5f)];
    line.backgroundColor = UIColorFromHex(0xdeae3c);
    [view addSubview:line];
    
    float height = 387.5f;
    if([VknDeviceUtil is35Inch]){
        height -= 35;
    }
    CGRect frame = CGRectMake(10, 88, 285, height);
    tableView_ = [[UITableView alloc] initWithFrame:frame];
    tableView_.delegate = self;
    tableView_.dataSource = self;
    tableView_.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView_.backgroundColor = [UIColor clearColor];
    [view addSubview:tableView_];
    
    // ad
    UIView* adViewWrapper = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 280, 100)];
    float scale = 0.877f;
    adView = [[AdstirMraidView alloc] initWithAdSize:kAdstirAdSize320x100 media:ADSTIR_MEDIA_ID spot:ADSTIR_RECOMMEND_BOTTOM_SPOT_ID];
    adView.frame = CGRectMake(-20, 0, kAdstirAdSize320x100.size.width, kAdstirAdSize320x100.size.height);
//    adView.backgroundColor = UIColorFromHex(0xcccccc);
    adView.transform = CGAffineTransformMakeScale(scale, scale);
    [adViewWrapper addSubview:adView];
    tableView_.tableFooterView = adViewWrapper;
    [adView start];
}

-(void) toTop{
    [tableView_ setContentOffset:CGPointMake(0, -80) animated:YES];
}

-(void) tapBack{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- UITableViewDelegate --

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [recommendList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellIdentifier = @"Cell";
    RecommendCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[RecommendCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    Recommend* recommend = [recommendList get:indexPath.row];
    [cell reload:recommend];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 82.5f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Recommend* recommend = [recommendList get:indexPath.row];
    [delegate onSelectRecommend:recommend];
    
    RecommendCell* cell = (RecommendCell*)[tableView_ cellForRowAtIndexPath:indexPath];
    [cell light];
    
    [recommend readed];
    [YakuUtil readRecommend:recommend.id_];
    [VknUtils wait:0.3f callback:^{
        [tableView reloadData];
    }];
//    [VknUtils openBrowser:recommend.linkUrl];
}


@end
