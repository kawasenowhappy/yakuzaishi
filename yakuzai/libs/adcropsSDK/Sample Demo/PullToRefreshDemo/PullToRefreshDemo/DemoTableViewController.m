//
// Created by Tetsuyak on 2014/05/08.
// Copyright (c) 2014 8crops. All rights reserved.
//

#import "DemoTableViewController.h"
#import "AdCropsGumRefreshControl.h"
#import "ChkController.h"
#import "AdCropsConstants.h"


@interface DemoTableViewController () <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>

@property (nonatomic, strong) AdCropsGumRefreshControl *adcropsRefreshControl;
@property (nonatomic, strong) ChkController *chkController;
@property (nonatomic, strong) NSMutableArray *dataSourceArray;

@end


@implementation DemoTableViewController

- (id)init {
	self = [super init];
	if (self) {

	}
	return self;
}


- (void)viewDidLoad {
	[super viewDidLoad];

	// [self.navigationController setNavigationBarHidden:YES animated:NO];
	self.edgesForExtendedLayout = UIRectEdgeNone;

	// AdCrops
	_chkController = [[ChkController alloc] initWithConfigDelegate:nil callback:self];
	[_chkController requestDataList];

	// ダミーデータ
	_dataSourceArray = [[NSMutableArray alloc] init];
	for (int i = 0; i < 100; i++) {
		[_dataSourceArray addObject:[NSString stringWithFormat:@"ダミーデータ【%d】", i]];
	}

	// RefreshControl
	_adcropsRefreshControl = [[AdCropsGumRefreshControl alloc] initWithFrame:CGRectMake(0.f, 0.f, self.view.frame.size.width , kRefreshControlHeightNormal)];
	[_adcropsRefreshControl addTarget:self
					   action:@selector(refreshOccured:)
			 forControlEvents:UIControlEventValueChanged];

	_adcropsRefreshControl.backgroundColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1];
	[self.tableView addSubview:_adcropsRefreshControl];

}
#pragma mark - refreshControl
- (void)refreshOccured:(id)sender {
	// 更新処理をここに記述

	// ここではデモなので2秒後にendRefreshingを呼ぶようにしています。
	[self performSelector:@selector(endRefreshing) withObject:nil afterDelay:2];

}

- (void)endRefreshing {
	// 更新終了
	[_adcropsRefreshControl endRefreshing];
}


#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

/**
 * セルの数
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return 100;
}

#pragma mark - UITableViewDelegate
/**
* セルの高さ
*/
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 40;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	return indexPath;
}

/**
* セルの生成と設定
*/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

	NSString *cellIdentifier = @"Cell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
	}

	cell.textLabel.text = _dataSourceArray[(NSUInteger) indexPath.row];

	return cell;
}


#pragma mark - AdCrops ChkControllerDelegate
- (void)chkControllerDataListWithSuccess:(NSDictionary *)data {
	NSLog(@"%s %@", __PRETTY_FUNCTION__, data);
	_adcropsRefreshControl.chkController = _chkController;
}

- (void)chkControllerDataListWithError:(NSError *)error {
	NSLog(@"%s %@", __PRETTY_FUNCTION__, [error debugDescription]);
}


- (void)dealloc {
	NSLog(@"%s", __PRETTY_FUNCTION__);
}

@end