//
//  ChkSlideIconView.m
//  8chk
//
//  Created by Sazawa Yuki on 02/06/14.
//  Copyright (c) 2014年 8crops inc. All rights reserved.
//

#import "ChkSlideIconView.h"
//#import "ChkAppViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ChkController.h"
#import "ChkControllerDelegate.h"
#import <dispatch/dispatch.h>

enum {
    ChkSlideIconTypeIcon       = 0, // アイコンサイズ
    ChkSlideIconTypeCpc        = 1, // CPCサイズ
}; typedef NSUInteger ChkSlideIconType;

@interface ChkSlideIconView () <ChkControllerDelegate,UIScrollViewDelegate,UIAccelerometerDelegate,SKStoreProductViewControllerDelegate>
@property (nonatomic, strong) ChkController    *chkController;
// 画像のスクロールビュー
@property (nonatomic, strong) UIScrollView              *chkScrollView;
// スクロールビューの背景画像
@property (nonatomic, strong) UIImageView               *chkScrollBackgroundImage;
// スクロールビューのインジケータ
@property (nonatomic, strong) UIActivityIndicatorView   *chkIndicator;
// インプレッション送信カウント用
@property (atomic, retain) NSMutableDictionary          *chkImpressionDic;
@property (nonatomic,strong)UIColor        *chkSlideIconTextColor;
@property (nonatomic,strong)UIColor        *chkSlideIconBackgroundColor;
@property (nonatomic)CGFloat                chkSlideIconBackgroundAlpha;

@end

@implementation ChkSlideIconView

// CPCサイズ
const CGSize kChkSlideIconAdViewCPCSize = {320, 50};
// Iconサイズ
const CGSize kChkSlideIconAdViewICONSize = {320, 82};

// 初回ロード成功フラグ
BOOL _chkLoadSuccessfulFlag = NO;
// バナー種別
//ChkSlideIconType _chkBannerType = ChkSlideIconTypeIcon;
ChkSlideIconType _chkBannerType = ChkSlideIconTypeCpc;

// アプリ名表示フラグ
BOOL _chkShowAppName = NO;

dispatch_queue_t main_queue;
dispatch_queue_t image_queue;

#pragma mark -
#pragma mark Private Method
-(void) sendImpression:(int)listNumber {
    //@synchronized(self) {
        int dataCount = [[self.chkController dataList] count];
        if( dataCount <= listNumber ) {
            return;
        }
        // 1ページに表示される分だけインプレッション送信する
        int max = listNumber - [self getOnePageIconCount];
        if(max<0) max = 0;
        for (int i = listNumber; i >= max; i--) {
            ChkRecordData *data = [[self.chkController dataList] objectAtIndex:i];
            
            NSString *isSend = [self.chkImpressionDic objectForKey:data.appStoreId];
            NSLog(@"***Value:%@",isSend);
            if(![isSend isEqualToString:@"1"]) {
                // インプレッション送信
                NSLog(@"sendImpression:%d:%@:%@",i,data.title,data.appStoreId);
                [self.chkController sendImpression:data];
                [self.chkImpressionDic setObject:@"1" forKey:data.appStoreId];
            }else{
                NSLog(@"****Impression Sended:%d:%@",i,data.title);
            }
        }
        //NSLog(@"self.chkImpressionDic:%@",self.chkImpressionDic);
    //}
}


- (id)initWithCpcSize:(CGFloat)x rootViewController:(UIViewController*)rootViewController {
    return [self initWithFrame:x rootViewController:rootViewController type:ChkSlideIconTypeCpc];
}

- (id)initWithIconSize:(CGFloat)x rootViewController:(UIViewController*)rootViewController {
    return [self initWithFrame:x rootViewController:rootViewController type:ChkSlideIconTypeIcon];
}

- (id)initWithFrame:(CGFloat)x rootViewController:(UIViewController*)rootViewController type:(ChkSlideIconType)type {
    CGRect frame = CGRectZero;
    _chkBannerType = type;
    if(type == ChkSlideIconTypeCpc) {
        frame = CGRectMake(0,
                              x,
                              kChkSlideIconAdViewCPCSize.width,
                              kChkSlideIconAdViewCPCSize.height);
    }else{
        frame = CGRectMake(0,
                           x,
                           kChkSlideIconAdViewICONSize.width,
                           kChkSlideIconAdViewICONSize.height);
    }
    
    if (self = [super initWithFrame:frame]) {
        
        self.rootViewController = rootViewController;
        
        self.chkImpressionDic = [[NSMutableDictionary alloc] init];
        
        main_queue = dispatch_get_main_queue();
		image_queue = dispatch_queue_create("net.adcrops.8chk.list-image", NULL);
        
        NSLog(@"%s : initWithFrame", __func__);
        self.chkController = [[ChkController alloc]initWithDelegate:self];
        self.backgroundColor = [UIColor whiteColor];
        
        // 画像のスクロールビュー
        self.chkScrollView = [[UIScrollView alloc] init];
        self.chkScrollView.frame = CGRectMake(0.0f,
                                              0.0f,
                                              self.frame.size.width,
                                              self.frame.size.height + 4.5f);
        self.chkScrollView.contentSize = CGSizeMake( self.frame.size.width + 1.0f, self.frame.size.height);
        
        
        // スクロールビュー
        self.chkScrollView.pagingEnabled = NO; // ページ単位
        
        self.chkScrollView.backgroundColor = [UIColor colorWithRed:0.839 green:0.843 blue:0.850 alpha:1.0];
        
        self.chkScrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
        [self.chkScrollView setCanCancelContentTouches:NO];
        self.chkScrollView.showsVerticalScrollIndicator = NO;
        self.chkScrollView.showsHorizontalScrollIndicator = NO;
        self.chkScrollView.scrollsToTop = NO;
        
        self.chkScrollView.clipsToBounds = YES;
        self.chkScrollView.scrollEnabled = YES;
        self.chkScrollView.delegate = self;
        
		[self addSubview:self.chkScrollView];
        
        // スクロールビューのインジケータ
        if ([[UIActivityIndicatorView class] respondsToSelector:@selector(appearance)]) {
            // iOS 5.0以上の処理
            self.chkIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            [self.chkIndicator setColor:[UIColor grayColor]]; // インジケータの色
        }else{
                // iOS 5.0未満(iOS 4.3以下)の処理
            self.chkIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        }
        
        self.chkIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin
        | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [self.chkIndicator setCenter:CGPointMake(self.center.x,self.frame.size.height/2 + 2.0f)];
        [self addSubview:self.chkIndicator];
        [self.chkIndicator startAnimating];
        
        __weak __block ChkSlideIconView *blockSelf = self;
        [[NSNotificationCenter defaultCenter]
         addObserverForName:UIApplicationDidEnterBackgroundNotification
         object:nil queue:nil
         usingBlock:^(NSNotification *note) {
             //BG状態へ
             [blockSelf applicationDidEnterBackground];
         }];
        
        [[NSNotificationCenter defaultCenter]
         addObserverForName:UIApplicationWillEnterForegroundNotification
         object:nil queue:nil
         usingBlock:^(NSNotification *note) {
             // FG状態へ
             [blockSelf applicationWillEnterForeground];
         }];
    }
    return self;
}

-(CGFloat) getIconMarginHeight {
    if(_chkBannerType == ChkSlideIconTypeCpc) {
        return 3.0f;
    }
    return 10.0f;
}

-(CGFloat) getIconMarginWidth {
    if(_chkBannerType == ChkSlideIconTypeCpc) {
        return 5.0f;
    }
    return 10.0f;
}

-(CGFloat) getIconSize {
    if(_chkBannerType == ChkSlideIconTypeCpc) {
        return 44.0f;
    }
    return 64.0f;
}

-(int) getOnePageIconCount {
    if(_chkBannerType == ChkSlideIconTypeCpc) {
        return 6;
    }
    return 3;
}

- (void)setShowAppName:(UIColor*)textColor backgroundColor:(UIColor *)backgroundColor backgroundAlpha:(CGFloat)backgroundAlpha{
    _chkShowAppName = YES;
    self.chkSlideIconTextColor = textColor;
    self.chkSlideIconBackgroundColor = backgroundColor;
    self.chkSlideIconBackgroundAlpha = backgroundAlpha;
}

- (void)dealloc {
    NSLog(@"dealloc");
    self.delegate = nil;
    self.rootViewController = nil;
    [self.chkController clearDelegate];
    self.chkController = nil;
    self.chkScrollView  = nil;
    self.chkScrollBackgroundImage   = nil;
    self.chkIndicator       = nil;
    self.chkImpressionDic   = nil;
    self.chkSlideIconBackgroundColor = nil;
    self.chkSlideIconTextColor = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)load {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSLog(@"load");
    for(id subview in self.chkScrollView.subviews) {
        if([subview tag] <= [[self.chkController dataList] count] ) {
            [subview removeFromSuperview];
            NSLog(@"%s : remove subview:%d", __func__, [subview tag]);
        }
    }
    // スクロール位置を初期化
    [self.chkScrollView setContentOffset:CGPointMake(0.0f, 0.0f)];
    
    self.chkImpressionDic = [[NSMutableDictionary alloc] init];
    [self.chkIndicator startAnimating];
    [self.chkController resetDataList];
    [self.chkController requestDataList];
}

- (void) setIndicatorColor:(UIColor*)color {
    if ([[UIActivityIndicatorView class] respondsToSelector:@selector(appearance)]) {
        [self.chkIndicator setColor:color];
    }
}

- (void) applicationDidEnterBackground {
    NSLog(@"applicationDidEnterBackground");
    // subviewがあったら消す。
    for(id subview in self.chkScrollView.subviews) {
        if([subview tag] <= [[self.chkController dataList] count] ) {
            [subview removeFromSuperview];
            NSLog(@"%s : remove subview:%@:%d", __func__, subview,[subview tag]);
        }
    }
}

- (void) applicationWillEnterForeground {
    NSLog(@"applicationWillEnterForeground");
    [self load];
}

#pragma mark -
#pragma mark ChkControllerDelegate delegate
- (void)chkControllerDataListWithSuccess:(NSDictionary *)data{
    NSLog(@"chkControllerDataListWithSuccess:%@",data);
    
    // 最後まで全部取りに行く。
    if( [self.chkController hasNextData] ) {
        NSLog(@"****** retry request *****");
        [self.chkController requestDataList];
        return;
    }
    [self.chkIndicator stopAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    // アイコンを設定する。
    float scroolWidth = 0.0f;
    int i = 0;
    for (ChkRecordData* data in [self.chkController dataList]) {
        
        UIButton *chkIconImage = [UIButton buttonWithType:UIButtonTypeCustom];
        chkIconImage.tag = i;
        [chkIconImage addTarget:self action:@selector(chkIconTap:) forControlEvents:UIControlEventTouchUpInside];
        [chkIconImage addTarget:self action:@selector(chkIconTouchDown:) forControlEvents:UIControlEventTouchDown];
        [chkIconImage addTarget:self action:@selector(chkIconTouchDragExit:) forControlEvents:UIControlEventTouchDragExit];
        
        
        chkIconImage.frame = CGRectMake(0.0f + (i*[self getIconSize]) + ([self getIconMarginWidth]*(i+1)),
                                        [self getIconMarginHeight],
                                        [self getIconSize],
                                        [self getIconSize]);
        
        // 角を丸くする。
        chkIconImage.layer.masksToBounds = YES;
        chkIconImage.layer.cornerRadius = 16.0f;
        
        // アプリアイコン
        if (![data cacheImage]) {
            NSString *url = data.imageIcon;
            dispatch_async(image_queue, ^{
                UIImage *icon = [self.chkController getImage:url];
                // 画像イメージをキャッシュする。
                [data setCacheImage:icon];
                dispatch_async(main_queue, ^{
                    [chkIconImage setImage:icon forState:UIControlStateNormal];
                });
            });
        } else {
            [chkIconImage setImage:[data cacheImage] forState:UIControlStateNormal];
        }

        
        scroolWidth += chkIconImage.frame.size.width + [self getIconMarginWidth];
        
        if(_chkShowAppName) {
            // アプリ名ラベルを付ける。
            UILabel *appNameLabel = [[UILabel alloc] init];
            appNameLabel.backgroundColor = self.chkSlideIconBackgroundColor;
            appNameLabel.textColor = self.chkSlideIconTextColor;
            appNameLabel.highlightedTextColor = [UIColor grayColor];
            appNameLabel.frame = CGRectMake(0.0f,
                                            [self getIconSize] - [self getIconSize]/4,
                                            [self getIconSize],
                                            [self getIconSize]/4);
            appNameLabel.alpha = self.chkSlideIconBackgroundAlpha;
            appNameLabel.text = data.title;
            [appNameLabel setAdjustsFontSizeToFitWidth:NO];
            if(_chkBannerType == ChkSlideIconTypeCpc) {
                [appNameLabel setFont:[UIFont boldSystemFontOfSize:8.0]];
            }else{
                [appNameLabel setFont:[UIFont boldSystemFontOfSize:10.0]];
            }
            
            [appNameLabel setTextAlignment:NSTextAlignmentCenter];
            [chkIconImage addSubview:appNameLabel];
        }
        
        // インプレッション送信する。1ページ目に表示される物は表示時にインプレッション送信する。
        if(i <= [self getOnePageIconCount]) {
            [self sendImpression:i];
        }
        
        [self.chkScrollView addSubview:chkIconImage];
        i++;
    }
    if(scroolWidth + [self getIconMarginWidth] < self.frame.size.width ) {
        self.chkScrollView.contentSize = CGSizeMake( self.frame.size.width + 1.0f, self.frame.size.height);
    }else{
        self.chkScrollView.contentSize = CGSizeMake( scroolWidth + [self getIconMarginWidth], self.frame.size.height);
    }
    
    if(!_chkLoadSuccessfulFlag && [[self.chkController dataList] count] > 0) {
        // 初回の受信成功時
        [self setHidden:NO];
        _chkLoadSuccessfulFlag = YES;
        if ([self.delegate respondsToSelector:@selector(chkBannerViewDidFinishLoad:)]) {
            [self.delegate chkBannerViewDidFinishLoad:self];
        }
    }else{
        if([[self.chkController dataList] count] == 0 ) {
            // 案件無しはエラーとする。
            [self setHidden:YES];
            if ([self.delegate respondsToSelector:@selector(chkBannerViewDidFailToReceiveAd:)]) {
                [self.delegate chkBannerViewDidFailToReceiveAd:self];
            }
        }else{
            // 2回目以降の受信成功時
            [self setHidden:NO];
            if ([self.delegate respondsToSelector:@selector(chkBannerViewDidReceiveAd:)]) {
                [self.delegate chkBannerViewDidReceiveAd:self];
            }
        }
    }
}

- (void)chkControllerDataListWithError:(NSError*)error{
    NSLog(@"chkControllerDataListWithError");
    [self.chkIndicator stopAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    [self setHidden:YES];
    if ([self.delegate respondsToSelector:@selector(chkBannerViewDidFailToReceiveAd:)]) {
        [self.delegate chkBannerViewDidFailToReceiveAd:self];
    }
}

#pragma mark -
#pragma mark UIScrollViewDelegate delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    float x = scrollView.contentOffset.x;
    
    if(x>0) {
        // インプレッション
        float oneSize = [self getIconSize] + [self getIconMarginWidth];
        int iconCount = (x+self.frame.size.width)/oneSize;
        [self sendImpression:iconCount];
    }
} 

#pragma mark -
#pragma mark UIButton Action Method
- (void) chkIconTouchDown:(UIButton*) sender {
    for(UILabel *subview in sender.subviews) {
        subview.highlighted = YES;
    }
}

- (void) chkIconTouchDragExit:(UIButton*) sender {
    for(UILabel *subview in sender.subviews) {
        subview.highlighted = NO;
    }
}

- (void) chkIconTap:(UIButton*) sender {
    
    for(UILabel *subview in sender.subviews) {
        subview.highlighted = NO;
    }

    if([[self.chkController dataList]count] > 0 ) {
        ChkRecordData *recordData = [[ChkRecordData alloc] init];
        recordData = [[self.chkController dataList] objectAtIndex:sender.tag];
        NSLog(@"click url:%@",recordData.linkUrl);
        UIApplication *application = [UIApplication sharedApplication];
        NSURL *url = [NSURL URLWithString:recordData.linkUrl];
        
        //入稿URLが計測URLの場合Safari起動
        if (recordData.isMeasuring) {
            
            if( [application canOpenURL:url]){
                [application openURL:url];
            }else{
                NSLog(@"openUrl error.");
            }
            
        } else {
            
            if(NSClassFromString(@"SKStoreProductViewController")) { //ios6のバージョンの処理
                
                NSURLRequest  *request = [[NSURLRequest alloc] initWithURL:url];
                
                
                [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                    
                    if (error) {
                        // エラー処理を行う。
                        if (error.code == -1003) {
                            NSLog(@"not found hostname. targetURL=%@", url);
                        } else if (error.code == -1019) {
                            NSLog(@"auth error. reason=%@", error);
                        } else {
                            NSLog(@"unknown error occurred. reason = %@", error);
                        }
                        
                    } else {
                        int httpStatusCode = ((NSHTTPURLResponse *)response).statusCode;
                        if (httpStatusCode == 404) {
                            NSLog(@"404 NOT FOUND ERROR. targetURL=%@", url);
                            // } else if (・・・) {
                            // 他にも処理したいHTTPステータスがあれば書く。
                            
                        } else {
                            NSLog(@"success request!!");
                                
                            NSDictionary *productParameters = @{ SKStoreProductParameterITunesItemIdentifier:recordData.appStoreId};
                                
                            SKStoreProductViewController* storeviewController = [[SKStoreProductViewController alloc] init];
                            storeviewController.delegate = self;

                            [storeviewController loadProductWithParameters:productParameters completionBlock:^(BOOL result, NSError *error) {
                                if (result) {
                                    [self.rootViewController presentViewController:storeviewController animated:YES completion:nil];
                                }
                                
                            }];

                        }
                    }
                }];
                
            } else { //ios6以前のバージョンの処理
                
                if( [application canOpenURL:url]){
                    [application openURL:url];
                }else{
                    NSLog(@"openUrl error.");
                }
            }
        }
    }
}

#pragma mark -
#pragma mark SKStoreProductViewController delegate

- (void)productViewControllerDidFinish:(SKStoreProductViewController*)viewController
{
	[self.rootViewController dismissViewControllerAnimated:YES completion:nil];
    
}




@end
