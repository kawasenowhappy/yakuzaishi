//
//  ViewController.m
//  AdcropsISAutoDemo
//
//  Created by sazawayuki on 2014/03/06.
//  Copyright (c) 2014年 8crops inc. All rights reserved.
//

#import "ViewController.h"
#import "ChkISAutoView.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor whiteColor];

    // スクロール広告
    ChkISAutoView *chkSlideIconView = [[ChkISAutoView alloc]initWithCpcSize:self.view.frame.size.height - kChkISAutoViewCPCSize.height rootViewController:self];
    [chkSlideIconView setShowAppName:[UIColor whiteColor] backgroundColor:[UIColor clearColor] backgroundAlpha:0.8];
    [chkSlideIconView setIndicatorColor:[UIColor blackColor]];
    [self.view addSubview:chkSlideIconView];
    [chkSlideIconView load];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
