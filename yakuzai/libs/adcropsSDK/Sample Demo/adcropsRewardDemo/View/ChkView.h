//
//  ChkView.h
//  8chk
//
//  Created by 8crops Inc. on 11/06/14.
//  Copyright 2011 8crops Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "ChkConfig.h"

#define RVIEW_FRAME_PADDING 10.0f
#define RVIEW_BORDER_WIDTH 10.0f
#define RVIEW_TRANSITION_DURATION 0.3f
#define RVIEW_TITLE_MARGIN 8.0f

// アニメーション種別
enum {
    ChkViewAnimationNone            = 0,    // アニメーションなし。
    ChkViewAnimationDefault         = 1,    // bounce?デフォルト(なんて言うんだろう。)
    ChkViewAnimationCurl            = 2,    // ページめくり
    ChkViewAnimationFadeInOut       = 3,    // フェードイン/アウト
    ChkViewAnimationScrollUp        = 4,    // 下から上へスクロール
    ChkViewAnimationScrollDown      = 5,    // 上から下へスクロール
    ChkViewAnimationScrollLeft      = 6,    // 右から左へスクロール
    ChkViewAnimationScrollRight     = 7,    // 左から右へスクロール
    ChkViewAnimationFlipFromLeft    = 8,    // 左から右に裏返す
    ChkViewAnimationFlipFromRight   = 9,    // 右から左に裏返す
}; typedef NSUInteger ChkViewAnimationType;

@interface ChkView : UIView{
}
@property (nonatomic, retain) ChkConfig     *chkConfig;

- (id)initWithConfig:(ChkConfig*)config;

- (void)show;
- (void)hide;

+ (UIColor*)convertHtmlColorToUIColor:(NSString *)htmlcolor;

@end

