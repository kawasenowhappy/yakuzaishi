//
//  RecommendCell.m
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "RecommendCell.h"

@interface RecommendCell (){
    Recommend* recommend;
    UILabel* titleLabel;
    UILabel* excerptLabel;
    UIImageView* icon;
    UIImageView* arrow;
    
    UIView* overView;
    UIImageView* newIcon;
}

@end

@implementation RecommendCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initialize];
    }
    return self;
}

-(void) initialize{
    self.backgroundColor = [UIColor clearColor];
    UIView* view = self.contentView;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    overView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 82)];
    overView.backgroundColor = UIColorFromHex(0xffec9f);
    overView.alpha = 0;
    [view addSubview:overView];
    
    UIView* wrapView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 82.5f)];
    [view addSubview:wrapView];
    
    icon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, 60, 60)];
    icon.contentMode = UIViewContentModeScaleAspectFill;
    icon.layer.cornerRadius = 14;
    icon.layer.borderWidth = 2;
    icon.clipsToBounds = YES;
    [wrapView addSubview:icon];
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(65, 10, 200, 17)];
    titleLabel.font = [UIFont boldSystemFontOfSize:14];
    titleLabel.textColor = UIColorFromHex(0x76452c);
    [wrapView addSubview:titleLabel];
    
    excerptLabel = [[UILabel alloc] initWithFrame:CGRectMake(65, 34, 200, 37)];
    excerptLabel.font = [UIFont systemFontOfSize:10.0f];
    excerptLabel.numberOfLines = 0;
    [wrapView addSubview:excerptLabel];
    
    UIImageView* line  = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lineYellow"]];
    line.frame = CGRectMake(0, 80.5f, 280, 2);
    [wrapView addSubview:line];
    
    arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrowYellow"]];
    arrow.frame = CGRectMake(280-13, 30, 13, 21.5f);
    [wrapView addSubview:arrow];
    
    newIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"listNewIcon"]];
    newIcon.frame = CGRectMake(60-30+2, 7, 30, 15);
    [wrapView addSubview:newIcon];
}

-(void) reload:(Recommend *)recommend_{
    recommend = recommend_;
    icon.image = nil;
    
    titleLabel.text = recommend.title;
    excerptLabel.text = recommend.excerpt;
    
    [VknImageCache loadImage:recommend.iconUrl defaultImage:nil callback:^(UIImage *image, NSString *key, BOOL useCache) {
        if(![key isEqualToString:recommend.iconUrl]) return;
        [VknThreadUtil mainBlock:^{
            icon.image = image;
        }];
    }];
    
    newIcon.hidden = ![recommend isNew];
    
    if(!newIcon.hidden){
        newIcon.transform = CGAffineTransformMakeScale(1.6f, 1.6f);
        newIcon.alpha = 0;
        [UIView animateWithDuration:0.5f delay:0.4f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            newIcon.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
            newIcon.alpha = 1.0;
        } completion:^(BOOL finished) {
            
        }];
    }
    
    if(recommend.isReaded){
        arrow.image = [UIImage imageNamed:@"readedArrowYellow"];
        excerptLabel.textColor= UIColorFromHex(0xddb13a);
        titleLabel.textColor = UIColorFromHex(0xddb13a);
        icon.layer.borderColor = UIColorFromHex(0xb86f19).CGColor;
        return;
    }
    
    arrow.image = [UIImage imageNamed:@"arrowYellow"];
    titleLabel.textColor = excerptLabel.textColor= UIColorFromHex(0xb86f19);
    icon.layer.borderColor = UIColorFromHex(0xb86f19).CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void) light{
    [recommend noNew];
    newIcon.hidden = YES;
    overView.alpha = 0.5f;
    SEL sel = @selector(updateLight:);
    [Tween addTween:self tweenId:99 startValue:0.5f endValue:0.0 time:0.3 delay:0 easing:@"easeInCirc" startSEL:sel updateSEL:sel endSEL:sel];
}

-(void) updateLight:(TweenObject*)tween{
    double val = tween.currentValue;
    overView.alpha = val;
}

@end
