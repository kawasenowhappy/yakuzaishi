//
//  SiteList.m
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "SiteList.h"

@implementation SiteList

-(Site*)get:(int)index{
    return (Site*)[super get:index];
}

+(SiteList*)createModelList{
    SiteList* list = [[SiteList alloc] init];
    
    // dummy
    for(int i=0;i<20;i++){
        Site* site = [[Site alloc] init];
        site.title = @"看護のお仕事";
        site.linkUrl = @"https://kango-oshigoto.jp/support/regist-input/";
        site.iconUrl = @"http://kango-oshigoto.jp/img/style/banSpecialContents.jpg";
        site.excerpt = @"看護師さんと寄り添い向き合う転職サイト\nそしてあなたが登録すると僕らにお金が入ります、あざーす。";
        site.content = @"「看護のお仕事」では、経験豊富なキャリアコンサルタントによる無料の転職フルサポートサービスを行なっております。\n\n転職は、「悩む」ところから始まり、希望の病院の「情報収集や選択」、「面接の設定」、「条件交渉」などといった難関がいくつもあります。「看護のお仕事」では、そのひとつひとつを丁寧にサポート。全国10万件以上の施設の中から、あなたの希望条件にあう職場をご紹介させていただきます。これだけのサポートが無料で受けられるのは、多くの看護師さんが「看護のお仕事」を通して転職されているからこそ。 \n\n転職されなかった場合も相談料などはいただいておりません。掲載以外のご希望病院でもしっかりサポートいたしますので、まずはお気軽にご相談ください。";
        [list add:site];
    }
    
    return list;
}


-(void) reloadWithJson:(NSArray*)jsonArray{
    int index = 0;
    for(NSDictionary* row in jsonArray){
        Site* site = [[Site alloc] initWithJson:row];
        [self replace:site at:index];
        index++;
        
        if([YakuUtil isReadedSite:site.id_]){
            [site readed];
        }
    }
    
    [self removeToTail:index];
    
    [self readedSort];
}

-(void) readedSort{
    SiteList* readedList = [[SiteList alloc] init];
    for(int i=0;i<[self count];i++){
        Site* site = [self get:i];
        
        if(site.isReaded){
            [items removeObject:site];
            [readedList add:site];
            i--;
        }
    }
    
    for(int i=0,max=[readedList count];i<max;i++){
        [self add:[readedList get:i]];
    }
}

@end
