//
//  NewsContents.m
//  yakuzai
//
//  Created by kawase yu on 2014/07/12.
//  Copyright (c) 2014年 nowhappy. All rights reserved.
//

#import "NewsContents.h"
#import "NewsListViewController.h"
#import "NewsDetailViewController.h"

@interface NewsContents ()<
NewsListViewControllerDelegate
, NewsDetailViewControllerDelegate
>{
    UIView* view;
    NSObject<NewsContentsDelegate>* delegate;
    UIButton* showButton;
    UIImageView* bgView;
    UIView* newIcon;
    UINavigationController* navigationController;
}

@end

@implementation NewsContents

-(id) initWithDelegate:(NSObject<NewsContentsDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
//    605, 891
    CGRect frame = [self baseFrame];
    frame.size.height = [self hideHeight];
    view = [[UIView alloc] initWithFrame:frame];
    view.clipsToBounds = YES;
    UIImage* bgImage = [UIImage imageNamed:@"kamiRed"];
    bgImage = [bgImage stretchableImageWithLeftCapWidth:7 topCapHeight:7];
    
    frame.origin.x = frame.origin.y = 0;
    bgView = [[UIImageView alloc] initWithFrame:frame];
    bgView.image = bgImage;
    [view addSubview:bgView];
    
    showButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [showButton setImage:[UIImage imageNamed:@"showNewsButton"] forState:UIControlStateNormal];
    showButton.frame = CGRectMake(0, [self hideHeight]-44-5, 300, 44);
    [showButton addTarget:self action:@selector(tapShow:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:showButton];
    
    newIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"newIconRed"]];
    newIcon.frame = CGRectMake(215, showButton.frame.origin.y + 9, 30.5f, 22.5f);
    newIcon.hidden = YES;
    [view addSubview:newIcon];
}

-(void) tapShow:(UIButton*)button{
    [YakuUtil pain:button];
    
    [delegate onContentsShow:2];
    [self show];
}

-(CGRect) baseFrame{
    return CGRectMake(11, 28, 302.0f, [VknDeviceUtil windowHeight]);
}

-(float)hideHeight2{
    return 44.0f;
}

-(float) hideHeight{
//    NSLog(@"hideHeight:%f", [VknDeviceUtil windowHeight] - 223.0f + 82 + 4);
    float height = 431.000000;
    if([VknDeviceUtil is35Inch]){
        height -= 35;
    }
    return height;
}

-(float)showHeight{
//    NSLog(@"showHeight:%f", [VknDeviceUtil windowHeight] - 85.5f - 5);
    float height = 477.5f;
    if([VknDeviceUtil is35Inch]){
        height -= 35;
    }
    return height;
//    return [VknDeviceUtil windowHeight] - 85.5f - 5;
}

-(void) show{
    CGRect frame = [self baseFrame];
    frame.size.height = [self showHeight];
    
    CGRect bgFrame = frame;
    bgFrame.origin.x = 0;
//    bgFrame.size.height += 5;
    bgFrame.size.height -= 27;
    
    showButton.alpha = 1.0f;
    showButton.userInteractionEnabled = NO;
    newIcon.alpha = 0.0;
    newIcon.hidden = YES;
    
    NewsListViewController* listViewController = [[NewsListViewController alloc] initWithDelegate:self];
    navigationController = [[UINavigationController alloc] initWithRootViewController:listViewController];
    [navigationController setNavigationBarHidden:YES];
    
    [UIView animateWithDuration:0.3f animations:^{
        view.frame = frame;
        bgView.frame = bgFrame;
        float toY = -340;
        if([VknDeviceUtil is35Inch]){
            toY += 35;
        }
        showButton.transform = CGAffineTransformMakeTranslation(0, toY);
    } completion:^(BOOL finished) {
        UIView* navigationView = navigationController.view;
        navigationView.alpha = 0;
        [view addSubview:navigationView];
        [UIView animateWithDuration:0.2f animations:^{
            navigationView.alpha = 1.0f;
            [YakuUtil showNewsList];
        }];
    }];
}

-(void) hide{
    CGRect frame = [self baseFrame];
    frame.size.height = [self hideHeight];
    
    CGRect bgFrame = frame;
    bgFrame.origin.x = bgFrame.origin.y = 0;
    showButton.userInteractionEnabled = YES;
    [UIView animateWithDuration:0.3f animations:^{
        view.frame = frame;
        bgView.frame = bgFrame;
        showButton.alpha = 1.0f;
        showButton.transform = CGAffineTransformMakeTranslation(0, 0);
        navigationController.view.alpha = 0;
    } completion:^(BOOL finished) {
        newIcon.alpha = 1.0f;
        [navigationController.view removeFromSuperview];
        navigationController = nil;
    }];
}

-(void) hide2{
    CGRect frame = [self baseFrame];
    frame.origin.y = 30;
    frame.size.height = [self hideHeight2];
    
    CGRect bgFrame = frame;
    bgFrame.origin.x = bgFrame.origin.y = 0;
    showButton.alpha = 0.0f;
    newIcon.alpha = 0.0f;
    [UIView animateWithDuration:0.2f animations:^{
        view.frame = frame;
        bgView.frame = bgFrame;
    } completion:^(BOOL finished) {
        
    }];
}

-(UIView*) getView{
    return view;
}

-(void) onSelect:(News *)news{
    [YakuUtil readNews:news.id_];
    [news readed];
    NewsDetailViewController* detailViewController = [[NewsDetailViewController alloc] initWithDelegate:self andNews:news];
    [[YakuUtil rootViewController] presentViewController:detailViewController animated:YES completion:^{
        
    }];
}

-(void) hasNew{
    newIcon.hidden = NO;
}

@end
