//
//  ChkIconView.m
//  iconDemo
//
//  Created by sazawayuki on 2013/04/26.
//  Copyright (c) 2013年 8crops inc. All rights reserved.
//

#import "ChkIconView.h"
#import "ChkControllerNetworkNotifyDelegate.h"

@interface ChkIconView () <ChkControllerDelegate, SKStoreProductViewControllerDelegate, ChkControllerNetworkNotifyDelegate>{
    NSInteger iconCount_;
    BOOL chkShowAppName_;
    BOOL chkLoadSuccessfulFlag_;
    dispatch_queue_t main_queue_;
    dispatch_queue_t image_queue_;
}
@property(strong, nonatomic) ChkController *chkController;
@property(weak, nonatomic) UIViewController *rootViewController;
@property(strong, nonatomic) UIActivityIndicatorView *indicator;
@property(strong, nonatomic) NSMutableDictionary *chkImpressionDic;

@end

const float kChkAppIconSize = 57.0;
const float kChkMarginTop = 5.0;

@implementation ChkIconView

#pragma mark -
#pragma mark Private Method

- (void)sendImpression:(int)listNumber {
    int dataCount = [[self.chkController dataList] count];
    if (dataCount <= listNumber) {
        return;
    }
    // 1ページに表示される分だけインプレッション送信する
    int max = listNumber - iconCount_;
    if (max < 0) max = 0;
    for (int i = listNumber; i >= max; i--) {
        ChkRecordData *chkData = [[self.chkController dataList] objectAtIndex:i];

        NSString *isSend = [self.chkImpressionDic objectForKey:chkData.appStoreId];
        NSLog(@"%s***Value:%@", __func__ ,isSend);
        if (![isSend isEqualToString:@"1"]) {
            // インプレッション送信
            NSLog(@"sendImpression:%d:%@:%@", i, chkData.title, chkData.appStoreId);
            [self.chkController sendImpression:chkData];
            [self.chkImpressionDic setObject:@"1" forKey:chkData.appStoreId];
        } else {
            NSLog(@"%s****Impression Sended:%d:%@", __func__, i, chkData.title);
        }
    }
    //NSLog(@"self.chkImpressionDic:%@",self.chkImpressionDic);
}

-(NSString*) getLanguageEnvironment
{
	// 言語配列を得る
	NSArray* languageList = [NSLocale preferredLanguages];
	// 使用中の言語は言語配列の先頭の項目となる
	return [languageList objectAtIndex:0];
}

-(void)checkDeviceLanguage{
    // 言語環境により処理を分ける
    NSString *lang = [self getLanguageEnvironment];
    NSString *locale = [[NSLocale currentLocale] objectForKey:NSLocaleIdentifier];
    if ([lang isEqualToString:@"ja"] && [locale isEqualToString:@"ja_JP"]) {
        //日本語環境
    } else {
        // その他の言語環境
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"アラート" message:@"端末の言語、書式を日本に設定して下さい。" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (id)initWithFrame:(CGRect)frame rootViewController:(UIViewController *)rootViewController iconCount:(NSInteger)count iconSize:(float)iconSize{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self checkDeviceLanguage];
        
        main_queue_ = dispatch_get_main_queue();
        image_queue_ = dispatch_queue_create("net.adcrops.8chk.list-image", NULL);

        self.backgroundColor = [UIColor whiteColor];
        self.chkController = [[ChkController alloc] initWithDelegate:self];
        self.rootViewController = rootViewController;
        self.chkImpressionDic = [[NSMutableDictionary alloc] init];

        iconCount_ = count;
        for (int i = 0; i < count; i++) {
            int indexX = i % count;      // 横方向
            float centerX = (iconSize + 10) * indexX;

            UIButton *chkIconImage = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [chkIconImage addTarget:self action:@selector(chkIconTap:) forControlEvents:UIControlEventTouchUpInside];
            [chkIconImage addTarget:self action:@selector(chkIconTouchDown:) forControlEvents:UIControlEventTouchDown];
            [chkIconImage addTarget:self action:@selector(chkIconTouchDragExit:) forControlEvents:UIControlEventTouchDragExit];
            chkIconImage.tag = i + 1;
            chkIconImage.frame = CGRectMake(centerX + (kChkMarginTop * (i + kChkMarginTop)),
                    kChkMarginTop,
                    iconSize,
                    iconSize);
            chkIconImage.layer.masksToBounds = YES;
            chkIconImage.layer.cornerRadius = 10.0f;
            [self addSubview:chkIconImage];

            // アプリ名ラベルを付ける。
            UILabel *appNameLabel = [[UILabel alloc] init];
            appNameLabel.backgroundColor = [UIColor clearColor];
            appNameLabel.textColor = [UIColor blackColor];
            appNameLabel.highlightedTextColor = [UIColor grayColor];
            appNameLabel.frame = CGRectMake(centerX + (kChkMarginTop * (i + kChkMarginTop)),
                    kChkMarginTop + chkIconImage.frame.size.height,
                    chkIconImage.frame.size.width,
                    10);
            [appNameLabel setFont:[UIFont boldSystemFontOfSize:8.0]];
            appNameLabel.tag = i + 10;
            [appNameLabel setAdjustsFontSizeToFitWidth:NO];
            [appNameLabel setTextAlignment:NSTextAlignmentCenter];
            [self addSubview:appNameLabel];
        }

        // indicator作成
        self.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        self.indicator.color = [UIColor grayColor];
        self.indicator.autoresizingMask =
                UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin
                        | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        self.indicator.center = CGPointMake(160, 45);
        [self addSubview:self.indicator];
        
        __weak __block ChkIconView *blockSelf = self;
        [[NSNotificationCenter defaultCenter]
                addObserverForName:UIApplicationDidEnterBackgroundNotification
                            object:nil queue:nil
                        usingBlock:^(NSNotification *note) {
                            //BG状態へ
                            [blockSelf applicationDidEnterBackground];
                        }];
        
        [[NSNotificationCenter defaultCenter]
                addObserverForName:UIApplicationWillEnterForegroundNotification
                            object:nil queue:nil
                        usingBlock:^(NSNotification *note) {
                            // FG状態へ
                            [blockSelf applicationWillEnterForeground];
                        }];
    }
    return self;
}

#pragma mark -
#pragma mark ChkControllerDelegate delegate
- (void)chkControllerDataListWithSuccess:(NSDictionary *)data {

    // 最後まで全部取りに行く。
    if ([self.chkController hasNextData]) {
        NSLog(@"%s****** retry request *****", __func__);
        //[self.chkController requestDataList];
        //return;
    }

    // アイコンを設定する。
    if ([[self.chkController dataList] count]<iconCount_) {
        int chkIconCount = [[self.chkController dataList] count];
        for (int i = 0; i < iconCount_; i++) {
            UIButton *iconImage = (UIButton *) [self viewWithTag:i + 1];
            if (iconImage.tag > chkIconCount) {
                [iconImage removeFromSuperview];
            }
        }
        iconCount_ = [[self.chkController dataList] count];
    }

    for (int i = 0; i < iconCount_; i++) {
        ChkRecordData *chkData = [[self.chkController dataList] objectAtIndex:i];

        // アプリアイコン
        UIButton *iconImage = (UIButton *) [self viewWithTag:i + 1];
        if (![chkData cacheImage]) {
            NSString *url = chkData.imageIcon;
            dispatch_async(image_queue_, ^{
                UIImage *icon = [self.chkController getImage:url];
                // 画像イメージをキャッシュする。
                [chkData setCacheImage:icon];
                dispatch_async(main_queue_, ^{
                    [iconImage setBackgroundImage:icon forState:UIControlStateNormal];
                });
            });
        } else {
            [iconImage setBackgroundImage:[chkData cacheImage] forState:UIControlStateNormal];
        }

        UILabel *appName = (UILabel *) [self viewWithTag:i + 10];
        if (chkShowAppName_) {
            appName.hidden = NO;
            appName.text = chkData.title;
        } else {
            appName.hidden = YES;
        }

        // インプレッション送信する。
        if (i <= iconCount_) {
            [self sendImpression:i];
        }
    }

    if (!chkLoadSuccessfulFlag_ && [[self.chkController dataList] count] > 0) {
        // 初回の受信成功時
        [self setHidden:NO];
        chkLoadSuccessfulFlag_ = YES;
        if ([self.delegate respondsToSelector:@selector(chkBannerViewDidFinishLoad:)]) {
            [self.delegate chkBannerViewDidFinishLoad:self];
        }
    } else {
        if ([[self.chkController dataList] count] == 0) {
            // 案件無しはエラーとする。
            [self setHidden:YES];
            if ([self.delegate respondsToSelector:@selector(chkBannerViewDidFailToReceiveAd:)]) {
                [self.delegate chkBannerViewDidFailToReceiveAd:self];
            }
        } else {
            // 2回目以降の受信成功時
            [self setHidden:NO];
            if ([self.delegate respondsToSelector:@selector(chkBannerViewDidReceiveAd:)]) {
                [self.delegate chkBannerViewDidReceiveAd:self];
            }
        }
    }

    [self.indicator stopAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

}

- (void)chkControllerDataListWithError:(NSError *)error {
    if ([self.delegate respondsToSelector:@selector(chkBannerViewDidFailToReceiveAd:)]) {
        [self.delegate chkBannerViewDidFailToReceiveAd:self];
    }
}

- (void)load {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSLog(@"%s : load", __func__);
    self.chkImpressionDic = [[NSMutableDictionary alloc] init];
    [self.indicator startAnimating];
    [self.chkController resetDataList];
    [self.chkController requestDataList];
}


- (void)applicationDidEnterBackground {
    NSLog(@"%s : applicationDidEnterBackground", __func__);
    for (int i = 0; i < iconCount_; i++) {
        UIButton *iconImage = (UIButton *) [self viewWithTag:i + 1];
        [iconImage setBackgroundImage:nil forState:UIControlStateNormal];
        UILabel *appName = (UILabel *) [self viewWithTag:i + 10];
        appName.text = nil;
    }
}

- (void)applicationWillEnterForeground {
    NSLog(@"%s : applicationWillEnterForeground",__func__);
    [self load];
}

#pragma mark - chkControllerNetworkNotifyDelegate
-(void)chkControllerInitNotReachableStatusError:(NSError *)error{
    //ChkController⽣生成時にネットワークに接続できない場合に呼び出されます。
    [self.indicator stopAnimating];
}

-(void)chkControllerNetworkNotReachable:(NSError *)error{
    //ネットワークの状況が接続→切切断に変わった時に呼び出されます。
    [self.indicator stopAnimating];
}

-(void)chkControllerNetworkReachable:(NSUInteger)networkType{
    //ネットワークの状況が切切断→接続に変わった時に呼び出されます。引数には、WiFiにつながった場合1、3Gにつながった場合は2が設定されます。
    [self.indicator startAnimating];
}

-(void)chkControllerRequestNotReachableStatusError:(NSError *)error{
    //データ取得時にネットワークに接続できない場合に呼び出されます。
    [self.indicator stopAnimating];
}

#pragma mark -
#pragma mark UIButton Action Method
- (void)chkIconTouchDown:(UIButton *)sender {
    if (sender.tag >= 0) {
        [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1f, 1.1f);
        }                completion:nil];
        for (UILabel *subview in self.subviews) {
            if (sender.tag + 9 == subview.tag) {
                subview.highlighted = YES;
            }
        }
    }

}

- (void)chkIconTouchDragExit:(UIButton *)sender {
    if (sender.tag >= 0) {
        [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0f, 1.0f);
        }                completion:NO];
        for (UILabel *subview in self.subviews) {
            subview.highlighted = NO;
        }
    }
}

- (void)chkIconTap:(UIButton *)sender {
    if (sender.tag >= 0) {
        [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0f, 1.0f);
        }                completion:NO];
        for (UILabel *subview in self.subviews) {
            subview.highlighted = NO;
        }
        for (UIButton *iconView in self.subviews) {
            iconView.enabled = NO;
        }
    }

    if ([[self.chkController dataList] count] > 0) {
        //ChkRecordData *recordData;
        ChkRecordData *recordData = [[self.chkController dataList] objectAtIndex:sender.tag - 1];
        NSLog(@"%s : click url:%@", __func__,recordData.linkUrl);
        UIApplication *application = [UIApplication sharedApplication];
        NSURL *url = [NSURL URLWithString:recordData.linkUrl];

        //入稿URLが計測URLの場合Safari起動
        if (recordData.isMeasuring) {
            for (UIButton *iconView in self.subviews) {
                iconView.enabled = YES;
            }
            if ([application canOpenURL:url]) {
                [application openURL:url];
            } else {
                NSLog(@"openUrl error.");
            }
        } else {
            if (NSClassFromString(@"SKStoreProductViewController")) { //ios6のバージョンの処理
                NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
                [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *responseError) {

                    if (responseError) {
                        // エラー処理を行う。
                        if (responseError.code == -1003) {
                            NSLog(@"%s : not found hostname. targetURL=%@", __func__,url);
                        } else if (responseError.code == -1019) {
                            NSLog(@"%s : auth error. reason=%@", __func__, responseError);
                        } else {
                            NSLog(@"%s : unknown error occurred. reason = %@", __func__, responseError);
                        }

                    } else {
                        int httpStatusCode = ((NSHTTPURLResponse *) response).statusCode;
                        if (httpStatusCode == 404) {
                            NSLog(@"%s : 404 NOT FOUND ERROR. targetURL=%@", __func__, url);
                        } else {
                            NSLog(@"%s : success request", __func__);
                            NSDictionary *productParameters = @{SKStoreProductParameterITunesItemIdentifier : recordData.appStoreId};
                            SKStoreProductViewController *storeviewController = [[SKStoreProductViewController alloc] init];
                            storeviewController.delegate = self;
                            [storeviewController loadProductWithParameters:productParameters completionBlock:^(BOOL result, NSError *error) {
                                if (result) {
                                    [self.rootViewController presentViewController:storeviewController animated:YES completion:nil];
                                }
                            }];
                        }
                    }
                }];

            } else { //ios6以前のバージョンの処理
                for (UIButton *iconView in self.subviews) {
                    iconView.enabled = YES;
                }
                if ([application canOpenURL:url]) {
                    [application openURL:url];
                } else {
                    NSLog(@"%s : openUrl error.", __func__);
                }
            }
        }
    }
}

- (void)setShowAppName:(BOOL)show {
    if (!show) {
        chkShowAppName_ = NO;
    } else {
        chkShowAppName_ = YES;
    }
}

- (void)setShowIconCount:(NSInteger)count {
    iconCount_ = count;
}

#pragma mark -
#pragma mark SKStoreProductViewControllerDelegate
- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController {
    for (UIButton *iconView in self.subviews) {
        iconView.enabled = YES;
    }
    [self.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)didMoveToSuperview{
     if (!self.superview) {
         self.rootViewController = nil;
     }
}

- (void)dealloc {
    NSLog(@"%s", __func__);
    self.delegate = nil;
    self.rootViewController = nil;
    [self.chkController clearDelegate];
    self.chkController = nil;
    self.indicator = nil;
    self.chkImpressionDic = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end