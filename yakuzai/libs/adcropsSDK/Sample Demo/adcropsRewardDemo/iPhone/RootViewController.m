//
//  RootViewController.m
//  adcropsDemo
//
//  Created by 8crops Inc. on 11/06/22.
//  Copyright 2011 8crops Inc. All rights reserved.
//

#import "RootViewController.h"
#import "ChkViewController.h"
#import "ChkConfig.h"

@interface RootViewController ()
@property (retain, nonatomic) ChkViewController   *chkView;
@property (retain, nonatomic) ChkConfig           *chkConfig;
@end


@implementation RootViewController

- (id)init
{
    if (self = [super init]) {
        // TODO:設定オブジェクト(sadや、suidなど動的に変化する値を付与する場合はChkConfigオブジェクトを生成して下さい。)
        
        self.chkConfig = [[ChkConfig alloc] init];
        self.chkConfig.sad           = @"sad";
        self.chkConfig.suid          = @"suid";
    }
    return self;
}

-(void) btnDidPush {

    // TODO:カスタマイズ値を使う場合のイニシャライズ方法(sadや、suidなど動的に変化する値を付与する場合)
    // ChkViewController *chkView = [[[ChkViewController alloc] initWith8chk:chkConfig] autorelease];
    
    // TODO:通常のイニシャライズ方法
    self.chkView = [[[ChkViewController alloc] init] autorelease];
    if( self.chkView != nil ) {
        [self.chkView show];
    }
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	self.view.backgroundColor = [UIColor whiteColor];
	
	//ボタン作成
	UIButton *rewardButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	[rewardButton setTitle:@"コンテンツ" forState:UIControlStateNormal];
	[rewardButton sizeToFit];
	rewardButton.center = self.view.center;
	[rewardButton addTarget:self action:@selector(btnDidPush) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:rewardButton];
	
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    // TODO:設定オブジェクトを使用する場合はリリースして下さい。
    self.chkConfig = nil;
    self.chkView = nil;
    [super dealloc];
}


@end
