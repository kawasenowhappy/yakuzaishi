//
// Created by Tetsuya Kuramochi
//


#import "DemoViewController.h"
#import "DemoTableViewController.h"


@implementation DemoViewController {
	UIButton *openBtn_;
}


- (void)viewDidLoad {
	[super viewDidLoad];

//	[self.navigationController setNavigationBarHidden:YES animated:NO];

	openBtn_ = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	openBtn_.frame = CGRectMake(100, 100, 100, 40);
	[openBtn_ addTarget:self action:@selector(tappedBtn:) forControlEvents:UIControlEventTouchUpInside];
	[openBtn_ setTitle:@"デモ開始" forState:UIControlStateNormal];

	[self.view addSubview:openBtn_];


}

-(void)tappedBtn:(id)sender
{

	DemoTableViewController *vc = [[DemoTableViewController alloc] initWithStyle:UITableViewStylePlain];
	[self.navigationController pushViewController:vc animated:YES];

}




- (void)dealloc {
	NSLog(@"%s", __func__);
}

@end