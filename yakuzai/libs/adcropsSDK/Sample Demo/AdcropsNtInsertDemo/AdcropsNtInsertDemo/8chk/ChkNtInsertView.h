//
//  ChkNtInsertView.h
//  AdcropsNtInsertDemo
//
//  Created by sazawayuki on 2014/03/24.
//  Copyright (c) 2014年 8crops inc. All rights reserved.
//

#import <UIKit/UIKit.h>

// サイズ
extern const CGSize kChkNtInsertViewSize;

@class ChkNtInsertView;

@protocol ChkNtInsertViewDelegate <NSObject>
// 初回ロードが成功した際に通知(在庫なしの場合は呼ばれません)
- (void)chkBannerViewDidFinishLoad:(ChkNtInsertView *)ntInsertView;
// 2回目以降の受信が成功した際に通知(在庫なしの場合は呼ばれません)
- (void)chkBannerViewDidReceiveAd:(ChkNtInsertView *)ntInsertView;
// 受信が失敗した際に通知(在庫なしの場合はエラーが返されます)
- (void)chkBannerViewDidFailToReceiveAd:(ChkNtInsertView *)ntInsertView;
@end

@interface ChkNtInsertView : UIView

@property (nonatomic, weak) id <ChkNtInsertViewDelegate> delegate;

- (id)initWithFrame:(CGFloat)x rootViewController:(UIViewController*)rootViewController;
// 広告のロード
- (void)load;

@end
