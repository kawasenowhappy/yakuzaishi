//
//  ViewController.m
//  AdcropsListing
//
//  Created by sazawayuki on 2014/03/03.
//  Copyright (c) 2014年 8crops inc. All rights reserved.
//

#import "ViewController.h"
#import "ChkAppListViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIButton *showBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    showBtn.frame = CGRectMake(0, 0, 150, 50);
    showBtn.center = self.view.center;
    [showBtn setTitle:@"オススメアプリ表示" forState:UIControlStateNormal];
    [showBtn addTarget:self action:@selector(showAppList) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:showBtn];
    
}

-(void)showAppList{
    ChkAppListViewController *chkAppViewController = [[ChkAppListViewController alloc] init];
    [chkAppViewController setAppNameTextColor:ChkTextColorDefault];
    [chkAppViewController setAppNameTextFont:ChkTextFontTypeDefault];
    [chkAppViewController setAppDescriptionTextColor:ChkTextColorDefault];
    [chkAppViewController setAppDescriptionTextFont:ChkTextFontType4];
    [self presentViewController:chkAppViewController animated:YES completion:nil];
}

@end
