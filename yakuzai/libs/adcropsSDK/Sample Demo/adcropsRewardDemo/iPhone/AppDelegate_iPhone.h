//
//  AppDelegate_iPhone.h
//  adcropsDemo
//
//  Created by 8crops Inc. on 11/06/14.
//  Copyright 2011 8crops Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"

@interface AppDelegate_iPhone : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    UIBackgroundTaskIdentifier backgroundTask;
}

@property (nonatomic, retain) UIWindow *window;
@property (nonatomic, retain) RootViewController *rootView;

@end

