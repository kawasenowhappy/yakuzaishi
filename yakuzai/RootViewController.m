//
//  RootViewController.m
//  yakuzai
//
//  Created by kawase yu on 2014/07/05.
//  Copyright (c) 2014年 nowhappy. All rights reserved.
//

#import "RootViewController.h"

@interface RootViewController (){
    UIView* contentView;
    UIImageView* splashView;
}

@end

@implementation RootViewController

-(id) init{
    self = [super init];
    if(self){
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    UIView* view = self.view;
    
    UIImageView* bgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg"]];
    [view addSubview:bgView];
    
    splashView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"splashOver"]];
    [view addSubview:splashView];
    
    contentView = [[UIView alloc] initWithFrame:view.frame];
    contentView.transform = CGAffineTransformMakeTranslation(0, [VknDeviceUtil windowHeight]);
    [view addSubview:contentView];
}

-(UIView*)contentView{
    return contentView;
}

-(void) startAnimation{
    [UIView animateWithDuration:0.4f animations:^{
        splashView.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.4f animations:^{
            contentView.transform = CGAffineTransformMakeTranslation(0, 0);
        } completion:^(BOOL finished) {
            
        }];
    }];
}

@end
