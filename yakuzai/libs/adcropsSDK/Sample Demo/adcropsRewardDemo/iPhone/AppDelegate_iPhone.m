//
//  AppDelegate_iPhone.m
//  adcropsDemo
//
//  Created by 8crops Inc. on 11/06/14.
//  Copyright 2011 8crops Inc. All rights reserved.
//

#import "AppDelegate_iPhone.h"
#import "ChkApplicationOptional.h"

@implementation AppDelegate_iPhone

@synthesize window;
@synthesize rootView;

#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
	window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

	self.rootView = [[RootViewController alloc] init];
//	[self.window addSubview:self.rootView.view];
    self.window.rootViewController = self.rootView;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // TODO:アプリがバックグランド実行へ遷移した際にコンバージョン定期送信を実行します。
    [ChkApplicationOptional applicationDidEnterBackground:application];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // TODO:アプリがフォアグランド実行へ遷移した際にコンバージョン定期送信を停止します。
    [ChkApplicationOptional applicationWillEnterForeground];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}


- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     NSLog(@"applicationDidBecomeActive");
     */
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}


- (void)dealloc {
    self.rootView = nil;
    [window release];
    [super dealloc];
}


@end
