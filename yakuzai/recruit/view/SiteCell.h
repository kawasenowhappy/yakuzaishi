//
//  SiteCell.h
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Site.h"

@protocol SiteCellDelegate <NSObject>


@end

@interface SiteCell : UITableViewCell

@property NSObject<SiteCellDelegate>* delegate;

-(void) reload:(Site*)site_;
-(void) light;

@end
