//
//  NewsListViewController.m
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "NewsListViewController.h"
#import "NewsList.h"
#import "NewsCell.h"

@interface NewsListViewController ()<
UITableViewDataSource
, UITableViewDelegate
>{
    NSObject<NewsListViewControllerDelegate>* delegate;
    UITableView* tableView_;
    NewsList* newsList;
    AdstirMraidView* adView;
}

@end

@implementation NewsListViewController

-(id) initWithDelegate:(NSObject<NewsListViewControllerDelegate> *)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        newsList = [YakuUtil newsList];
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    UIView* view = self.view;
    
    UIView* line = [[UIView alloc] initWithFrame:CGRectMake(10, 85.5f, 280, 2.5f)];
    line.backgroundColor = UIColorFromHex(0xd95d92);
    [view addSubview:line];
    
    float height = 388.0f;
    if([VknDeviceUtil is35Inch]){
        height -= 35;
    }
    CGRect frame = CGRectMake(10, 88.0f, 285, height);
    tableView_ = [[UITableView alloc] initWithFrame:frame];
    tableView_.delegate = self;
    tableView_.dataSource = self;
    tableView_.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView_.backgroundColor = [UIColor clearColor];
    [view addSubview:tableView_];
    
    // ad
    UIView* adViewWrapper = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 280, 100)];
    float scale = 0.877f;
    adView = [[AdstirMraidView alloc] initWithAdSize:kAdstirAdSize320x100 media:ADSTIR_MEDIA_ID spot:ADSTIR_NEWS_BOTTOM_SPOT_ID];
    adView.frame = CGRectMake(-20, 0, kAdstirAdSize320x100.size.width, kAdstirAdSize320x100.size.height);
//    adView.backgroundColor = UIColorFromHex(0xcccccc);
    adView.transform = CGAffineTransformMakeScale(scale, scale);
    [adViewWrapper addSubview:adView];
    tableView_.tableFooterView = adViewWrapper;
    [adView start];
}

-(void) tapBack{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- UITableViewDataSource --

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [newsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellIdentifier = @"Cell";
    NewsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[NewsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    News* news = [newsList get:indexPath.row];
    [cell reload:news];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 82.5f;
}

#pragma mark -- UITableViewDelegate --

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    News* news = [newsList get:indexPath.row];
    [delegate onSelect:news];
    
    NewsCell* newsCell = (NewsCell*)[tableView_ cellForRowAtIndexPath:indexPath];
    [newsCell light];
    
    [VknUtils wait:0.3f callback:^{
        [tableView reloadData];
    }];
}

@end
