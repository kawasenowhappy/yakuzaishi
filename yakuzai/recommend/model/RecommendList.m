//
//  RecommendList.m
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "RecommendList.h"

@implementation RecommendList

+(RecommendList*) createModelList{
    RecommendList* list = [[RecommendList alloc] init];
    
    for(int i=0;i<10;i++){
        Recommend* recommend = [[Recommend alloc] init];
        recommend.title = @"Belle";
        recommend.linkUrl = @"https://itunes.apple.com/de/app/kosume-meikuappu-sukinkeanotoraiarukosumeyasanpurukosumenokataroguapuri/id777166701?mt=8";
        recommend.excerpt = @"いつまでも、若々しい肌でい続けたい。久しぶりに会った友人達に、お肌キレイねと言われたい。初めて会った方から、10歳若く見られたい。";
        recommend.iconUrl = @"http://a5.mzstatic.com/us/r30/Purple6/v4/3e/6a/4d/3e6a4d35-b390-d465-f719-a1c86eef2b8d/mzl.pkklqifu.175x175-75.jpg";
        [list add:recommend];
    }
    
    return list;
}

-(Recommend*)get:(int)index{
    return (Recommend*)[super get:index];
}

-(void) reloadWithJson:(NSArray*)jsonArray{
    int index = 0;
    for(NSDictionary* row in jsonArray){
        Recommend* recommend = [[Recommend alloc] initWithJson:row];
        [self replace:recommend at:index];
        index++;
        
        
        if([YakuUtil isReadedRecommend:recommend.id_]){
            [recommend readed];
        }
    }
    
    [self removeToTail:index];
}

-(void) readedSort{
    
    RecommendList* readedList = [[RecommendList alloc] init];
    for(int i=0;i<[self count];i++){
        Recommend* recommend = [self get:i];
        
        if(recommend.isReaded){
            [items removeObject:recommend];
            [readedList add:recommend];
            i--;
        }
    }
    
    for(int i=0,max=[readedList count];i<max;i++){
        [self add:[readedList get:i]];
    }
}

@end
