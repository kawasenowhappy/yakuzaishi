//
//  ChkSlideIconView.m
//  8chk
//
//  Created by Sazawa Yuki on 02/06/14.
//  Copyright (c) 2014年 8crops inc. All rights reserved.
//

#import "ChkISAutoView.h"
#import <QuartzCore/QuartzCore.h>
#import "ChkController.h"
#import "ChkControllerDelegate.h"
#import <dispatch/dispatch.h>
#import <StoreKit/StoreKit.h>
#import "ChkControllerNetworkNotifyDelegate.h"

@interface ChkISAutoView () <ChkControllerDelegate,UIScrollViewDelegate,SKStoreProductViewControllerDelegate, ChkControllerNetworkNotifyDelegate>{
    dispatch_queue_t main_queue_;
    dispatch_queue_t image_queue_;
    BOOL chkLoadSuccessfulFlag_;// 初回ロード成功フラグ
    BOOL chkShowAppName_;// アプリ名表示フラグ
}
@property (nonatomic, strong) ChkController    *chkController;
// モーダルビューを表示元のビューコントローラを指定
@property (nonatomic, weak) UIViewController *rootViewController;
// 画像のスクロールビュー
@property (nonatomic, strong) UIScrollView              *chkScrollView;
// スクロールビューの背景画像
@property (nonatomic, strong) UIImageView               *chkScrollBackgroundImage;
// スクロールビューのインジケータ
@property (nonatomic, strong) UIActivityIndicatorView   *chkIndicator;
@property (nonatomic, strong) UIView                    *chkLoadingView;
// インプレッション送信カウント用
@property (nonatomic, strong) NSMutableDictionary       *chkImpressionDic;
@property (nonatomic,strong)UIColor        *chkISAutoTextColor;
@property (nonatomic,strong)UIColor        *chkISAutoBackgroundColor;
@property (nonatomic)CGFloat                chkISAutoBackgroundAlpha;

@end

@implementation ChkISAutoView

// CPCサイズ
const CGSize kChkISAutoViewCPCSize = {320, 50};

#pragma mark -
#pragma mark Private Method
-(void) sendImpression:(int)listNumber {
        int dataCount = [[self.chkController dataList] count];
        if( dataCount <= listNumber ) {
            return;
        }
        // 1ページに表示される分だけインプレッション送信する
        int max = listNumber - [self getOnePageIconCount];
        if(max<0) max = 0;
        for (int i = listNumber; i >= max; i--) {
            ChkRecordData *data = [[self.chkController dataList] objectAtIndex:i];
            
            NSString *isSend = [self.chkImpressionDic objectForKey:data.appStoreId];
            //NSLog(@"***Value:%@",isSend);
            if(![isSend isEqualToString:@"1"]) {
                // インプレッション送信
                NSLog(@"sendImpression:%d:%@:%@",i,data.title,data.appStoreId);
                [self.chkController sendImpression:data];
                [self.chkImpressionDic setObject:@"1" forKey:data.appStoreId];
            }else{
                //NSLog(@"****Impression Sended:%d:%@",i,data.title);
            }
        }
        //NSLog(@"self.chkImpressionDic:%@",self.chkImpressionDic);
}


- (id)initWithCpcSize:(CGFloat)x rootViewController:(UIViewController*)rootViewController {
    return [self initWithFrame:x rootViewController:rootViewController];
}

- (id)initWithFrame:(CGFloat)x rootViewController:(UIViewController*)rootViewController{
    CGRect frame = CGRectMake(0,
                       x,
                       kChkISAutoViewCPCSize.width,
                       kChkISAutoViewCPCSize.height);
    
    if (self = [super initWithFrame:frame]) {
        
        self.rootViewController = rootViewController;
        self.chkImpressionDic = [[NSMutableDictionary alloc] init];
        main_queue_ = dispatch_get_main_queue();
		image_queue_ = dispatch_queue_create("net.adcrops.8chk.list-image", NULL);
        chkLoadSuccessfulFlag_ = NO;
        chkShowAppName_ = NO;
        
        NSLog(@"%s : initWithFrame", __func__);
        self.chkController = [[ChkController alloc]initWithDelegate:self];
        self.backgroundColor = [UIColor clearColor];
        
        // 画像のスクロールビュー
        self.chkScrollView = [[UIScrollView alloc] init];
        self.chkScrollView.frame = CGRectMake(0.0f,
                                              -2.0f,
                                              self.frame.size.width,
                                              self.frame.size.height + 4.5f);
        self.chkScrollView.contentSize = CGSizeMake( self.frame.size.width + 1.0f, self.frame.size.height);
        
        // スクロールビュー背景画像
        UIImage *iconTmp = [UIImage imageNamed:@"background"];
        CGSize size = CGSizeMake(self.frame.size.width, self.frame.size.height);
        if (UIGraphicsBeginImageContextWithOptions != NULL) {
            // Retinaディスプレイ対応
            UIGraphicsBeginImageContextWithOptions(size, NO, [[UIScreen mainScreen] scale]);
        } else {
            // iOS4より古いバージョンの場合
            UIGraphicsBeginImageContext(size);
        }
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
        [iconTmp drawInRect:CGRectMake(0, 0, size.width, size.height)];
        UIImage *icon = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        self.chkScrollBackgroundImage = [[UIImageView alloc] initWithImage:icon];
        self.chkScrollBackgroundImage.frame = self.chkScrollView.frame;
        [self addSubview:self.chkScrollBackgroundImage];
        
        // スクロールビュー
        self.chkScrollView.pagingEnabled = YES; // ページ単位
        self.chkScrollView.backgroundColor = [UIColor clearColor];
        self.chkScrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
        [self.chkScrollView setCanCancelContentTouches:NO];
        self.chkScrollView.showsVerticalScrollIndicator = NO;
        self.chkScrollView.showsHorizontalScrollIndicator = NO;
        self.chkScrollView.scrollsToTop = NO;
        self.chkScrollView.clipsToBounds = YES;
        self.chkScrollView.scrollEnabled = YES;
        self.chkScrollView.delegate = self;
		[self addSubview:self.chkScrollView];
        
        self.chkLoadingView = [[UIView alloc] init];
        self.chkLoadingView.frame = CGRectMake(0, 0, kChkISAutoViewCPCSize.width, kChkISAutoViewCPCSize.height);
        self.chkLoadingView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        [self addSubview:self.chkLoadingView];
        
        // スクロールビューのインジケータ
        if ([[UIActivityIndicatorView class] respondsToSelector:@selector(appearance)]) {
            // iOS 5.0以上の処理
            self.chkIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            [self.chkIndicator setColor:[UIColor grayColor]]; // インジケータの色
        }else{
                // iOS 5.0未満(iOS 4.3以下)の処理
            self.chkIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        }
        
        self.chkIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin
        | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [self.chkIndicator setCenter:CGPointMake(self.center.x,self.frame.size.height/2 + 2.0f)];
        [self addSubview:self.chkIndicator];
        [self.chkIndicator startAnimating];

        __weak __block ChkISAutoView *blockSelf = self;
        [[NSNotificationCenter defaultCenter]
         addObserverForName:UIApplicationDidEnterBackgroundNotification
         object:nil queue:nil
         usingBlock:^(NSNotification *note) {
             //BG状態へ
             [blockSelf applicationDidEnterBackground];
         }];
        
        [[NSNotificationCenter defaultCenter]
         addObserverForName:UIApplicationWillEnterForegroundNotification
         object:nil queue:nil
         usingBlock:^(NSNotification *note) {
             // FG状態へ
             [blockSelf applicationWillEnterForeground];
         }];
    }
    return self;
}


-(CGFloat) getIconSize {
    return 40.0f;
}

-(int) getOnePageIconCount {
    return 2;
}

- (void)setShowAppName:(UIColor*)textColor backgroundColor:(UIColor *)backgroundColor backgroundAlpha:(CGFloat)backgroundAlpha{
    chkShowAppName_ = YES;
    self.chkISAutoTextColor = textColor;
    self.chkISAutoBackgroundColor = backgroundColor;
    self.chkISAutoBackgroundAlpha = backgroundAlpha;
}

- (void)load {
    NSLog(@"load");
    for(id subview in self.chkScrollView.subviews) {
        if([subview tag] <= [[self.chkController dataList] count] ) {
            [subview removeFromSuperview];
            NSLog(@"%s : remove subview:%ld", __func__, (long)[subview tag]);
        }
    }
    // スクロール位置を初期化
    [self.chkScrollView setContentOffset:CGPointMake(0.0f, 0.0f)];
    [self.chkIndicator startAnimating];
    self.chkLoadingView.hidden = NO;
    self.chkImpressionDic = [[NSMutableDictionary alloc] init];
    [self.chkIndicator startAnimating];
    [self.chkController resetDataList];
    [self.chkController requestDataList];
}

- (void) setIndicatorColor:(UIColor*)color {
    if ([[UIActivityIndicatorView class] respondsToSelector:@selector(appearance)]) {
        [self.chkIndicator setColor:color];
    }
}

- (void) applicationDidEnterBackground {
    NSLog(@"applicationDidEnterBackground");
    // subviewがあったら消す。
    for(id subview in self.chkScrollView.subviews) {
        if([subview tag] <= [[self.chkController dataList] count] ) {
            [subview removeFromSuperview];
            NSLog(@"%s : remove subview:%ld", __func__,(long)[subview tag]);
        }
    }
    [ChkISAutoView cancelPreviousPerformRequestsWithTarget:self selector:@selector(autoScroll) object:nil];
}

- (void) applicationWillEnterForeground {
    NSLog(@"applicationWillEnterForeground");
    [self load];
}

#pragma mark -
#pragma mark ChkControllerDelegate delegate
- (void)chkControllerDataListWithSuccess:(NSDictionary *)data{
    NSLog(@"chkControllerDataListWithSuccess:%@",data);
    
    // 最後まで全部取りに行く。
    if( [self.chkController hasNextData] ) {
        NSLog(@"****** retry request *****");
        [self.chkController requestDataList];
        return;
    }
    [self.chkIndicator stopAnimating];
    self.chkLoadingView.hidden = YES;
    
    // アイコンを設定する。
    float scrollWidth = 0.0f;
    int i = 0;
    int oddNumber = 0;
    int evenNumber = 0;
    UIButton *iconBackView;
    for (ChkRecordData* chkData in [self.chkController dataList]) {
        
        iconBackView = [UIButton buttonWithType:UIButtonTypeCustom];
        iconBackView.tag = i;
        [iconBackView addTarget:self action:@selector(chkIconTap:) forControlEvents:UIControlEventTouchUpInside];
        [iconBackView addTarget:self action:@selector(chkIconTouchDown:) forControlEvents:UIControlEventTouchDown];
        [iconBackView addTarget:self action:@selector(chkIconTouchDragExit:) forControlEvents:UIControlEventTouchDragExit];
        int margin=0;
        if (i==0) {
            margin = 10;
        } else if (i==1){
            margin = 20;
        } else {
            if (i%2==0) {
                evenNumber++;
                margin = (20*(i/2))+20+(oddNumber*10);
            } else {
                oddNumber++;
                margin = ((evenNumber*20)+(oddNumber*10))+20;
            }
        }
        
        iconBackView.frame = CGRectMake(margin+i*145, 7, 145, [self getIconSize]);
        iconBackView.selected = YES;
        [iconBackView setImage:[UIImage imageNamed:@"bannerbox"] forState:UIControlStateNormal];
        [self.chkScrollView addSubview:iconBackView];
        
        UIImageView *chkIconImage = [[UIImageView alloc] init];
        chkIconImage.backgroundColor = [UIColor clearColor];
        chkIconImage.tag = i;
        chkIconImage.frame = CGRectMake(0.0f,
                                        0,
                                        [self getIconSize],
                                        [self getIconSize]);
        chkIconImage.layer.masksToBounds = YES;
        chkIconImage.layer.cornerRadius = 6.0f;
        
        // アプリアイコン
        if (![chkData cacheImage]) {
            NSString *url = chkData.imageIcon;
            dispatch_async(image_queue_, ^{
                UIImage *icon = [self.chkController getImage:url];
                // 画像イメージをキャッシュする。
                [chkData setCacheImage:icon];
                dispatch_async(main_queue_, ^{
                    chkIconImage.image = icon;
                });
            });
        } else {
            chkIconImage.image = [chkData cacheImage];
        }
        
        if(chkShowAppName_) {
            // アプリ名ラベルを付ける。
            UILabel *appNameLabel = [[UILabel alloc] init];
            appNameLabel.font = [UIFont fontWithName:@"HiraKakuProN-W6" size:9.0];
            appNameLabel.backgroundColor = self.chkISAutoBackgroundColor;
            appNameLabel.textColor = self.chkISAutoTextColor;
            appNameLabel.numberOfLines = 3;
            appNameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            appNameLabel.highlightedTextColor = [UIColor grayColor
                                                 ];
            appNameLabel.frame = CGRectMake([self getIconSize]+5,
                                            0.0f,
                                            145-[self getIconSize]-10,
                                            [self getIconSize]);
            appNameLabel.alpha = self.chkISAutoBackgroundAlpha;
            appNameLabel.text = chkData.description;
            [appNameLabel setAdjustsFontSizeToFitWidth:NO];
            [appNameLabel setTextAlignment:NSTextAlignmentCenter];
            [iconBackView addSubview:appNameLabel];
        }
        
        // インプレッション送信する。1ページ目に表示される物は表示時にインプレッション送信する。
        if(i < [self getOnePageIconCount]) {
            [self sendImpression:i];
        }
        
        [iconBackView addSubview:chkIconImage];
        i++;
    }
    scrollWidth += iconBackView.frame.size.width + iconBackView.frame.origin.x;
    if([[self.chkController dataList] count] % 2 == 0) {
        self.chkScrollView.contentSize = CGSizeMake( scrollWidth+10, self.frame.size.height);
    }else{
        self.chkScrollView.contentSize = CGSizeMake( scrollWidth+165, self.frame.size.height);
    }
    
    if(!chkLoadSuccessfulFlag_ && [[self.chkController dataList] count] > 0) {
        // 初回の受信成功時
        [self setHidden:NO];
        chkLoadSuccessfulFlag_ = YES;
        if ([self.delegate respondsToSelector:@selector(chkBannerViewDidFinishLoad:)]) {
            [self.delegate chkBannerViewDidFinishLoad:self];
        }
    }else{
        if([[self.chkController dataList] count] == 0 ) {
            // 案件無しはエラーとする。
            [self setHidden:YES];
            if ([self.delegate respondsToSelector:@selector(chkBannerViewDidFailToReceiveAd:)]) {
                [self.delegate chkBannerViewDidFailToReceiveAd:self];
            }
        }else{
            // 2回目以降の受信成功時
            [self setHidden:NO];
            if ([self.delegate respondsToSelector:@selector(chkBannerViewDidReceiveAd:)]) {
                [self.delegate chkBannerViewDidReceiveAd:self];
            }
        }
    }
    [self performSelector:@selector(autoScroll) withObject:nil afterDelay:30.0f];
}

- (void)autoScroll{
    [self performSelector:@selector(autoScroll) withObject:nil afterDelay:30.0f];

    CGPoint pt = self.chkScrollView.contentOffset;
    pt.x += self.chkScrollView.frame.size.width;
    if (!(pt.x < self.chkScrollView.contentSize.width)) {
        pt.x = 0;
    }
    [self.chkScrollView setContentOffset:pt animated:YES];
}

- (void)chkControllerDataListWithError:(NSError*)error{
    NSLog(@"chkControllerDataListWithError");
    [self.chkIndicator stopAnimating];
    self.chkLoadingView.hidden = YES;
    
    [self setHidden:YES];
    if ([self.delegate respondsToSelector:@selector(chkBannerViewDidFailToReceiveAd:)]) {
        [self.delegate chkBannerViewDidFailToReceiveAd:self];
    }
}

#pragma mark - chkControllerNetworkNotifyDelegate
-(void)chkControllerInitNotReachableStatusError:(NSError *)error{
    //ChkController⽣生成時にネットワークに接続できない場合に呼び出されます。
    [self.chkIndicator stopAnimating];
    self.chkLoadingView.hidden = YES;
}

-(void)chkControllerNetworkNotReachable:(NSError *)error{
    //ネットワークの状況が接続→切切断に変わった時に呼び出されます。
    [self.chkIndicator stopAnimating];
    self.chkLoadingView.hidden = YES;
}

-(void)chkControllerNetworkReachable:(NSUInteger)networkType{
    //ネットワークの状況が切切断→接続に変わった時に呼び出されます。引数には、WiFiにつながった場合1、3Gにつながった場合は2が設定されます。
    [self.chkIndicator startAnimating];
    self.chkLoadingView.hidden = NO;
}

-(void)chkControllerRequestNotReachableStatusError:(NSError *)error{
    //データ取得時にネットワークに接続できない場合に呼び出されます。
    [self.chkIndicator stopAnimating];
    self.chkLoadingView.hidden = YES;
}

#pragma mark -
#pragma mark UIScrollViewDelegate delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    float x = scrollView.contentOffset.x;
    if(x>0) {
        // インプレッション
        float oneSize = 160;
        int iconCount = (x+self.frame.size.width)/oneSize;
        iconCount--;
        NSLog(@"iconCount:%d", iconCount);
        [self sendImpression:iconCount];
    }
}

#pragma mark -
#pragma mark UIButton Action Method
- (void) chkIconTouchDown:(UIButton*) sender {
    [self.chkScrollView setScrollEnabled:NO];
    for(id subview in sender.subviews) {
		if ([subview isKindOfClass:[UILabel class]]){
			((UILabel *)subview).highlighted = YES;
		}else if ([subview isKindOfClass:[UIImageView class]]){
			((UIImageView *)subview).alpha = 0.5f;
		}
	}
}

- (void) chkIconTouchDragExit:(UIButton*) sender {
    [self.chkScrollView setScrollEnabled:YES];
    for(id subview in sender.subviews) {
		if ([subview isKindOfClass:[UILabel class]]){
			((UILabel *)subview).highlighted = NO;
		}else if ([subview isKindOfClass:[UIImageView class]]){
			((UIImageView *)subview).alpha = 1.0f;
		}
	}
}

- (void) chkIconTap:(UIButton*) sender {
    [self.chkScrollView setScrollEnabled:YES];
    for(id subview in sender.subviews) {
		if ([subview isKindOfClass:[UILabel class]]){
			((UILabel *)subview).highlighted = NO;
		}else if ([subview isKindOfClass:[UIImageView class]]){
			((UIImageView *)subview).alpha = 1.0f;
		}
	}

    if([[self.chkController dataList]count] > 0 ) {
        ChkRecordData *recordData = [[ChkRecordData alloc] init];
        recordData = [[self.chkController dataList] objectAtIndex:sender.tag];
        NSLog(@"click url:%@",recordData.linkUrl);
        UIApplication *application = [UIApplication sharedApplication];
        NSURL *url = [NSURL URLWithString:recordData.linkUrl];
        
        //入稿URLが計測URLの場合Safari起動
        if (recordData.isMeasuring) {
            
            if( [application canOpenURL:url]){
                [application openURL:url];
            }else{
                NSLog(@"openUrl error.");
            }
            
        } else {
            
            if(NSClassFromString(@"SKStoreProductViewController")) { //ios6のバージョンの処理
                [self.chkIndicator startAnimating];
                self.chkLoadingView.hidden = NO;
                NSURLRequest  *request = [[NSURLRequest alloc] initWithURL:url];
                [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                    
                    if (error) {
                        // エラー処理を行う。
                        if (error.code == -1003) {
                            NSLog(@"not found hostname. targetURL=%@", url);
                        } else if (error.code == -1019) {
                            NSLog(@"auth error. reason=%@", error);
                        } else {
                            NSLog(@"unknown error occurred. reason = %@", error);
                        }
                        
                    } else {
                        int httpStatusCode = ((NSHTTPURLResponse *)response).statusCode;
                        if (httpStatusCode == 404) {
                            NSLog(@"404 NOT FOUND ERROR. targetURL=%@", url);
                            // } else if (・・・) {
                            // 他にも処理したいHTTPステータスがあれば書く。
                            
                        } else {
                            NSLog(@"success request!!");
                                
                            NSDictionary *productParameters = @{ SKStoreProductParameterITunesItemIdentifier:recordData.appStoreId};
                                
                            SKStoreProductViewController* storeviewController = [[SKStoreProductViewController alloc] init];
                            storeviewController.delegate = self;

                            [storeviewController loadProductWithParameters:productParameters completionBlock:^(BOOL result, NSError *storeRequestError) {
                                if (result) {
                                    [self.chkIndicator stopAnimating];
                                    self.chkLoadingView.hidden = YES;
                                    [self.rootViewController presentViewController:storeviewController animated:YES completion:nil];
                                }
                                
                            }];

                        }
                    }
                }];
                
            } else { //ios6以前のバージョンの処理
                
                if( [application canOpenURL:url]){
                    [application openURL:url];
                }else{
                    NSLog(@"openUrl error.");
                }
            }
        }
    }
}

#pragma mark -
#pragma mark SKStoreProductViewController delegate

- (void)productViewControllerDidFinish:(SKStoreProductViewController*)viewController
{
	[self.rootViewController dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)didMoveToSuperview{
    if (!self.superview) {
        [ChkISAutoView cancelPreviousPerformRequestsWithTarget:self selector:@selector(autoScroll) object:nil];
    }
}

- (void)dealloc {
    NSLog(@"%s", __func__);
    self.delegate = nil;
    self.rootViewController = nil;
    [self.chkController clearDelegate];
    self.chkController = nil;
    self.chkScrollView  = nil;
    self.chkScrollBackgroundImage   = nil;
    self.chkIndicator       = nil;
    self.chkImpressionDic   = nil;
    self.chkISAutoBackgroundColor = nil;
    self.chkISAutoTextColor = nil;
    self.chkLoadingView = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



@end
