#import "ISAlternativeRefreshControl.h"

@class ISScalingActivityIndicatorView;
@class ISGumView;
@class ChkRecordData;
@class ChkController;

@interface AdCropsGumRefreshControl : ISAlternativeRefreshControl

//@property (nonatomic, strong) ChkRecordData *chkRecordData;
//@property (nonatomic, strong) NSMutableArray *chkRecordDatas;
@property (nonatomic, weak) ChkController *chkController;

@end
