//
//  Recommend.m
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "Recommend.h"

@implementation Recommend

@synthesize linkUrl;
@synthesize title;
@synthesize excerpt;
@synthesize iconUrl;
@synthesize isReaded;
@synthesize isNew;

-(id) initWithJson:(NSDictionary*)jsonDic{
    self = [super init];
    if(self){
        id_ = [[jsonDic objectForKey:@"id"] intValue];
        linkUrl = [jsonDic objectForKey:@"link_url"];
        title = [jsonDic objectForKey:@"name"];
        excerpt = [jsonDic objectForKey:@"excerpt"];
        iconUrl = [jsonDic objectForKey:@"image_url"];
        isNew = NO;
    }
    
    return self;
}

-(id) initWithAdcropsdata:(ChkRecordData*)data{
    self = [super init];
    if(self){
        id_ = [[data articleId] intValue];
        linkUrl = [data linkUrl];
        title = [data title];
        excerpt = [data description];
        iconUrl = [data imageIcon];
        isNew = NO;
    }
    
    return self;
}

-(void) readed{
    isReaded = YES;
}

-(void) setNew{
    isNew = YES;
}

-(void) noNew{
    isNew = NO;
}

//@property (nonatomic, retain, readonly) NSString  *articleId;
//@property (nonatomic, retain, readonly) NSString  *appId;
//@property (nonatomic, retain, readonly) NSString  *title;
//@property (nonatomic, retain, readonly) NSString  *description;
//@property (nonatomic, retain, readonly) NSString  *detail;
//@property (nonatomic, retain, readonly) NSString  *cvCondition;
//@property (nonatomic, retain, readonly) NSString  *linkUrl;
//@property (nonatomic, retain, readonly) NSString  *type;
//@property (nonatomic, retain, readonly) NSString  *imageIcon;
//@property (nonatomic, retain, readonly) NSString  *appSearcher;
//@property (nonatomic, retain, readonly) NSNumber  *price;
//@property (nonatomic, retain, readonly) NSNumber  *oldPrice;
//@property (nonatomic, retain, readonly) NSNumber  *reward;
//@property (nonatomic, retain, readonly) NSNumber  *point;
//@property (nonatomic, retain, readonly) NSNumber  *categoryId;
//@property (nonatomic, retain, readonly) NSString  *category;
//@property (nonatomic, retain, readonly) NSString  *impUrl;
//@property (nonatomic, retain, readonly) NSString  *appStoreId;
//
//@property (nonatomic, readonly)         BOOL      isInstalled;
//@property (nonatomic, readonly)         BOOL      isImpSend;
//
//@property (nonatomic, retain)           UIImage   *cacheImage;
//@property (nonatomic, retain)           UIImage   *cashImage UNAVAILABLE_ATTRIBUTE __attribute__((unavailable("This property is unavailable!")));
//@property (nonatomic, retain, readonly) NSNumber  *conversionActionType;
//@property (nonatomic, readonly) BOOL              isMeasuring;

@end
