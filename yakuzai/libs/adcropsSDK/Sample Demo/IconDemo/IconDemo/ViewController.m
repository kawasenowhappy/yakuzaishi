//
//  ViewController.m
//  IconDemo
//
//  Created by sazawayuki on 2014/02/14.
//  Copyright (c) 2014年 8crops inc. All rights reserved.
//

#import "ViewController.h"
#import "ChkIconView.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    ChkIconView *chkIconView= [[ChkIconView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 75, self.view.frame.size.width, 75) rootViewController:self iconCount:4 iconSize:kChkAppIconSize];
    [chkIconView setShowAppName:YES];
    [self.view addSubview:chkIconView];
    [chkIconView load];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
