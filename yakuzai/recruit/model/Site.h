//
//  Site.h
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "VknModel.h"

@interface Site : VknModel

@property NSString* title;
@property NSString* subtitle;
@property NSString* excerpt;
@property NSString* content;
@property NSString* linkUrl;
@property NSString* iconUrl;
@property (readonly) BOOL isReaded;

-(id) initWithJson:(NSDictionary*)json;
-(NSString*)listImageUrl;
-(NSString*)contentImageUrl;
-(void) readed;

@end
