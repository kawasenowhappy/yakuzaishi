//
//  SiteCell.m
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "SiteCell.h"

@interface SiteCell (){
    Site* site;
    UILabel* titleLabel;
    UILabel* excerptLabel;
    UIImageView* icon;
    UIImageView* arrow;
    UIView* overView;
}

@end

@implementation SiteCell

@synthesize delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initialize];
    }
    return self;
}

-(void) initialize{
    self.backgroundColor = [UIColor clearColor];
    UIView* view = self.contentView;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    overView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 82)];
    overView.backgroundColor = UIColorFromHex(0x7fecff);
    overView.alpha = 0;
    [view addSubview:overView];
    
    UIView* wrapView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 82.5f)];
    [view addSubview:wrapView];
    
    icon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, 60, 60)];
    icon.contentMode = UIViewContentModeScaleAspectFill;
    icon.layer.cornerRadius = 10;
    icon.layer.borderWidth = 2;
    icon.clipsToBounds = YES;
    [wrapView addSubview:icon];
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(65, 10, 200, 17)];
    titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [wrapView addSubview:titleLabel];
    
    excerptLabel = [[UILabel alloc] initWithFrame:CGRectMake(65, 34, 200, 37)];
    excerptLabel.font = [UIFont systemFontOfSize:10.0f];
    excerptLabel.numberOfLines = 0;
    [wrapView addSubview:excerptLabel];
    
    UIImageView* line  = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lineBlue"]];
    line.frame = CGRectMake(0, 80.5f, 280, 2);
    [wrapView addSubview:line];
    
    arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrowBlue"]];
    arrow.frame = CGRectMake(280-13, 30, 13, 21.5f);
    [wrapView addSubview:arrow];
}

-(void) reload:(Site *)site_{
    site = site_;
    icon.image = nil;
    
    titleLabel.text = site.title;
    excerptLabel.text = site.excerpt;
    
    [VknImageCache loadImage:[site listImageUrl] defaultImage:nil callback:^(UIImage *image, NSString *key, BOOL useCache) {
        if(![key isEqualToString:site.listImageUrl]) return;
        [VknThreadUtil mainBlock:^{
            icon.image = image;
        }];
    }];
    
    if(site.isReaded){
        arrow.image = [UIImage imageNamed:@"readedArrowBlue"];
        titleLabel.textColor = UIColorFromHex(0x1caac4);
        excerptLabel.textColor = UIColorFromHex(0x1caac4);
        icon.layer.borderColor = UIColorFromHex(0x278fa2).CGColor;
        return;
    }
    
    arrow.image = [UIImage imageNamed:@"arrowBlue"];
    titleLabel.textColor = UIColorFromHex(0x0d5663);
    excerptLabel.textColor = UIColorFromHex(0x0d5663);
    icon.layer.borderColor = UIColorFromHex(0x278fa2).CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) light{
    overView.alpha = 0.7f;
    SEL sel = @selector(updateLight:);
    [Tween addTween:self tweenId:99 startValue:0.7 endValue:0.0 time:0.3 delay:0 easing:@"easeInCirc" startSEL:sel updateSEL:sel endSEL:sel];
}

-(void) updateLight:(TweenObject*)tween{
    double val = tween.currentValue;
    overView.alpha = val;
}



@end
