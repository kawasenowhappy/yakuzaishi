//
//  NewsList.h
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "VknModelList.h"
#import "News.h"

@interface NewsList : VknModelList

-(News*) get:(int)index;
-(void) reloadWithJson:(NSArray*)jsonArray;

@end
