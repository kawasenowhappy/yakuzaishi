//
//  News.m
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "News.h"

@implementation News

@synthesize title;
@synthesize linkUrl;
@synthesize excerpt;
@synthesize date;
@synthesize isReaded;

-(NSString*)dateStr{
    return [VknDateUtil dateToString:date format:@"yyyy-MM-dd"];
}

-(id) initWithJson:(NSDictionary*)jsonDic{
    self = [super init];
    if(self){
        id_ = [[jsonDic objectForKey:@"id"] intValue];
        title = [jsonDic objectForKey:@"title"];
        linkUrl = [jsonDic objectForKey:@"link"];
        excerpt = [jsonDic objectForKey:@"content"];
        date = [NSDate dateWithTimeIntervalSince1970:[[jsonDic objectForKey:@"published_at"] intValue]];
    }
        
    return self;
}

-(void) readed{
    isReaded = YES;
}

@end
