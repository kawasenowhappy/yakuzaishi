//
//  ChkViewController.h
//  8chk
//
//  Created by 8crops Inc. on 11/06/20.
//  Copyright 2011 8crops Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChkConfig.h"
#import "ChkControllerDelegate.h"
#import "ChkControllerNetworkNotifyDelegate.h"
#import "ChkSecurityNotifyDelegate.h"

@interface ChkViewController : UIViewController <UITableViewDelegate, UITableViewDataSource ,
    ChkControllerDelegate,ChkControllerNetworkNotifyDelegate,ChkSecurityNotifyDelegate>{
}

- (void) show;
- (id) initWith8chk:(ChkConfig *)config;

@end


@interface ChkTableViewCell : UITableViewCell {
	UILabel                         *_titleLabel;
	UILabel                         *_category;
	UILabel                         *_price;
	UILabel                         *_point;
	UIImageView                     *_appImageView;
}

@property (nonatomic, retain) UILabel       *titleLabel;
@property (nonatomic, retain) UILabel       *category;
@property (nonatomic, retain) UILabel       *price;
@property (nonatomic, retain) UILabel       *point;
@property (nonatomic, retain) UIImageView   *appImageView;

@end
