//
//  NewsDetailViewController.h
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "BaseViewController.h"
#import "News.h"

@protocol NewsDetailViewControllerDelegate <NSObject>

@end

@interface NewsDetailViewController : BaseViewController

-(id) initWithDelegate:(NSObject<NewsDetailViewControllerDelegate>*)delegate_ andNews:(News*)news_;

@end
