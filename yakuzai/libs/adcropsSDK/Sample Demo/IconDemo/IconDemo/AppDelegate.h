//
//  AppDelegate.h
//  IconDemo
//
//  Created by sazawayuki on 2014/02/14.
//  Copyright (c) 2014年 8crops inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
