//
//  ChkSlideIconView.h
//  8chk
//
//  Created by Sazawa Yuki on 02/06/14.
//  Copyright (c) 2014年 8crops inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>



// CPCサイズ
extern const CGSize kChkSlideIconAdViewCPCSize;
// Iconサイズ
extern const CGSize kChkSlideIconAdViewICONSize;

@class ChkSlideIconView;
@class ChkAppViewController;

@protocol ChkSlideIconViewDelegate <NSObject>
// 初回ロードが成功した際に通知(在庫なしの場合は呼ばれません)
- (void)chkBannerViewDidFinishLoad:(ChkSlideIconView *)slideIconView;
// 2回目以降の受信が成功した際に通知(在庫なしの場合は呼ばれません)
- (void)chkBannerViewDidReceiveAd:(ChkSlideIconView *)slideIconView;
// 受信が失敗した際に通知(在庫なしの場合はエラーが返されます)
- (void)chkBannerViewDidFailToReceiveAd:(ChkSlideIconView *)slideIconView;
@end

@interface ChkSlideIconView : UIView {
    
}
@property (nonatomic, weak) id <ChkSlideIconViewDelegate> delegate;
// モーダルビューを表示元のビューコントローラを指定
@property (nonatomic, weak) UIViewController *rootViewController;

// イニシャライズ(CPCサイズ)
- (id)initWithCpcSize:(CGFloat)x rootViewController:(UIViewController*)rootViewController;
// イニシャライズ(アイコンサイズ)
- (id)initWithIconSize:(CGFloat)x rootViewController:(UIViewController*)rootViewController;

// 広告のロード
- (void)load;
// インジケータの色を設定(iOS5.0以上対応)
- (void)setIndicatorColor:(UIColor*)color;
// バックグランドへ遷移した時
- (void)applicationDidEnterBackground;
// バッグランドから復帰した時
- (void)applicationWillEnterForeground;

// アプリ名を表示するか？
- (void)setShowAppName:(UIColor*)textColor backgroundColor:(UIColor*)backgroundColor backgroundAlpha:(CGFloat)backgroundAlpha;



@end
