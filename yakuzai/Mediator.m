//
//  Mediator.m
//  yakuzai
//
//  Created by kawase yu on 2014/07/05.
//  Copyright (c) 2014年 nowhappy. All rights reserved.
//

#import "Mediator.h"
#import "RootViewController.h"
#import "TopViewController.h"
#import "RecruitContents.h"
#import "NewsContents.h"
#import "RecommendContents.h"
#import "ChkController.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

#import "VknURLLoadCommand.h"

@interface Mediator ()<
TopViewDelegate
, RecruitContentsDelegate
, NewsContentsDelegate
, RecommendContentsDelegate
, AdstirMraidViewDelegate
>{
    UIWindow* window;
    TopViewController* topViewController;
    RecruitContents* recruitContents;
    NewsContents* newsContents;
    RecommendContents* recommendContents;
    RootViewController* rootViewController;
    ChkController* chkController;
    AdstirMraidView* adView;
    UIView* adViewWrapper;
}

@end

@implementation Mediator

-(id) initWithWindow:(UIWindow*)window_{
    self = [super init];
    if(self){
        window = window_;
        [self initialize];
        [self loadModel];
    }
    
    return self;
}

static NSString* API_URL = @"http://yaku.nowhappy.info/api/";
-(void) loadModel{
    VknUrlLoadCommand* c = [[VknUrlLoadCommand alloc] initWithUrl:API_URL];
    c.onComplete = ^(NSData* data){
        NSString* jsonStr = [VknDataUtil dataToString:data];
        if(jsonStr == nil) return;
        
        [YakuUtil setModelJson:jsonStr];
        [YakuUtil reloadModel];
        if([YakuUtil hasNewNews]){
            [newsContents hasNew];
        }
        [recommendContents getView].hidden = ![YakuUtil isShowRecommend];
        [self startApp];
    };
    c.onFail = ^(NSError* error){
        [YakuUtil reloadModel];
        [recommendContents getView].hidden = ![YakuUtil isShowRecommend];
        [self startApp];
    };
    
    [c execute:nil];
}

-(void) reloadModel{
    VknUrlLoadCommand* c = [[VknUrlLoadCommand alloc] initWithUrl:API_URL];
    c.onComplete = ^(NSData* data){
        NSString* jsonStr = [VknDataUtil dataToString:data];
        if(jsonStr == nil) return;
        
        [YakuUtil setModelJson:jsonStr];
        [YakuUtil reloadModel];
        if([YakuUtil hasNewNews]){
            [newsContents hasNew];
        }
        
        [recommendContents getView].hidden = ![YakuUtil isShowRecommend];
        [YakuUtil tryShowRecommend];
    };
    c.onFail = ^(NSError* error){
        [YakuUtil reloadModel];
        [recommendContents getView].hidden = ![YakuUtil isShowRecommend];
    };
    
    [c execute:nil];
}

-(void) startApp{
    [VknUtils wait:0.3f callback:^{
        [rootViewController startAnimation];
    }];
    [VknUtils wait:1.0f callback:^{
        [UIView animateWithDuration:0.2f animations:^{
            adViewWrapper.transform = CGAffineTransformMakeTranslation(0, 0);
        } completion:^(BOOL finished) {
            chkController = [[ChkController alloc] initWithDelegate:self];
            [chkController requestDataList];
            [adView start];
            
            [YakuUtil tryShowRecommend];
        }];
    }];
    
    NSLog(@"isShowRecommend:%d", [YakuUtil isShowRecommend]);
}

-(void) initialize{
    rootViewController = [[RootViewController alloc] init];
    [YakuUtil setup:rootViewController];
    
    window.rootViewController = rootViewController;
    
    topViewController = [[TopViewController alloc] initWithDelegate:self];
    recruitContents = [[RecruitContents alloc] initWithDelegate:self];
    newsContents = [[NewsContents alloc] initWithDelegate:self];
    recommendContents = [[RecommendContents alloc] initWithDelegate:self];
    
    UIView* rootView = [rootViewController contentView];
    [rootView addSubview:[recommendContents getView]];
    [rootView addSubview:[newsContents getView]];
    [rootView addSubview:[recruitContents getView]];
    [rootView addSubview:topViewController.view];
    
    
    // analytics
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [GAI sharedInstance].dispatchInterval = 10;
    //    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelError];
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-52771387-1"];
    [self trackScreen:@"トップ"];
    
    // 小さい画面ならad無し
    if([VknDeviceUtil is35Inch]) return;
    adView = [[AdstirMraidView alloc] initWithAdSize:kAdstirAdSize320x50 media:ADSTIR_MEDIA_ID spot:ADSTIR_BANNER_SPOT_ID];
    adViewWrapper = [[UIView alloc] initWithFrame:CGRectMake(0, [VknDeviceUtil windowHeight]-50, kAdstirAdSize320x50.size.width, kAdstirAdSize320x50.size.height)];
    [adViewWrapper addSubview:adView];
    adView.intervalTime = 10;
    adViewWrapper.backgroundColor = UIColorFromHex(0xaaaaaa);
    adViewWrapper.transform = CGAffineTransformMakeTranslation(0, 50);
    adView.delegate = self;
    
    [rootViewController.view addSubview:adViewWrapper];
}

-(void) trackScreen:(NSString*)screenName{
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:screenName];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

-(void) willEnterForeground{
    [self reloadModel];
}

-(void) memoryWarning{
    [VknImageCache clear];
}

-(void) didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    
}

-(void) onContentsShow:(int)index{
    [topViewController hide:index];
    
    if(index == 1){
        // 求人
        [self trackScreen:@"求人"];
    }else if(index == 2){
        // ニュース
        [self trackScreen:@"ニュース"];
        [recruitContents hide2];
    }else if(index == 3){
        // おすすめ
        [self trackScreen:@"オススメ"];
        [recruitContents hide2];
        [newsContents hide2];
    }
}

-(void) hideContents:(int)contentsIndex{
    [recruitContents hide];
    [newsContents hide];
    [recommendContents hide];
    
    [self trackScreen:@"トップ"];
}

-(void) onBack{
    
}

-(void) showBackButton{
    [topViewController showBack];
}

-(void) hideBackButton{
    [topViewController hideBack];
}

#pragma mark -- ChkControllerDelegate --

- (void) chkControllerDataListWithSuccess:(NSDictionary *)data{
    
    if( [chkController hasNextData] ) {
        NSLog(@"****** self.chkController retry request *****");
        [chkController requestDataList];
        return;
    }
    
    [YakuUtil setAdcropsList:[chkController dataList]];
    if([YakuUtil hasNewRecommend]){
        [recommendContents hasNew];
    }
}

- (void) chkControllerDataListWithError:(NSError*)error{
    NSLog(@"chkControllerDataListWithError:%@", error.debugDescription);
}

- (void) chkControllerDataListWithNotFound:(NSDictionary *)data{
    NSLog(@"chkControllerDataListWithNotFound");
    NSLog(@"%@", data);
}

#pragma mark -------- AdstirMraidViewDelegate -----------

- (void)adstirMraidViewWillPresentScreen:(AdstirMraidView *)mraidView{
    NSLog(@"adstirMraidViewWillPresentScreen");
}

- (void)adstirMraidViewDidPresentScreen:(AdstirMraidView *)mraidView{
    NSLog(@"adstirMraidViewDidPresentScreen");
}

- (void)adstirMraidViewWillDismissScreen:(AdstirMraidView *)mraidView{
    NSLog(@"adstirMraidViewWillDismissScreen");
}

- (void)adstirMraidViewWillLeaveApplication:(AdstirMraidView *)mraidView{
    NSLog(@"adstirMraidViewWillLeaveApplication");
}

@end
