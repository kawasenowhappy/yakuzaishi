AdstirWebView for iOS - README

Copyright 2012-2014 UNITED, inc. All rights reserved.


 -- Usage --

use AdSupport.framework

#import "AdstirWebView.h"

@property (retain) AdstirWebView* wv;


- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	self.wv = [[[AdstirWebView alloc]initWithFrame:CGRectMake(0, 0, 320, 50) media:@"MEDIA-ID" spot:@"SPOT-NO"]autorelease];
	[self.view addSubview:self.wv];
}


- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	[self.wv removeFromSuperview];
	self.wv = nil;
}

