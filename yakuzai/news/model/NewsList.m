//
//  NewsList.m
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "NewsList.h"

@implementation NewsList

-(News*) get:(int)index{
    return (News*)[super get:index];
}

-(void) reloadWithJson:(NSArray*)jsonArray{
    int i = 0;
    for(NSDictionary* row in jsonArray){
        News* news = [[News alloc] initWithJson:row];
        [self replace:news at:i];
        
        if([YakuUtil isReadedNews:news.id_]){
            [news readed];
        }
        i++;
    }
    
    [self removeToTail:i];
//    [self readedSort];
}

-(void) readedSort{
    for(int i=0,max=[self count];i<max;i++){
        News* news = [self get:i];
        if(news.isReaded){
            [items removeObject:news];
            [items addObject:news];
        }
    }
}

@end
