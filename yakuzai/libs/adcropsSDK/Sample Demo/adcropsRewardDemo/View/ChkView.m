//
//  ChkView.m
//  8chk
//
//  Created by 8crops Inc. on 11/06/14.
//  Copyright 2011 8crops Inc. All rights reserved.
//

#import "ChkView.h"
#import "chkViewSetting.h"
#import <UiKit/UIScreen.h>

@implementation ChkView

@synthesize chkConfig = _chkConfig;

#pragma mark -
#pragma mark Private method
- (CGAffineTransform)transformForOrientation {
	UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
	if (orientation == UIInterfaceOrientationLandscapeLeft) {
		return CGAffineTransformMakeRotation(M_PI*1.5);
	} else if (orientation == UIInterfaceOrientationLandscapeRight) {
		return CGAffineTransformMakeRotation(M_PI/2);
	} else if (orientation == UIInterfaceOrientationPortraitUpsideDown) {
		return CGAffineTransformMakeRotation(-M_PI);
	} else {
		return CGAffineTransformIdentity;
	}
}

- (void)bounce1AnimationStopped {
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:RVIEW_TRANSITION_DURATION/2];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(bounce2AnimationStopped)];
	self.transform = CGAffineTransformScale([self transformForOrientation], 0.9, 0.9);
	[UIView commitAnimations];
}

- (void)bounce2AnimationStopped {
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:RVIEW_TRANSITION_DURATION/2];
	self.transform = [self transformForOrientation];
	[UIView commitAnimations];
}

- (void) ViewAnimationDidStop {
    [self removeFromSuperview];
}

#pragma mark -
#pragma mark Pubulic method
- (void)show {
    // アニメーション種別を取得
    NSNumber *type = [NSNumber numberWithInt:CHK_VIEW_ANIMATION_TYPE];
    
    CGContextRef context = UIGraphicsGetCurrentContext();

    switch([type intValue]) {
        case ChkViewAnimationNone: {
            // なし
            break;
        }
        case ChkViewAnimationDefault: {
            // バウンド
            self.transform = CGAffineTransformScale([self transformForOrientation], 0.001, 0.001);
            [UIView beginAnimations:@"ChkViewAnimationDefault" context:context];
            [UIView setAnimationDuration:RVIEW_TRANSITION_DURATION/1.5];
            [UIView setAnimationDelegate:self];
            [UIView setAnimationDidStopSelector:@selector(bounce1AnimationStopped)];
            self.transform = CGAffineTransformScale([self transformForOrientation], 1.1, 1.1);
            break;
        }
        case ChkViewAnimationCurl: {
            // ページめくり
            [UIView beginAnimations:@"ChkViewAnimationCurl" context:context];   
            [UIView setAnimationDuration:1.0f];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
            [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp
                                   forView:self.window cache:YES];
            break;
        }
        case ChkViewAnimationFadeInOut: {
            // フェードイン/アウト
            [UIView beginAnimations:@"ChkViewAnimationFadeInOut" context:context];
            [self setAlpha:0.0f]; // アルファチャンネルを0.0fに
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            [UIView setAnimationDuration:1.0f]; // 時間の指定
            [self setAlpha:1.0f]; // アルファチャンネルを1.0fに
            break;
        }
        case ChkViewAnimationScrollUp: {
            // 下から上へ
            // ステータスバー領域を除いた領域を取得する
            CGRect sc = [[UIScreen mainScreen] applicationFrame];
            // 表示する中心座標を表示画面外に設定
            [self setCenter:CGPointMake((sc.size.width/2), (sc.size.height*2))]; 
            [UIView beginAnimations:@"ChkViewAnimationScrollUp" context:context];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            [UIView setAnimationDuration:1.0f]; // 時間の指定
            // 表示する中心座標を表示画面中央に
            [self setCenter:CGPointMake((sc.size.width/2), (sc.size.height/2)+RVIEW_FRAME_PADDING*2)];
            break;
        }
        case ChkViewAnimationScrollDown: {
            // 上から下へ
            // ステータスバー領域を除いた領域を取得する
            CGRect sc = [[UIScreen mainScreen] applicationFrame];
            // 表示する中心座標を表示画面外に設定
            [self setCenter:CGPointMake((sc.size.width/2), -(sc.size.height/2))];
            [UIView beginAnimations:@"ChkViewAnimationScrollDown" context:context];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            [UIView setAnimationDuration:1.0f]; // 時間の指定
            // 表示する中心座標を表示画面中央に
            [self setCenter:CGPointMake((sc.size.width/2), (sc.size.height/2)+RVIEW_FRAME_PADDING*2)];    
            break;
        }
        case ChkViewAnimationScrollLeft: {
            // 右から左へ
            // ステータスバー領域を除いた領域を取得する
            CGRect sc = [[UIScreen mainScreen] applicationFrame];
            [self setCenter:CGPointMake(sc.size.width*2, (sc.size.height/2)+RVIEW_FRAME_PADDING*2)];
            [UIView beginAnimations:@"ChkViewAnimationScrollLeft" context:context];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            [UIView setAnimationDuration:1.0f]; // 時間の指定
            [self setCenter:CGPointMake((sc.size.width/2), (sc.size.height/2)+RVIEW_FRAME_PADDING*2)];
            break;
        }
        case ChkViewAnimationScrollRight: {
            // 左から右へ
            // ステータスバー領域を除いた領域を取得する
            CGRect sc = [[UIScreen mainScreen] applicationFrame];
            [self setCenter:CGPointMake(-(sc.size.width*2), (sc.size.height/2)+RVIEW_FRAME_PADDING*2)];
            [UIView beginAnimations:@"ChkViewAnimationScrollRight" context:context];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            [UIView setAnimationDuration:1.0f]; // 時間の指定
            [self setCenter:CGPointMake((sc.size.width/2), (sc.size.height/2)+RVIEW_FRAME_PADDING*2)];
            break;
        }
        case ChkViewAnimationFlipFromLeft: {
            [UIView beginAnimations:@"ChkViewAnimationFlipFromLeft" context:context];   
            [UIView setAnimationDuration:1.0f];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
            [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft
                                   forView:self.window cache:YES];
            break;
        }
        case ChkViewAnimationFlipFromRight: {
            [UIView beginAnimations:@"ChkViewAnimationFlipFromRight" context:context];   
            [UIView setAnimationDuration:1.0f];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
            [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
                                   forView:self.window cache:YES];
            break;
        }
        default: {
            NSLog(@"##### ChkViewAnimation Not Found");
            break;
        }
    }
    [UIView commitAnimations];
}

- (void)hide {
    // アニメーション種別を取得
    NSNumber *type = [NSNumber numberWithInt:CHK_VIEW_ANIMATION_TYPE];
    
    CGContextRef context = UIGraphicsGetCurrentContext();

    switch([type intValue]) {
        case ChkViewAnimationNone: {
            // なし
            break;
        }
        case ChkViewAnimationDefault: {
            // バウンド
            self.transform = CGAffineTransformScale([self transformForOrientation], 1.1, 1.1);
            [UIView beginAnimations:@"ChkViewAnimationDefault" context:context];
            [UIView setAnimationDuration:RVIEW_TRANSITION_DURATION/1.5];
            [UIView setAnimationDelegate:self];
            [UIView setAnimationDidStopSelector:@selector(bounce1AnimationStopped)];
            self.transform = CGAffineTransformScale([self transformForOrientation], 0.001, 0.001);
            break;
        }
        case ChkViewAnimationCurl: {
            // ページめくり
            [UIView beginAnimations:@"ChkViewAnimationCurl" context:context];   
            [UIView setAnimationDuration:1.0f];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
            [UIView setAnimationTransition:UIViewAnimationTransitionCurlDown
                                   forView:self.window cache:YES];
            [self removeFromSuperview];
            break;
        }
        case ChkViewAnimationFadeInOut: {
            // フェードイン/アウト
            [UIView beginAnimations:@"ChkViewAnimationFadeInOut" context:context];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            [UIView setAnimationDuration:1.0f];    // 時間の指定
            [self setAlpha:0.0f];    // アルファチャンネルを0.0fに
            break;
        }
        case ChkViewAnimationScrollUp: {
            // 下から上へ
            // ステータスバー領域を除いた領域を取得する
            CGRect sc = [[UIScreen mainScreen] applicationFrame];
            // 表示する中心座標を表示画面中央に
            [self setCenter:CGPointMake((sc.size.width/2), (sc.size.height/2)+RVIEW_FRAME_PADDING*2)];
            [UIView beginAnimations:@"ChkViewAnimationScrollUp" context:context];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            [UIView setAnimationDuration:1.0f]; // 時間の指定
            // 表示する中心座標を表示画面外に設定
            [self setCenter:CGPointMake((sc.size.width/2), (sc.size.height*2))]; 
            break;
        }
        case ChkViewAnimationScrollDown: {
            // 上から下へ
            // ステータスバー領域を除いた領域を取得する
            CGRect sc = [[UIScreen mainScreen] applicationFrame];
            // 表示する中心座標を表示画面中央に
            [self setCenter:CGPointMake((sc.size.width/2), (sc.size.height/2)+RVIEW_FRAME_PADDING*2)];    
            [UIView beginAnimations:@"ChkViewAnimationScrollDown" context:context];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            [UIView setAnimationDuration:1.0f]; // 時間の指定
            // 表示する中心座標を表示画面外に設定
            [self setCenter:CGPointMake((sc.size.width/2), -(sc.size.height/2))];
            break;
        }
        case ChkViewAnimationScrollLeft: {
            // 右から左へ
            // ステータスバー領域を除いた領域を取得する
            CGRect sc = [[UIScreen mainScreen] applicationFrame];
            [self setCenter:CGPointMake((sc.size.width/2), (sc.size.height/2)+RVIEW_FRAME_PADDING*2)];
            [UIView beginAnimations:@"ChkViewAnimationScrollLeft" context:context];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            [UIView setAnimationDuration:1.0f]; // 時間の指定
            [self setCenter:CGPointMake(sc.size.width*2, (sc.size.height/2)+RVIEW_FRAME_PADDING*2)];
            break;
        }
        case ChkViewAnimationScrollRight: {
            // 左から右へ
            // ステータスバー領域を除いた領域を取得する
            CGRect sc = [[UIScreen mainScreen] applicationFrame];
            [self setCenter:CGPointMake((sc.size.width/2), (sc.size.height/2)+RVIEW_FRAME_PADDING*2)];
            [UIView beginAnimations:@"ChkViewAnimationScrollRight" context:context];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            [UIView setAnimationDuration:1.0f]; // 時間の指定
            [self setCenter:CGPointMake(-(sc.size.width*2), (sc.size.height/2)+RVIEW_FRAME_PADDING*2)];
            break;
        }
        case ChkViewAnimationFlipFromLeft: {
            [UIView beginAnimations:@"ChkViewAnimationFlipFromLeft" context:context];   
            [UIView setAnimationDuration:1.0f];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
            [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
                                   forView:self.window cache:YES];
            [self removeFromSuperview];
            break;
        }
        case ChkViewAnimationFlipFromRight: {
            [UIView beginAnimations:@"ChkViewAnimationFlipFromRight" context:context];   
            [UIView setAnimationDuration:1.0f];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
            [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft
                                   forView:self.window cache:YES];
            [self removeFromSuperview];
            break;
        }
        default: {
            NSLog(@"##### ChkViewAnimation Not Found");
            break;
        }
    }
    [UIView setAnimationDidStopSelector:@selector(ViewAnimationDidStop)];
    [UIView commitAnimations];
}

#pragma mark -
#pragma mark Initialization
- (id)initWithConfig:(ChkConfig*)config {
	if ((self = [super initWithFrame:CGRectZero])) {
        self.chkConfig = config;
		self.backgroundColor = [UIColor clearColor];
		self.autoresizesSubviews = YES;
		self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		
		// windowのフレームサイズを取得する
		CGRect frame = [UIScreen mainScreen].applicationFrame;
		
		// windowの中心ポイントを取得する
		CGPoint center = CGPointMake(
									 frame.origin.x + ceil(frame.size.width/2),
									 frame.origin.y + ceil(frame.size.height/2));
		
		// frameサイズを設定する
		self.frame = CGRectMake(
								frame.origin.x + RVIEW_FRAME_PADDING,
								frame.origin.y + RVIEW_FRAME_PADDING,
								frame.size.width - RVIEW_FRAME_PADDING * 2,
								frame.size.height - RVIEW_FRAME_PADDING *2);
		self.center = center;
	}
	return self;
}

#pragma mark -
#pragma mark drawRect method
- (void)addRoundedRectToPath:(CGContextRef)context rect:(CGRect)rect radius:(float)radius {
	CGContextBeginPath(context);
	CGContextSaveGState(context);
	
	if (radius == 0) {
		CGContextTranslateCTM(context, CGRectGetMinX(rect), CGRectGetMinY(rect));
		CGContextAddRect(context, rect);
	} else {
		rect = CGRectOffset(CGRectInset(rect, 0.5, 0.5), 0.5, 0.5);
		CGContextTranslateCTM(context, CGRectGetMinX(rect)-0.5, CGRectGetMinY(rect)-0.5);
		CGContextScaleCTM(context, radius, radius);
		float fw = CGRectGetWidth(rect) / radius;
		float fh = CGRectGetHeight(rect) / radius;
		
		CGContextMoveToPoint(context, fw, fh/2);
		CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);
		CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1);
		CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1);
		CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1);
	}
	
	CGContextClosePath(context);
	CGContextRestoreGState(context);
}

- (void)drawRect:(CGRect)rect fill:(const CGFloat*)fillColors radius:(CGFloat)radius {
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
	
	if (fillColors) {
		CGContextSaveGState(context);
		CGContextSetFillColor(context, fillColors);
		if (radius) {
			[self addRoundedRectToPath:context rect:rect radius:radius];
			CGContextFillPath(context);
		} else {
			CGContextFillRect(context, rect);
		}
		CGContextRestoreGState(context);
	}
	
	CGColorSpaceRelease(space);
}

- (void)strokeLines:(CGRect)rect stroke:(const CGFloat*)strokeColor {
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
	
	CGContextSaveGState(context);
	CGContextSetStrokeColorSpace(context, space);
	CGContextSetStrokeColor(context, strokeColor);
	CGContextSetLineWidth(context, 1.0);
    
	{
		CGPoint points[] = {{rect.origin.x+0.5, rect.origin.y-0.5},
			{rect.origin.x+rect.size.width, rect.origin.y-0.5}};
		CGContextStrokeLineSegments(context, points, 2);
	}
	{
		CGPoint points[] = {{rect.origin.x+0.5, rect.origin.y+rect.size.height-0.5},
			{rect.origin.x+rect.size.width-0.5, rect.origin.y+rect.size.height-0.5}};
		CGContextStrokeLineSegments(context, points, 2);
	}
	{
		CGPoint points[] = {{rect.origin.x+rect.size.width-0.5, rect.origin.y},
			{rect.origin.x+rect.size.width-0.5, rect.origin.y+rect.size.height}};
		CGContextStrokeLineSegments(context, points, 2);
	}
	{
		CGPoint points[] = {{rect.origin.x+0.5, rect.origin.y},
			{rect.origin.x+0.5, rect.origin.y+rect.size.height}};
		CGContextStrokeLineSegments(context, points, 2);
	}
	
	CGContextRestoreGState(context);
	
	CGColorSpaceRelease(space);
}

+ (UIColor*)convertHtmlColorToUIColor:(NSString *)htmlcolor{
    NSScanner *scanner = [NSScanner scannerWithString:[htmlcolor stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@"0x"]];
    [scanner setCharactersToBeSkipped:[NSCharacterSet symbolCharacterSet]];
    unsigned int basecolor;
    [scanner scanHexInt:&basecolor];
    CGFloat red = ((basecolor & 0xFF0000) >> 16) / 255.0f;
    CGFloat green = ((basecolor & 0x00FF00) >> 8) / 255.0f;
    CGFloat blue = (basecolor & 0x0000FF) / 255.0f;
    return [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
}

- (void)drawRect:(CGRect)rect {
	CGRect grayRect = CGRectOffset(rect, -0.5, -0.5);
    
    const CGFloat *frameComponents = CGColorGetComponents([[ChkView convertHtmlColorToUIColor:CHK_VIEW_APP_FRAME_COLOR]CGColor]);
    CGFloat red = frameComponents[0];
    CGFloat green = frameComponents[1];
    CGFloat blue = frameComponents[2];
    CGFloat alpha = 0.8;
    CGFloat kBorderFrame[4] = {red, green, blue, alpha};
	[self drawRect:grayRect fill:kBorderFrame radius:10];
	
	CGRect headerRect = CGRectMake(
								   ceil(rect.origin.x + RVIEW_BORDER_WIDTH), ceil(rect.origin.y + RVIEW_BORDER_WIDTH),
								   rect.size.width - RVIEW_BORDER_WIDTH*2, 26);
	CGFloat kBorderBlue[4] = {0.23, 0.35, 0.6, 1.0};
	
    const CGFloat *titlebackgroundComponents = CGColorGetComponents([[ChkView convertHtmlColorToUIColor:CHK_VIEW_APP_TITLE_BACKGROUND_COLOR]CGColor]);
    
	[self drawRect:headerRect fill:titlebackgroundComponents radius:0];
	[self strokeLines:headerRect stroke:kBorderBlue];
	
}

#pragma mark -
#pragma mark Memory management
- (void)dealloc {
    self.chkConfig = nil;
    [super dealloc];
}



@end
