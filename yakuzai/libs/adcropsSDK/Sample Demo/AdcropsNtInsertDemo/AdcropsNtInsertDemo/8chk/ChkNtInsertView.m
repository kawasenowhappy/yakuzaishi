//
//  ChkNtInsertView.m
//  AdcropsNtInsertDemo
//
//  Created by sazawayuki on 2014/03/24.
//  Copyright (c) 2014年 8crops inc. All rights reserved.
//

#import "ChkNtInsertView.h"
#import "ChkController.h"
#import "ChkControllerDelegate.h"
#import <StoreKit/StoreKit.h>
#import "ChkControllerNetworkNotifyDelegate.h"

@interface ChkNtInsertView () <ChkControllerDelegate,UIScrollViewDelegate,SKStoreProductViewControllerDelegate, ChkControllerNetworkNotifyDelegate>{
    dispatch_queue_t main_queue_;
    dispatch_queue_t image_queue_;
    dispatch_queue_t interstitialImage_main_queue_;
    dispatch_queue_t interstitialImage_queue_;
    BOOL chkLoadSuccessfulFlag_;// 初回ロード成功フラグ
    CGSize scrollViewSize_;
}
@property (nonatomic, strong) ChkController    *chkController;
@property (nonatomic, weak) UIViewController *rootViewController;
@property (nonatomic, strong) UIScrollView              *chkScrollView;
@property (nonatomic, strong) UIActivityIndicatorView   *chkIndicator;
@property (nonatomic, strong) UIView                    *chkLoadingView;
@property (nonatomic, strong) UIActivityIndicatorView   *chkStoreIndicator;
@property (nonatomic, strong) UIView                    *chkStoreLoadingView;
// インプレッション送信カウント用
@property (nonatomic, strong) NSMutableDictionary       *chkImpressionDic;
@property(nonatomic, strong) NSMutableArray             *chkDataList;
@property (nonatomic, strong) UIView                    *chkAppBgView;

@end

@implementation ChkNtInsertView

// サイズ
const CGSize kChkNtInsertViewSize = {320, 308};

#pragma mark -
#pragma mark Private Method
-(void) sendImpression:(int)listNumber {
    int dataCount = [self.chkDataList count];
    if( dataCount <= listNumber ) {
        return;
    }
    // 1ページに表示される分だけインプレッション送信する
    int max = listNumber - [self getOnePageAppCount];
    if(max<0) max = 0;
    for (int i = listNumber; i >= max; i--) {
        ChkRecordData *data = [self.chkDataList objectAtIndex:i];
        NSString *isSend = [self.chkImpressionDic objectForKey:data.appStoreId];
        //NSLog(@"***Value:%@",isSend);
        if(![isSend isEqualToString:@"1"]) {
            // インプレッション送信
            NSLog(@"sendImpression:%d:%@:%@",i,data.title,data.appStoreId);
            [self.chkController sendImpression:data];
            [self.chkImpressionDic setObject:@"1" forKey:data.appStoreId];
        }else{
            //NSLog(@"****Impression Sended:%d:%@",i,data.title);
        }
    }
    //NSLog(@"self.chkImpressionDic:%@",self.chkImpressionDic);
}

-(int) getOnePageAppCount {
    return 1;
}

-(CGFloat) getAppSize {
    return 280.0f;
}

-(CGFloat) getAppMarginWidth {
    return 10.0f;
}

- (id)initWithFrame:(CGFloat)x rootViewController:(UIViewController*)rootViewController{
    CGRect frame = CGRectMake(0,
                              x,
                              kChkNtInsertViewSize.width,
                              kChkNtInsertViewSize.height);
    
    if (self = [super initWithFrame:frame]) {
        NSLog(@"%s : initWithFrame", __func__);
        self.backgroundColor = [UIColor clearColor];
        
        UIImageView *bgView = [[UIImageView alloc] init];
        bgView.frame = CGRectMake(9, 0, 302, 308);
        bgView.image = [UIImage imageNamed:@"background"];
        [self addSubview:bgView];
        
        UIImageView *matchImg = [[UIImageView alloc] init];
        matchImg.frame = CGRectMake(bgView.frame.size.width - 35, 0, 35, 23);
        matchImg.image =[UIImage imageNamed:@"match"];
        [bgView addSubview:matchImg];
        
        UILabel *headLbl = [[UILabel alloc] init];
        headLbl.frame = CGRectMake(20, 10, 100, 20);
        headLbl.text = @"おすすめアプリ";
        headLbl.textColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0];
        headLbl.font = [UIFont fontWithName:@"HiraKakuProN-W6" size:14];
        [bgView addSubview:headLbl];
        
        self.rootViewController = rootViewController;
        self.chkImpressionDic = [[NSMutableDictionary alloc] init];
        main_queue_ = dispatch_get_main_queue();
        interstitialImage_main_queue_ = dispatch_get_main_queue();
		image_queue_ = dispatch_queue_create("net.adcrops.8chk.list-image", NULL);
        interstitialImage_queue_ = dispatch_queue_create("net.adcrops.8chk.interstitial-image", NULL);
        chkLoadSuccessfulFlag_ = NO;
        self.chkController = [[ChkController alloc]initWithDelegate:self];
        
        // 画像のスクロールビュー
        self.chkScrollView = [[UIScrollView alloc] init];
        self.chkScrollView.frame = CGRectMake(20, headLbl.frame.size.height+headLbl.frame.origin.y+5, 280, 265);
        CGRect scrollViewFrame = self.chkScrollView.frame;
        scrollViewSize_ = scrollViewFrame.size;
        scrollViewFrame.origin.x -= [self getAppMarginWidth]/2;
        scrollViewFrame.size.width += [self getAppMarginWidth];
        self.chkScrollView.frame = scrollViewFrame;
        //self.scrollView.contentSize = CGSizeMake((scrollViewSize.width+[self getIconMarginWidth])*5, scrollViewSize.height);
        self.chkScrollView.pagingEnabled = YES;
        self.chkScrollView.clipsToBounds = NO;
        [self addSubview:self.chkScrollView];

        self.chkScrollView.backgroundColor = [UIColor clearColor];
        self.chkScrollView.pagingEnabled = YES; // ページ単位
        self.chkScrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
        [self.chkScrollView setCanCancelContentTouches:NO];
        self.chkScrollView.showsVerticalScrollIndicator = NO;
        self.chkScrollView.showsHorizontalScrollIndicator = NO;
        self.chkScrollView.scrollsToTop = NO;
        self.chkScrollView.clipsToBounds = NO;
        self.chkScrollView.scrollEnabled = YES;
        self.chkScrollView.delegate = self;

        self.chkLoadingView = [[UIView alloc] init];
        self.chkLoadingView.frame = CGRectMake(0, 0, kChkNtInsertViewSize.width, kChkNtInsertViewSize.height);
        self.chkLoadingView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        self.chkLoadingView.userInteractionEnabled = NO;
        [self addSubview:self.chkLoadingView];
        
        self.chkStoreLoadingView = [[UIView alloc] init];
        self.chkStoreLoadingView.frame = rootViewController.view.bounds;
        self.chkStoreLoadingView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        self.chkStoreLoadingView.userInteractionEnabled = NO;
        [rootViewController.view addSubview:self.chkStoreLoadingView];
        self.chkStoreLoadingView.hidden = YES;
        
        // スクロールビューのインジケータ
        if ([[UIActivityIndicatorView class] respondsToSelector:@selector(appearance)]) {
            // iOS 5.0以上の処理
            self.chkIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            [self.chkIndicator setColor:[UIColor whiteColor]]; // インジケータの色
        }else{
            // iOS 5.0未満(iOS 4.3以下)の処理
            self.chkIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        }
        
        self.chkIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin
        | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        self.chkIndicator.center = self.chkLoadingView.center;
        [self.chkLoadingView addSubview:self.chkIndicator];
        [self.chkIndicator startAnimating];
        
        if ([[UIActivityIndicatorView class] respondsToSelector:@selector(appearance)]) {
            // iOS 5.0以上の処理
            self.chkStoreIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            [self.chkStoreIndicator setColor:[UIColor whiteColor]]; // インジケータの色
        }else{
            // iOS 5.0未満(iOS 4.3以下)の処理
            self.chkStoreIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        }
        
        self.chkStoreIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin
        | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        self.chkStoreIndicator.center = self.chkStoreLoadingView.center;
        [self.chkStoreLoadingView addSubview:self.chkStoreIndicator];
        [self.chkStoreIndicator startAnimating];
    }
    return self;
}

#pragma mark - chkControllerNetworkNotifyDelegate
-(void)chkControllerInitNotReachableStatusError:(NSError *)error{
    //ChkController⽣生成時にネットワークに接続できない場合に呼び出されます。
    [self.chkIndicator stopAnimating];
}

-(void)chkControllerNetworkNotReachable:(NSError *)error{
    //ネットワークの状況が接続→切切断に変わった時に呼び出されます。
    [self.chkIndicator stopAnimating];
}

-(void)chkControllerNetworkReachable:(NSUInteger)networkType{
    //ネットワークの状況が切切断→接続に変わった時に呼び出されます。引数には、WiFiにつながった場合1、3Gにつながった場合は2が設定されます。
    [self.chkIndicator startAnimating];
}

-(void)chkControllerRequestNotReachableStatusError:(NSError *)error{
    //データ取得時にネットワークに接続できない場合に呼び出されます。
    [self.chkIndicator stopAnimating];
}

#pragma mark - chkControllerDataList

//エラーが起きた場合
-(void)chkControllerDataListWithError:(NSError *)error
{
    NSLog(@"chkControllerDataListWithError");
    // エラー処理を書きます。
    [self.chkIndicator stopAnimating];
}

//在庫切れ
-(void)chkControllerDataListWithNotFound:(NSDictionary *)data{
    NSLog(@"chkControllerDataListWithNotFound:%@",data);
    [self.chkIndicator stopAnimating];
    self.chkLoadingView.hidden = YES;
}

//成功した場合
-(void)chkControllerDataListWithSuccess:(NSDictionary *)data
{
    if( [self.chkController hasNextData] ) {
        [self.chkController requestDataList];
        return;
    }
    
    [self.chkIndicator stopAnimating];
    self.chkLoadingView.hidden = YES;
    
    float scroolWidth = 0.0f;
    int i = 0;
    CGFloat x = 0;
    for (ChkRecordData* chkRecordData in [self.chkController dataList]) {
        if (chkRecordData.hasNativeBanner) {
            [self.chkDataList addObject:chkRecordData];
        }
    }
    
    for (ChkRecordData* chkInterstitialRecordData in self.chkDataList) {
        //left space
        x += [self getAppMarginWidth]/2.0;
        self.chkAppBgView = [[UIView alloc] init];
        self.chkAppBgView.backgroundColor = [UIColor whiteColor];
        [[self.chkAppBgView layer] setBorderColor:[[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0] CGColor]];
        [[self.chkAppBgView layer] setBorderWidth:0.3];
        CGRect rect = CGRectMake(x, 0, scrollViewSize_.width, scrollViewSize_.height);
        self.chkAppBgView.frame = rect;
        x += rect.size.width;;
        // right space
        x += [self getAppMarginWidth]/2.0;
        
        // アプリアイコン
        UIImageView *chkIconImage = [[UIImageView alloc] init];
        chkIconImage.frame = CGRectMake(10, 7, 54, 54);
        chkIconImage.layer.masksToBounds = YES;
        chkIconImage.layer.cornerRadius = 8.0f;
        if (![chkInterstitialRecordData cacheImage]) {
            NSString *url = chkInterstitialRecordData.imageIcon;
            dispatch_async(image_queue_, ^{
                UIImage *icon = [self.chkController getImage:url];
                // 画像イメージをキャッシュする。
                [chkInterstitialRecordData setCacheImage:icon];
                dispatch_async(main_queue_, ^{
                    chkIconImage.image = icon;
                });
            });
        } else {
            [chkIconImage setImage:[chkInterstitialRecordData cacheImage]];
        }
        [self.chkAppBgView addSubview:chkIconImage];
        
        scroolWidth += self.chkAppBgView.frame.size.width + [self getAppMarginWidth];
        
        //アプリ詳細
        UILabel *appDetailLabel = [[UILabel alloc] init];
        appDetailLabel.backgroundColor = [UIColor clearColor];
        appDetailLabel.font = [UIFont fontWithName:@"HiraMinProN-W3" size:11];
        NSMutableParagraphStyle *paragrahStyle = [[NSMutableParagraphStyle alloc] init];
        paragrahStyle.lineHeightMultiple = 1.4f;
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:chkInterstitialRecordData.description];
        [attributedText addAttribute:NSParagraphStyleAttributeName
                               value:paragrahStyle
                               range:NSMakeRange(0, attributedText.length)];
        appDetailLabel.frame = CGRectMake(chkIconImage.frame.size.width + chkIconImage.frame.origin.x+5, 5, scrollViewSize_.width-20-chkIconImage.frame.size.width, 54);
        appDetailLabel.attributedText = attributedText;
        [appDetailLabel setAdjustsFontSizeToFitWidth:NO];
        appDetailLabel.numberOfLines = 3;
        appDetailLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [self.chkAppBgView addSubview:appDetailLabel];
        
        //インタースティシャル画像
        UIImageView *chkInterstitialImg = [[UIImageView alloc] init];
        chkInterstitialImg.frame = CGRectMake(0, chkIconImage.frame.origin.y+chkIconImage.frame.size.height+8, 280, 143.5);
        [self.chkAppBgView addSubview:chkInterstitialImg];
        NSString *url = chkInterstitialRecordData.nativeBannerUrl;
        dispatch_async(interstitialImage_queue_, ^{
            UIImage *icon = [self.chkController getImage:url];
            [chkInterstitialRecordData setCacheImage:icon];
            dispatch_async(interstitialImage_main_queue_, ^{
                chkInterstitialImg.image = icon;
            });
        });
        
        //アプリ名
        UILabel *chkAppNameLabel = [[UILabel alloc] init];
        chkAppNameLabel.backgroundColor = [UIColor clearColor];
        chkAppNameLabel.font = [UIFont fontWithName:@"HiraMinProN-W6" size:11];
        chkAppNameLabel.frame = CGRectMake(10, chkInterstitialImg.frame.size.height+chkInterstitialImg.frame.origin.y+5, 150, 30);
        chkAppNameLabel.text = chkInterstitialRecordData.title;
        chkAppNameLabel.numberOfLines = 2;
        chkAppNameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [chkAppNameLabel setAdjustsFontSizeToFitWidth:NO];
        [self.chkAppBgView addSubview:chkAppNameLabel];
        
        //アプリカテゴリ
        UILabel *chkAppCategoryLabel = [[UILabel alloc] init];
        chkAppCategoryLabel.backgroundColor = [UIColor clearColor];
        chkAppCategoryLabel.font = [UIFont fontWithName:@"HiraMinProN-W3" size:8];
        chkAppCategoryLabel.textColor = [UIColor colorWithRed:0.6 green:0.6 blue:0.6 alpha:1.0];
        chkAppCategoryLabel.frame = CGRectMake(10, chkAppNameLabel.frame.size.height+chkAppNameLabel.frame.origin.y, 150, 10);
        chkAppCategoryLabel.text = chkInterstitialRecordData.category;
        [chkAppCategoryLabel setAdjustsFontSizeToFitWidth:NO];
        [self.chkAppBgView addSubview:chkAppCategoryLabel];
        
        //インストールボタン
        UIButton *chkAppInstall = [UIButton buttonWithType:UIButtonTypeCustom];
        chkAppInstall.frame = CGRectMake(scrollViewSize_.width-99, chkInterstitialImg.frame.origin.y+chkInterstitialImg.frame.size.height+8, 90, 35);
        chkAppInstall.tag = i;
        [chkAppInstall addTarget:self action:@selector(chkIconTap:) forControlEvents:UIControlEventTouchUpInside];
        [chkAppInstall setImage:[UIImage imageNamed:@"install"] forState:UIControlStateNormal];
        [self.chkAppBgView addSubview:chkAppInstall];
        
        // インプレッション送信する。1ページ目に表示される物は表示時にインプレッション送信する。
        if(i < [self getOnePageAppCount]) {
            [self sendImpression:i];
        }
        
        [self.chkScrollView addSubview:self.chkAppBgView];
        i++;
    }
    if(scroolWidth + [self getAppMarginWidth] < self.frame.size.width ) {
        self.chkScrollView.contentSize = CGSizeMake( scrollViewSize_.height + 1.0f, scrollViewSize_.height);
    }else{
        self.chkScrollView.contentSize = CGSizeMake( scroolWidth, scrollViewSize_.height);
    }
    
    if(!chkLoadSuccessfulFlag_ && [[self.chkController dataList] count] > 0) {
        // 初回の受信成功時
        [self setHidden:NO];
        chkLoadSuccessfulFlag_ = YES;
        if ([self.delegate respondsToSelector:@selector(chkBannerViewDidFinishLoad:)]) {
            [self.delegate chkBannerViewDidFinishLoad:self];
        }
    }else{
        if([[self.chkController dataList] count] == 0 ) {
            // 案件無しはエラーとする。
            [self setHidden:YES];
            if ([self.delegate respondsToSelector:@selector(chkBannerViewDidFailToReceiveAd:)]) {
                [self.delegate chkBannerViewDidFailToReceiveAd:self];
            }
        }else{
            // 2回目以降の受信成功時
            [self setHidden:NO];
            if ([self.delegate respondsToSelector:@selector(chkBannerViewDidReceiveAd:)]) {
                [self.delegate chkBannerViewDidReceiveAd:self];
            }
        }
    }
}

- (void)load {
    NSLog(@"load");
    for(id subview in self.chkScrollView.subviews) {
        if([subview tag] <= [[self.chkController dataList] count] ) {
            [subview removeFromSuperview];
            NSLog(@"%s : remove subview:%ld", __func__, (long)[subview tag]);
        }
    }
    // スクロール位置を初期化
    [self.chkScrollView setContentOffset:CGPointMake(0.0f, 0.0f)];
    [self.chkIndicator startAnimating];
    self.chkLoadingView.hidden = NO;
    self.chkImpressionDic = [[NSMutableDictionary alloc] init];
    self.chkDataList = [NSMutableArray array];
    [self.chkIndicator startAnimating];
    [self.chkController resetDataList];
    [self.chkController requestDataList];
}

- (void) chkIconTap:(UIButton*) sender {
    if([[self.chkController dataList]count] > 0 ) {
        ChkRecordData *recordData = [[ChkRecordData alloc] init];
        recordData = [self.chkDataList objectAtIndex:sender.tag];
        NSLog(@"click url:%@",recordData.linkUrl);
        UIApplication *application = [UIApplication sharedApplication];
        NSURL *url = [NSURL URLWithString:recordData.linkUrl];
        
        //入稿URLが計測URLの場合Safari起動
        if (recordData.isMeasuring) {
            
            if( [application canOpenURL:url]){
                [application openURL:url];
            }else{
                NSLog(@"openUrl error.");
            }
            
        } else {
            
            if(NSClassFromString(@"SKStoreProductViewController")) { //ios6のバージョンの処理
                for(id subview in self.chkAppBgView.subviews) {
                    if ([subview isKindOfClass:[UIButton class]]){
                        ((UIButton *)subview).enabled = NO;
                    }
                }
                self.chkStoreLoadingView.hidden = NO;
                [self.chkStoreIndicator startAnimating];
                [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
                self.chkScrollView.scrollEnabled = NO;
                NSURLRequest  *request = [[NSURLRequest alloc] initWithURL:url];
                [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                    
                    if (error) {
                        // エラー処理を行う。
                        if (error.code == -1003) {
                            NSLog(@"not found hostname. targetURL=%@", url);
                        } else if (error.code == -1019) {
                            NSLog(@"auth error. reason=%@", error);
                        } else {
                            NSLog(@"unknown error occurred. reason = %@", error);
                        }
                        
                    } else {
                        int httpStatusCode = ((NSHTTPURLResponse *)response).statusCode;
                        if (httpStatusCode == 404) {
                            NSLog(@"404 NOT FOUND ERROR. targetURL=%@", url);
                            
                        } else {
                            NSLog(@"success request!!");
                            
                            NSDictionary *productParameters = @{ SKStoreProductParameterITunesItemIdentifier:recordData.appStoreId};
                            
                            SKStoreProductViewController* storeviewController = [[SKStoreProductViewController alloc] init];
                            storeviewController.delegate = self;
                            
                            [storeviewController loadProductWithParameters:productParameters completionBlock:^(BOOL result, NSError *storeRequestError) {
                                if (result) {
                                    for(id subview in self.chkAppBgView.subviews) {
                                        if ([subview isKindOfClass:[UIButton class]]){
                                            ((UIButton *)subview).enabled = YES;
                                        }
                                    }
                                    self.chkStoreLoadingView.hidden = YES;
                                    [self.chkStoreIndicator stopAnimating];
                                    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                                    self.chkScrollView.scrollEnabled = YES;
                                    [self.rootViewController presentViewController:storeviewController animated:YES completion:nil];
                                }
                                if (storeviewController) {
                                    self.chkStoreLoadingView.hidden = YES;
                                    [self.chkStoreIndicator stopAnimating];
                                    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                                    self.chkScrollView.scrollEnabled = YES;
                                }
                                
                            }];
                            
                        }
                    }
                }];
                
            } else { //ios6以前のバージョンの処理
                
                if( [application canOpenURL:url]){
                    [application openURL:url];
                }else{
                    NSLog(@"openUrl error.");
                }
            }
        }
        
    }
}

#pragma mark -
#pragma mark SKStoreProductViewController delegate

- (void)productViewControllerDidFinish:(SKStoreProductViewController*)viewController
{
	[self.rootViewController dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark -
#pragma mark UIScrollViewDelegate delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    float x = scrollView.contentOffset.x;
    
    if(x>0) {
        // インプレッション
        float oneSize = [self getAppSize] + [self getAppMarginWidth];
        int iconCount = (x+self.frame.size.width)/oneSize;
        [self sendImpression:iconCount-1];
    }
}

-(void)dealloc{
    NSLog(@"%s", __func__);
    self.delegate = nil;
    self.rootViewController = nil;
    [self.chkController clearDelegate];
    self.chkController = nil;
    self.chkScrollView  = nil;
    self.chkIndicator       = nil;
    self.chkImpressionDic   = nil;
    self.chkDataList = nil;
    self.chkLoadingView = nil;
    self.chkAppBgView = nil;
    self.chkStoreIndicator = nil;
    self.chkStoreLoadingView = nil;
}

@end
