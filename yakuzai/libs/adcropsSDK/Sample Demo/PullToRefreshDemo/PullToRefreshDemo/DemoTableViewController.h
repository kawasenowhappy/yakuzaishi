//
// Created by Tetsuyak on 2014/05/08.
// Copyright (c) 2014 8crops. All rights reserved.
//

#import "ChkControllerDelegate.h"

@interface DemoTableViewController : UITableViewController<ChkControllerDelegate>

@end