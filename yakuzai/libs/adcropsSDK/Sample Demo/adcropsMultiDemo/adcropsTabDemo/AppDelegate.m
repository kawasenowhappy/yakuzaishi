//
//  AppDelegate.m
//  adcropsTabDemo
//
//  Created by Tatsuya Uemura on 12/02/23.
//  Copyright (c) 2012年 8crops inc. All rights reserved.
//

#import "AppDelegate.h"
#import "ChkApplicationOptional.h"

@implementation AppDelegate

@synthesize window = _window;

@synthesize tabBar;
@synthesize firstListView;
@synthesize secondListView;
@synthesize appListView;

- (void)dealloc
{
    self.appListView    = nil;
    self.firstListView  = nil;
    self.secondListView = nil;
    self.tabBar = nil;
    
    [_window release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    self.window.backgroundColor = [UIColor whiteColor];
    
    self.tabBar = [[[UITabBarController alloc] init] autorelease];
    
    self.firstListView = [[[FirstListViewController alloc] init] autorelease];
    self.secondListView = [[[SecondListViewController alloc] init] autorelease];
    self.appListView = [[[AppListViewController alloc] init] autorelease];
    
    NSArray *views = [NSArray arrayWithObjects:self.firstListView,self.secondListView,self.appListView,nil];
    [self.tabBar setViewControllers:views animated:NO];
    tabBar.delegate = self;

    // 初期画面
    self.tabBar.selectedIndex = 0;
    
    [self.window addSubview:tabBar.view];
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    NSLog(@"applicationDidEnterBackground");
    
    // アプリがバックグランド実行へ遷移した際にコンバージョン定期送信を実行します。 
    [ChkApplicationOptional applicationDidEnterBackground:application];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // TODO:アプリがフォアグランド実行へ遷移した際にコンバージョン定期送信を停止します。
    [ChkApplicationOptional applicationWillEnterForeground];
    
    [self.firstListView loadView];
    [self.secondListView loadView];
    [self.appListView loadView];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    //切り替えに応じた処理があれば
//    NSLog(@"didSelectViewController");
//    [viewController loadView];
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    return YES;
}

@end
