//
//  ViewController.m
//  SplashDemo
//
//  Created by sazawayuki on 2014/03/20.
//  Copyright (c) 2014年 8crops inc. All rights reserved.
//

#import "ViewController.h"
#import "ChkSplashViewManager.h"

@interface ViewController ()
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIButton *showSplash = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    showSplash.frame = CGRectMake(0, 0, 100, 50);
    showSplash.center = self.view.center;
    [showSplash setTitle:@"表示" forState:UIControlStateNormal];
    [showSplash addTarget:self action:@selector(show) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:showSplash];

}

-(void)show{
    [[ChkSplashViewManager sharedManager] showWithRootViewController:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
