//
//  YakuUtil.m
//  yakuzai
//
//  Created by kawase yu on 2014/07/05.
//  Copyright (c) 2014年 nowhappy. All rights reserved.
//

#import "YakuUtil.h"
#import "NewsList.h"
#import "SiteList.h"
#import "RecommendList.h"

#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>

@interface YakuUtil ()

+(void) noReview;

@end

@interface AlertDelegate : NSObject<UIAlertViewDelegate>

@end

@implementation AlertDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
        // レビューする
        [VknUtils openBrowser:@"https://itunes.apple.com/us/app/yao-ji-shi-nian-shou-zhen/id899049413?l=ja&ls=1&mt=8"];
        [YakuUtil noReview];
    }else if(buttonIndex == 1){
        // あとで
        return;
    }else if(buttonIndex == 2){
        // レビューしない
        [YakuUtil noReview];
    }
}

@end

@implementation YakuUtil

static UIViewController* rootViewController;

static SiteList* siteList;
static NewsList* newsList;
static RecommendList* recommendList;
static NSDictionary* masta;

static NSMutableArray* showedSiteList;
static NSMutableArray* showedNewsList;
static NSMutableArray* showedRecommendList;

+(NSMutableArray*) showedRecommendList{
    return showedRecommendList;
}

+(void) setup:(UIViewController*)rootViewController_{
    rootViewController = rootViewController_;
    
    siteList = [[SiteList alloc] init];
    newsList = [[NewsList alloc] init];
    recommendList = [[RecommendList alloc] init];
    
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    readedNewsList = [[ud arrayForKey:newsListKey] mutableCopy];
    if(readedNewsList == nil)
        readedNewsList = [[NSMutableArray alloc] init];
    readedSiteList = [[ud arrayForKey:siteListKey] mutableCopy];
    if(readedSiteList == nil)
        readedSiteList = [[NSMutableArray alloc] init];
    readedRecommendList = [[ud arrayForKey:recommendListKey] mutableCopy];
    if(readedRecommendList == nil)
        readedRecommendList = [[NSMutableArray alloc] init];
    
    showedNewsList = [[ud arrayForKey:showedNewsListKey] mutableCopy];
    if(showedNewsList == nil)
        showedNewsList = [[NSMutableArray alloc] init];
    showedSiteList = [[ud arrayForKey:showedSiteListKey] mutableCopy];
    if(showedSiteList == nil)
        showedSiteList = [[NSMutableArray alloc] init];
    showedRecommendList = [[ud arrayForKey:showedRecommendListKey] mutableCopy];
    if(showedRecommendList == nil)
        showedRecommendList = [[NSMutableArray alloc] init];
}

static NSString* const modelJsonStringKey = @"modelJsonStringKey";

+(void) setModelJson:(NSString*)jsonString{
    
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:jsonString forKey:modelJsonStringKey];
    [ud synchronize];
}

+(void)reloadModel{
    NSString* jsonStr = [[NSUserDefaults standardUserDefaults] stringForKey:modelJsonStringKey];
    NSDictionary* json = [VknDataUtil strToJson:jsonStr];
    
    NSArray* articleJson = [json objectForKey:@"news"];
    [newsList reloadWithJson:articleJson];
    
    NSArray* recommendJson = [json objectForKey:@"recommend"];
    [recommendList reloadWithJson:recommendJson];
    
    NSArray* siteJson = [json objectForKey:@"site"];
    [siteList reloadWithJson:siteJson];
    
    masta = [json objectForKey:@"masta"];
}

+(UIViewController*) rootViewController{
    return rootViewController;
}

+(void) pain:(UIView*)view{
    view.transform = CGAffineTransformMakeScale(0.97f, 0.97f);
    [UIView animateWithDuration:0.2f animations:^{
        view.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
    }];
}

static NSMutableArray* readedNewsList;
static NSMutableArray* readedSiteList;
static NSMutableArray* readedRecommendList;

static NSString* const newsListKey = @"newsListKey";
static NSString* const siteListKey = @"siteListKey";
static NSString* const recommendListKey = @"recommendListKey";

static NSString* const showedNewsListKey = @"showedNewsListKey";
static NSString* const showedSiteListKey = @"showedSiteListKey";
static NSString* const showedRecommendListKey = @"showedRecommendListKey";

+(void) readNews:(int)id_{
    NSNumber* idNum = [NSNumber numberWithInt:id_];
    if([readedNewsList indexOfObject:idNum] != NSNotFound) return;
    
    [readedNewsList addObject:idNum];
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:[NSArray arrayWithArray:readedNewsList] forKey:newsListKey];
    [ud synchronize];
}

+(void) readSite:(int)id_{
    NSNumber* idNum = [NSNumber numberWithInt:id_];
    if([readedSiteList indexOfObject:idNum] != NSNotFound) return;
    
    [readedSiteList addObject:idNum];
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:[NSArray arrayWithArray:readedSiteList] forKey:siteListKey];
    [ud synchronize];
}

+(void) readRecommend:(int) id_{
    NSNumber* idNum = [NSNumber numberWithInt:id_];
    if([readedRecommendList indexOfObject:idNum] != NSNotFound) return;
    
    [readedRecommendList addObject:idNum];
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:[NSArray arrayWithArray:readedRecommendList] forKey:recommendListKey];
    [ud synchronize];
}

+(BOOL) isReadedNews:(int)id_{
    NSNumber* idNum = [NSNumber numberWithInt:id_];
    return ([readedNewsList indexOfObject:idNum] != NSNotFound);
}

+(BOOL) isReadedSite:(int)id_{
    NSNumber* idNum = [NSNumber numberWithInt:id_];
    
    return ([readedSiteList indexOfObject:idNum] != NSNotFound);
}

+(BOOL) isReadedRecommend:(int)id_{
    NSNumber* idNum = [NSNumber numberWithInt:id_];
    return ([readedRecommendList indexOfObject:idNum] != NSNotFound);
}

+(void) showNewsList{
    for(int i=0,max=[newsList count];i<max;i++){
        News* site = [newsList get:i];
        NSNumber* idNum = [NSNumber numberWithInt:site.id_];
        if([showedNewsList indexOfObject:idNum] == NSNotFound){
            [showedNewsList addObject:idNum];
        }
    }
    
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:[NSArray arrayWithArray:showedNewsList] forKey:showedNewsListKey];
    [ud synchronize];
}

+(void) showRecommendList{
    RecommendList* r = [self recommendList];
    for(int i=0,max=[r count];i<max;i++){
        Recommend* recommend = [r get:i];
        NSNumber* idNum = [NSNumber numberWithInt:recommend.id_];
        if([showedRecommendList indexOfObject:idNum] == NSNotFound){
            [showedRecommendList addObject:idNum];
        }
    }
    
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:[NSArray arrayWithArray:showedRecommendList] forKey:showedRecommendListKey];
    [ud synchronize];
}

+(RecommendList*)recommendList{
    
    RecommendList* list = [[RecommendList alloc] init];
    for(int i =0,max=[recommendList count];i<max;i++){
        Recommend* recommend = [recommendList get:i];
        [list add:recommend];
    }
    
    for(ChkRecordData* data in adcropsList){
        Recommend* recommend = [[Recommend alloc] initWithAdcropsdata:data];
        [list add:recommend];
    }
    
    return list;
}

+(SiteList*)siteList{
    return siteList;
}

+(NewsList*)newsList{
    return newsList;
}

static NSMutableArray* adcropsList;
+(void) setAdcropsList:(NSMutableArray*)adcropsList_{
    adcropsList = adcropsList_;
}

+(BOOL) hasNewNews{
    for(int i=0,max=[newsList count];i<max;i++){
        News* n = [newsList get:i];
        NSNumber* idNum = [NSNumber numberWithInt:n.id_];
        if([showedNewsList indexOfObject:idNum] == NSNotFound) return YES;
    }
    
    return NO;
}

+(BOOL) hasNewRecommend{
    RecommendList* r = [self recommendList];
    for(int i=0,max=[r count];i<max;i++){
        Recommend* n = [r get:i];
        NSNumber* idNum = [NSNumber numberWithInt:n.id_];
        if([showedRecommendList indexOfObject:idNum] == NSNotFound) return YES;
    }
    
    return NO;
}

+(BOOL) isShowRecommend{
    if(![self isJaDevice]) return NO;
    NSNumber* showRecommend = [masta objectForKey:@"showRecommend"];
    if(showRecommend == nil){
        return NO;
    }
    return ([showRecommend intValue] == 1);
}

+(BOOL) isJaDevice{
    CTTelephonyNetworkInfo *netinfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [netinfo subscriberCellularProvider];
    NSString* countryCode = carrier.mobileCountryCode;
    return ([countryCode isEqualToString:@"440"] || [countryCode isEqualToString:@"441"]);
}

+(int) tekisei:(int)ageIndex prefIndex:(int)prefIndex workIndex:(int)workIndex{
    float price = (float)[self ageTekisei:ageIndex];
    price = price * [self prefRate:prefIndex];
    price = price * [self workRate:workIndex];
    return (int)price;
}

// TODO テキトウ
+(float) workRate:(int)workIndex{
    switch (workIndex) {
        case 0: return 0.99f;
        case 1: return 1.01f;
        case 2: return 1.00f;
    }
    
    return 1.0f;
}

+(int) current:(int)currentIndex{
    int start = 24;
    int current = (start + currentIndex) * 100000;
    return current;
}

+(float) prefRate:(int)prefIndex{
    switch (prefIndex) {
        case 0: return 0.84f;
        case 1: return 0.75f;
        case 2: return 0.81f;
        case 3: return 0.91f;
        case 4: return 0.75f;
        case 5: return 0.82f;
        case 6: return 0.86f;
        case 7: return 0.95f;
        case 8: return 0.97f;
        case 9: return 0.91f;
        case 10: return 0.89f;
        case 11: return 0.94f;
        case 12: return 1.31f;
        case 13: return 1.01f;
        case 14: return 0.87f;
        case 15: return 0.88f;
        case 16: return 0.90f;
        case 17: return 0.95f;
        case 18: return 0.89f;
        case 19: return 0.91f;
        case 20: return 0.89f;
        case 21: return 0.98f;
        case 22: return 1.03f;
        case 23: return 0.91f;
        case 24: return 0.95f;
        case 25: return 0.98f;
        case 26: return 1.11f;
        case 27: return 0.95f;
        case 28: return 0.91f;
        case 29: return 0.89f;
        case 30: return 0.81f;
        case 31: return 0.83f;
        case 32: return 1.00f;
        case 33: return 0.96f;
        case 34: return 0.98f;
        case 35: return 0.91f;
        case 36: return 0.89f;
        case 37: return 0.85f;
        case 38: return 0.86f;
        case 39: return 0.97f;
        case 40: return 0.78f;
        case 41: return 0.88f;
        case 42: return 0.84f;
        case 43: return 0.85f;
        case 44: return 0.78f;
        case 45: return 0.81f;
        case 46: return 0.80;
    }
    
    return 1.0f;
}

+(int) ageTekisei:(int)ageIndex{
    switch (ageIndex) {
        case 0: return 3250176;
        case 1: return 3939276;
        case 2: return 4369308;
        case 3: return 4712640;
        case 4: return 4830588;
        case 5: return 5145084;
        case 6: return 5191644;
        case 7: return 5222232;
        case 8: return 5259012;
        case 9: return 5188068;
    }
    
    return 0;
}

+(NSString*)priceToString:(int)price{
    int man = price / 10000;
    int sen = price - (man * 10000);
    
    return [NSString stringWithFormat:@"%d万%d円", man, sen];
}

static NSString* showedCountKey = @"showedCountKey";
static AlertDelegate* alertDelegate ;
+(void) tryShowRecommend{
    if([self isNoReview]) return;
    
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    int current = [ud integerForKey:showedCountKey];
    current++;
    [ud setInteger:current forKey:showedCountKey];
    [ud synchronize];
    
    if(!(current == 5 || current == 15 || current == 40 || current == 100)) return;
    
    UIAlertView* alertView = [[UIAlertView alloc] init];
    alertView.title = @"助けてください";
    alertView.message = @"いつもご利用ありがとうございます。\nもし今お時間ありましたら、レビューにてご意見ご感想を書いていただけないでしょうか。\n開発の励みになりますm(^_^)m。";
    [alertView addButtonWithTitle:@"レビューする"];
    [alertView addButtonWithTitle:@"あとで"];
    [alertView addButtonWithTitle:@"レビューしない"];
    
    alertDelegate = [[AlertDelegate alloc] init];
    alertView.delegate = alertDelegate;
    
    [alertView show];
}

static NSString* noReviewKey = @"noReviewKey";
+(BOOL) isNoReview{
    return [[NSUserDefaults standardUserDefaults] boolForKey:noReviewKey];
}

+(void) noReview{
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    [ud setBool:YES forKey:noReviewKey];
    [ud synchronize];
}

@end
